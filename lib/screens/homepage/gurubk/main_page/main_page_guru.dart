import 'dart:convert';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/catatan_siswa_intro.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/aum_result/list_kelas_aum.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/bimbingan_page.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/profile_guru/profile_guru_page.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/schedule_guru/schedule_page.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/gurubk_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/user_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sticky_headers/sticky_headers.dart';
import 'package:provider/provider.dart';
import 'package:flutter/scheduler.dart';

class MainPageGuru extends StatefulWidget {
  @override
  _MainPageGuruState createState() => _MainPageGuruState();
}

class _MainPageGuruState extends State<MainPageGuru> {
  void _profileGuruPage() {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => ProfileGuruPage()),
    );
  }

  void _aumresultPage() {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => ListKelasAum()),
    );
  }

  void _bimbinganGuruPage() {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => ChatGuru()),
    );
  }

  void _catatanSiswaPage() {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => CatatanSiswa()),
    );
  }

  void _reportGuruPage() {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => JadwalGuruPage()),
    );
  }

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context.read<GurubkViewModel>().setNetworkService(
          context.read<NetworkService>()); //for fetchGurubk()
      context.read<GurubkViewModel>().fetchGurubk();
    });
  }

  @override
  Widget build(BuildContext context) {
    var mediaQueryData = MediaQuery.of(context);
    //List<String> entries = <String>['Student', 'Class', ''];
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Home',
          style: CustomFont.appBar,
        ),
        centerTitle: true,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: false,
      ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            SizedBox(
              width: double.infinity,
              child: Consumer<UserViewModel>(
                builder: (_, UserViewModel vm, __) => InkWell(
                  child: Card(
                    child: Consumer<GurubkViewModel>(
                      builder: (_, GurubkViewModel vm1, __) => Padding(
                        padding:
                            const EdgeInsets.fromLTRB(8.0, 26.0, 26.0, 26.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.fromLTRB(
                                  0, 10.0, 10.0, 10.0),
                            ),
                            CircleAvatar(
                              radius: 50,
                              backgroundColor: Colors.grey,
                              child: ClipOval(
                                child: SizedBox(
                                  width: 100,
                                  height: 100,
                                  child: Image(
                                    image: CustomImage.avatar,
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(
                                  20.0, 10.0, 10.0, 10.0),
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  vm?.user?.name ?? '-',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18.0),
                                ),
                                Text(
                                  'Guru Bimbingan Konseling',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18.0,
                                      color: Colors.grey),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  onTap: _profileGuruPage,
                  //print("Profile");
                ),
              ),
            ),
            Expanded(
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: InkWell(
                      child: Card(
                        color: CustomColor.whitebg,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 20.0),
                                child: Image(
                                  image: CustomImage.guru1,
                                  height: 170,
                                ),
                              ),
                            ),
                            Center(
                              child: Container(
                                //padding: const EdgeInsets.only(top: 2.0),
                                child: Text(
                                  'Catatan Siswa',
                                  style: TextStyle(fontWeight: FontWeight.w500),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: _catatanSiswaPage,
                      //print("AUM");
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      child: Card(
                        color: CustomColor.whitebg,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 10.0),
                                child: Image(
                                  image: CustomImage.menu2,
                                  width: 180,
                                  height: 180,
                                ),
                              ),
                            ),
                            Center(
                              child: Container(
                                //padding: const EdgeInsets.only(top: 5.0),
                                child: Text(
                                  'Bimbingan',
                                  style: TextStyle(fontWeight: FontWeight.w500),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: _bimbinganGuruPage,
                      //print("Bimbingan");
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Row(
                children: <Widget>[
                  // bisa di rapiin dijadiin widget terpisah
                  Expanded(
                    child: InkWell(
                      child: Card(
                        color: CustomColor.whitebg,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 20.0),
                                child: Image(
                                  image: CustomImage.menu3,
                                  width: 160,
                                  height: 160,
                                ),
                              ),
                            ),
                            Center(
                              child: Container(
                                padding: const EdgeInsets.only(top: 10.0),
                                child: Text(
                                  'AUM Siswa',
                                  style: TextStyle(fontWeight: FontWeight.w500),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: _aumresultPage,
                      //print("Informasi");
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      child: Card(
                        color: CustomColor.whitebg,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 10.0),
                                child: Image(
                                  image: CustomImage.schedule,
                                  width: 180.0,
                                  height: 180.0,
                                ),
                              ),
                            ),
                            Center(
                              child: Container(
                                //padding: const EdgeInsets.only(top: 5.0),
                                child: Text(
                                  'Jadwal',
                                  style: TextStyle(fontWeight: FontWeight.w500),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: _reportGuruPage,
                      //print("Tata Tertib");
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
            ),
          ],
        ),
      ),
    );
  }

  Future<bool> _onBackPressed() {
    return SystemNavigator.pop();
  }
}
