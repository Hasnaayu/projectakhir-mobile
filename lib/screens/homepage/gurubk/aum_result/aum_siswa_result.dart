import 'dart:convert';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/kelas_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/aum_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/route_manager.dart';
import 'package:provider/provider.dart';
import 'package:flutter/scheduler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class AumSiswaResult extends StatefulWidget {
  @override
  _AumSiswaResultState createState() => _AumSiswaResultState();
}

class _AumSiswaResultState extends State<AumSiswaResult> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context.read<AumViewModel>().setNetworkService(
            context.read<NetworkService>(),
          ); //for fetchPelanggaran()
      int id = context.read<KelasViewModel>().selectedSiswaModel.id;
      context.read<AumViewModel>().fetchAum(id);
    });
  }

  void _onRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    int id = context.read<KelasViewModel>().selectedSiswaModel.id;
    context.read<AumViewModel>().removeEarlierData(id);
    _refreshController.refreshCompleted();
  }

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  void dispose() {
    super.dispose();
    _refreshController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Hasil AUM',
          style: CustomFont.appBar,
        ),
        centerTitle: true,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
        actions: <Widget>[
          IconButton(
            icon: new Icon(Icons.article, color: Colors.white),
            onPressed: () => Get.toNamed<void>('/formaum'),
          ),
        ],
      ),
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: false,
        controller: _refreshController,
        onRefresh: _onRefresh,
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Consumer<AumViewModel>(
            builder: (_, AumViewModel vm, __) => Column(
              children: [
                Padding(padding: EdgeInsets.only(top: 20.0)),
                Center(
                  child: Text(
                    'Muat ulang untuk menampilkan data terbaru',
                    style: CustomFont.noticeText,
                  ),
                ),
                if (vm.aums.isNotEmpty)
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        width: 480,
                        decoration: BoxDecoration(
                          color: CustomColor.whitebg,
                          border: Border.all(
                              width: 2.0, color: CustomColor.themedarker),
                          borderRadius: BorderRadius.all(
                            Radius.circular(16),
                          ),
                        ),
                        child: Column(
                          //mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 8.0),
                              child: Text(
                                'Tanggal Diunggah',
                                style: CustomFont.smallMutedDarker,
                              ),
                            ),
                            for (var i = 0; i < 1; i++)
                              Container(
                                padding: const EdgeInsets.fromLTRB(
                                    8.0, 0.0, 8.0, 0.0),
                                child: Text(
                                    vm?.aums[i]?.createdAtToString() ?? 'null',
                                    style: CustomFont.noticeText),
                              ),
                            Padding(
                              padding:
                                  const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 8.0),
                              child: Text(
                                'Hasil Alat Ungkap Masalah',
                                style: CustomFont.smallMutedDarker,
                              ),
                            ),
                            for (var i = 0; i < vm.aums.length; i++)
                              Container(
                                padding: const EdgeInsets.fromLTRB(
                                    8.0, 8.0, 8.0, 8.0),
                                child: Text(
                                  vm?.aums[i]?.title ?? 'null',
                                  style: CustomFont.basicText,
                                ),
                              ),
                          ],
                        ),
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
