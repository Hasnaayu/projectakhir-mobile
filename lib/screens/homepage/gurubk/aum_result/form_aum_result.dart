import 'dart:convert';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/kelas_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/aum_form_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/route_manager.dart';
import 'package:provider/provider.dart';
import 'package:flutter/scheduler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class FormAumSiswaResult extends StatefulWidget {
  @override
  _FormAumSiswaResultState createState() => _FormAumSiswaResultState();
}

class _FormAumSiswaResultState extends State<FormAumSiswaResult> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context.read<AumFormViewModel>().setNetworkService(
            context.read<NetworkService>(),
          ); //for fetchPelanggaran()
      int id = context.read<KelasViewModel>().selectedSiswaModel.id;
      context.read<AumFormViewModel>().fetchAumForm(id);
    });
  }

  void _onRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    int id = context.read<KelasViewModel>().selectedSiswaModel.id;
    context.read<AumFormViewModel>().removeEarlierDataForm(id);
    _refreshController.refreshCompleted();
  }

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  void dispose() {
    super.dispose();
    _refreshController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Hasil AUM Form',
          style: CustomFont.appBar,
        ),
        centerTitle: true,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: false,
        controller: _refreshController,
        onRefresh: _onRefresh,
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Consumer<AumFormViewModel>(
            builder: (_, AumFormViewModel vm, __) => Column(
              children: [
                Padding(padding: EdgeInsets.only(top: 20.0)),
                Center(
                  child: Text(
                    'Muat ulang untuk menampilkan data terbaru',
                    style: CustomFont.noticeText,
                  ),
                ),
                if (vm.forms.isNotEmpty)
                  for (var i = 0; i < vm.forms.length; i++)
                    Card(
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Container(
                          width: 480,
                          decoration: BoxDecoration(
                            color: CustomColor.whitebg,
                            border: Border.all(
                                width: 2.0, color: CustomColor.themedarker),
                            borderRadius: BorderRadius.all(
                              Radius.circular(16),
                            ),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.fromLTRB(
                                    8.0, 8.0, 8.0, 8.0),
                                child: Text(
                                  'Tanggal Diunggah',
                                  style: CustomFont.smallMutedDarker,
                                ),
                              ),
                              //for (var i = 0; i < 1; i++)
                              Container(
                                padding: const EdgeInsets.fromLTRB(
                                    8.0, 0.0, 8.0, 0.0),
                                child: Text(
                                    vm?.forms[i]?.createdAtToString() ?? 'null',
                                    style: CustomFont.noticeText),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(
                                    8.0, 8.0, 8.0, 8.0),
                                child: Text(
                                  'Hasil Form Alat Ungkap Masalah',
                                  style: CustomFont.smallMutedDarker,
                                ),
                              ),
                              Container(
                                padding: const EdgeInsets.fromLTRB(
                                    8.0, 8.0, 8.0, 8.0),
                                child: ListTile(
                                  title: Text(
                                    'Apakah sudah menggambarkan seluruh masalah Anda? Sertakan alasan Anda!',
                                    style: CustomFont.basicTextgrey,
                                  ),
                                  subtitle: Text(
                                    vm?.forms[i]?.problem ?? 'null',
                                    style: CustomFont.basicText,
                                  ),
                                ),
                              ),
                              Container(
                                padding: const EdgeInsets.fromLTRB(
                                    8.0, 8.0, 8.0, 8.0),
                                child: ListTile(
                                  title: Text(
                                    'Masalah lain yang Anda hadapi?',
                                    style: CustomFont.basicTextgrey,
                                  ),
                                  subtitle: Text(
                                    vm?.forms[i]?.desc ?? 'null',
                                    style: CustomFont.basicText,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
