import 'package:intl/intl.dart';

class BimbinganModel {
  BimbinganModel({
    this.id,
    this.tanggal_bimbingan,
    this.permasalahan,
    this.solusi,
    this.id_siswa,
    this.gurubkId,
  });

  BimbinganModel.fromJson(Map<String, dynamic> json) {
    id = json['id_bimbingan'] as int;
    tanggal_bimbingan = DateTime.parse(json['tanggal_bimbingan'] as String);
    permasalahan = json['permasalahan'] as String;
    solusi = json['solusi'] as String;
    id_siswa = json['id_siswa'] as int;
    gurubkId = json['id_guruBK'] as int;
  }

  int id;
  DateTime tanggal_bimbingan;
  String permasalahan;
  String solusi;
  int id_siswa;
  int gurubkId;

  @override
  String toString() {
    // TODO: implement toString
    return '$id_siswa';
  }

  @override
  String toStringDue() {
    return DateFormat('EEEEEE, d - M - y').format(tanggal_bimbingan);
  }
}
