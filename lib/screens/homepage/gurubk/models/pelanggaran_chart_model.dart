import 'package:bk_mobile_app/styles/color.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class PelanggaranChartModel {
  PelanggaranChartModel({this.month, this.barPelanggaran, this.pelanggaran});

  PelanggaranChartModel.fromJson(Map<String, dynamic> json) {
    month = json['bulan'] as String;
    pelanggaran = json['pelanggaran'] as int;
  }

  String month;
  charts.Color barPelanggaran =
      charts.ColorUtil.fromDartColor(CustomColor.themelighter);
  int pelanggaran;
}
