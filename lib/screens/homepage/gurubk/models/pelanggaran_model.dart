import 'package:intl/intl.dart';

class PelanggaranModel {
  PelanggaranModel({
    this.id,
    this.tanggal_pelanggaran,
    this.pelanggaran,
    this.tindak_lanjut,
    this.id_siswa,
    this.gurubkId,
  });

  PelanggaranModel.fromJson(Map<String, dynamic> json) {
    id = json['id_pelanggaran'] as int;
    tanggal_pelanggaran = DateTime.parse(json['tanggal_pelanggaran'] as String);
    pelanggaran = json['pelanggaran'] as String;
    tindak_lanjut = json['tindak_lanjut'] as String;
    id_siswa = json['id_siswa'] as int;
    gurubkId = json['id_guruBK'] as int;
  }

  int id;
  DateTime tanggal_pelanggaran;
  String pelanggaran;
  String tindak_lanjut;
  int id_siswa;
  int gurubkId;

  @override
  String toString() {
    // TODO: implement toString
    return '$id_siswa';
  }

  @override
  String toStringDue() {
    return DateFormat('EEEEEE, d - M - y').format(tanggal_pelanggaran);
  }
}
