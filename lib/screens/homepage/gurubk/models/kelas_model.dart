class KelasModel {
  KelasModel({
    this.id,
    this.kelas,
    this.jurusan,
    this.id_gurubk,
  });

  KelasModel.fromJson(Map<String, dynamic> json) {
    id = json['id_kelas'] as int;
    kelas = json['kelas'] as String;
    jurusan = json['jurusan'] as String;
    id_gurubk = json['id_gurubk'] as String;
  }

  int id;
  String kelas;
  String jurusan;
  String id_gurubk;
}
