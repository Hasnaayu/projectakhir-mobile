class GurubkModel {
  GurubkModel({
    this.id,
    this.name,
    this.datebirth,
    this.gender,
    this.telp,
    this.nik,
    this.address,
    this.status,
    this.accId,
    this.foto,
    this.createdAt,
    this.updatedAt,
  });

  GurubkModel.fromJson(Map<String, dynamic> json) {
    id = json['id_siswa'] as int;
    name = json['nama_gurubk'] as String;
    datebirth = json['tanggal_lahir'] as String;
    gender = json['jenis_kelamin'] as String;
    telp = json['telp'] as String;
    nik = json['nik'] as String;
    address = json['alamat'] as String;
    status = json['status_siswa'] as String;
    accId = json['id_account'] as int;
    foto = json['foto'] as String;
    createdAt = json["created_at"] as String;
    updatedAt = json["updated_at"] as String;
  }

  int id;
  String name;
  String datebirth;
  String gender;
  String telp;
  String nik;
  String address;
  String status;
  int accId;
  String foto;
  String createdAt;
  String updatedAt;

  @override
  String toString() {
    // TODO: implement toString
    return '$foto';
  }
}
