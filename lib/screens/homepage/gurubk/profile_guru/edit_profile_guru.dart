import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/gurubk_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/will_pop_vm.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:get/route_manager.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:image_picker/image_picker.dart';

class EditProfileGuru extends StatefulWidget {
  @override
  _EditProfileGuruState createState() => _EditProfileGuruState();
}

class _EditProfileGuruState extends State<EditProfileGuru> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context
          .read<GurubkViewModel>()
          .setNetworkService(context.read<NetworkService>()); //for addFoto()
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<WillPopViewModel>(
      builder: (_, WillPopViewModel _vm, __) => WillPopScope(
        onWillPop: () => _vm.onBackPressed('/homepageguru'),
        child: Scaffold(
          appBar: AppBar(
            title: Text(
              'Edit Foto Profile',
              style: CustomFont.appBar,
            ),
            centerTitle: true,
            backgroundColor: CustomColor.themedarker,
            automaticallyImplyLeading: false,
          ),
          body: Consumer<GurubkViewModel>(
            builder: (_, GurubkViewModel vm, __) => Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                // Padding(
                //   padding: const EdgeInsets.all(15.0),
                SizedBox(
                  height: 20.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Align(
                      alignment: Alignment.center,
                      child: CircleAvatar(
                        radius: 60,
                        backgroundColor: Colors.grey,
                        child: ClipOval(
                          child: SizedBox(
                            width: 200,
                            height: 200,
                            child: vm.foto != null
                                ? Image.file(
                                    vm.foto,
                                    fit: BoxFit.fill,
                                  )
                                : Image(
                                    image: CustomImage.profileNav,
                                    fit: BoxFit.fill,
                                  ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 80.0),
                      child: IconButton(
                        icon: Icon(
                          Icons.camera_alt,
                          size: 30.0,
                        ),
                        onPressed: vm.getImage,
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(top: 70.0),
                ),
                MaterialButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0)),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 100.0),
                      child: Text(
                        'Simpan'.toUpperCase(),
                        style: CustomFont.signIn,
                      ),
                    ),
                    padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                    color: CustomColor.themedarker,
                    onPressed: vm.addFoto),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
