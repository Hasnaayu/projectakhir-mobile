import 'dart:convert';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/profile_guru/edit_profile_guru.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/profile_guru/logout_page.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/gurubk_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/user_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/will_pop_vm.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/screens/welcome/login.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:get/route_manager.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:flutter/scheduler.dart';

class ProfileGuruPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ProfileGuruPageState();
  }
}

class _ProfileGuruPageState extends State<ProfileGuruPage> {
  ScrollController _controller;

  void _profilePage() {
    Get.offNamed<void>('/profilepageguru');
  }

  void _securityPin() {
    Get.toNamed<void>('/my_security_pin');
  }

  void _logoutPage() {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => LogoutPage()),
    );
  }

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context
          .read<UserViewModel>()
          .setNetworkService(context.read<NetworkService>()); //for fetchUser()
      context.read<UserViewModel>().fetchUser();
      context.read<GurubkViewModel>().setNetworkService(
          context.read<NetworkService>()); //for fetchGurubk()
      context.read<GurubkViewModel>().fetchGurubk();
    });
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Consumer<WillPopViewModel>(
      builder: (_, WillPopViewModel _vm, __) => WillPopScope(
        onWillPop: () => _vm.exit(),
        child: Scaffold(
          appBar: AppBar(
            title: Text(
              'Profile',
              style: CustomFont.appBar,
            ),
            centerTitle: true,
            backgroundColor: CustomColor.themedarker,
            automaticallyImplyLeading: false,
          ),
          body: ListView(
            shrinkWrap: true,
            controller: _controller,
            children: ListTile.divideTiles(
              context: context,
              tiles: [
                Consumer<UserViewModel>(
                  builder: (_, UserViewModel vm, __) => Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      // Padding(
                      //   padding: const EdgeInsets.all(15.0),
                      SizedBox(
                        height: 10.0,
                      ),
                      Consumer<GurubkViewModel>(
                        builder: (_, GurubkViewModel vm1, __) => Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Align(
                              alignment: Alignment.center,
                              child: CircleAvatar(
                                radius: 50,
                                backgroundColor: Colors.grey,
                                child: ClipOval(
                                  child: SizedBox(
                                    width: 100,
                                    height: 100,
                                    child: Image(
                                      image: CustomImage.avatar,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            // Padding(
                            //   padding: EdgeInsets.only(top: 60.0),
                            //   child: IconButton(
                            //     icon: Icon(
                            //       Icons.edit,
                            //       size: 30.0,
                            //     ),
                            //     onPressed: () {
                            //       Navigator.of(context).push(
                            //         MaterialPageRoute(
                            //             builder: (_) => EditProfileGuru()),
                            //       );
                            //     },
                            //   ),
                            // ),
                          ],
                        ),
                      ),
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 20, 0, 15),
                          child: Text(
                            vm?.user?.name ?? '-',
                            style: CustomFont.smallMutedDarker,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Consumer<GurubkViewModel>(
                  builder: (_, GurubkViewModel vm1, __) => Container(
                    padding: EdgeInsets.all(5.0),
                    decoration: BoxDecoration(
                        color: CustomColor.whitebg,
                        border: Border(
                            top: BorderSide(
                                color:
                                    CustomColor.mutedButton.withOpacity(0.3)),
                            bottom: BorderSide(
                                color:
                                    CustomColor.mutedButton.withOpacity(0.3)))),
                    child: ListTile(
                      tileColor: CustomColor.whitebg,
                      leading: Image(
                          image: CustomImage.kelas,
                          height: 35,
                          width: 35,
                          color: CustomColor.mutedButton),
                      title: Text(
                        'Nama Lengkap',
                        style: CustomFont.smallMuted,
                      ),
                      subtitle: Text(
                        vm1?.gurubk?.name ?? '-',
                      ),
                    ),
                  ),
                ),
                Consumer<UserViewModel>(
                  builder: (_, UserViewModel vm, __) => Container(
                    padding: EdgeInsets.all(5.0),
                    decoration: BoxDecoration(
                        color: CustomColor.whitebg,
                        border: Border(
                            top: BorderSide(
                                color:
                                    CustomColor.mutedButton.withOpacity(0.3)),
                            bottom: BorderSide(
                                color:
                                    CustomColor.mutedButton.withOpacity(0.3)))),
                    child: ListTile(
                      tileColor: CustomColor.whitebg,
                      leading: Image(
                          image: CustomImage.email,
                          height: 35,
                          width: 35,
                          color: CustomColor.mutedButton),
                      title: Text(
                        'Email',
                        style: CustomFont.smallMuted,
                      ),
                      subtitle: Text(
                        vm?.user?.email ?? '-',
                      ),
                    ),
                  ),
                ),
                Consumer<GurubkViewModel>(
                  builder: (_, GurubkViewModel vm1, __) => Container(
                    padding: EdgeInsets.all(5.0),
                    decoration: BoxDecoration(
                        color: CustomColor.whitebg,
                        border: Border(
                            top: BorderSide(
                                color:
                                    CustomColor.mutedButton.withOpacity(0.3)),
                            bottom: BorderSide(
                                color:
                                    CustomColor.mutedButton.withOpacity(0.3)))),
                    child: ListTile(
                      tileColor: CustomColor.whitebg,
                      leading: Image(
                          image: CustomImage.address,
                          height: 35,
                          width: 35,
                          color: CustomColor.mutedButton),
                      title: Text(
                        'Alamat',
                        style: CustomFont.smallMuted,
                      ),
                      subtitle: Text(
                        vm1?.gurubk?.address ?? '-',
                      ),
                    ),
                  ),
                ),
                // Container(
                //   padding: EdgeInsets.all(5.0),
                //   decoration: BoxDecoration(
                //     color: CustomColor.whitebg,
                //     border: Border(
                //       top: BorderSide(
                //           color: CustomColor.mutedButton.withOpacity(0.3)),
                //       bottom: BorderSide(
                //         color: CustomColor.mutedButton.withOpacity(0.3),
                //       ),
                //     ),
                //   ),
                //   child: ListTile(
                //     tileColor: CustomColor.whitebg,
                //     leading: Image(
                //         image: CustomImage.pin,
                //         height: 35,
                //         width: 35,
                //         color: CustomColor.mutedButton),
                //     title: Text(
                //       'PIN Keamanan',
                //       style: CustomFont.smallMuted,
                //     ),
                //     trailing: Icon(
                //       Icons.keyboard_arrow_right,
                //       size: 35,
                //     ),
                //     onTap: _securityPin,
                //   ),
                // ),
                Container(
                  padding: EdgeInsets.all(5.0),
                  decoration: BoxDecoration(
                      color: CustomColor.whitebg,
                      border: Border(
                          top: BorderSide(
                              color: CustomColor.mutedButton.withOpacity(0.3)),
                          bottom: BorderSide(
                              color:
                                  CustomColor.mutedButton.withOpacity(0.3)))),
                  child: ListTile(
                    tileColor: CustomColor.whitebg,
                    leading: Image(
                        image: CustomImage.logout,
                        height: 35,
                        width: 35,
                        color: CustomColor.mutedButton),
                    title: Text(
                      'Logout',
                      style: CustomFont.smallMuted,
                    ),
                    trailing: Icon(
                      Icons.keyboard_arrow_right,
                      size: 35,
                    ),
                    onTap: _logoutPage,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                ),
              ],
            ).toList(),
          ),
        ),
      ),
    );
  }
}
