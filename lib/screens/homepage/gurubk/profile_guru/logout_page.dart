import 'dart:convert';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/auth/view_models/LogoutViewModel.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/profile_page/profile_page.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/user_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/will_pop_vm.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/scheduler.dart';

class LogoutPage extends StatefulWidget {
  @override
  _LogoutPageState createState() => _LogoutPageState();
}

class _LogoutPageState extends State<LogoutPage> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((Duration _) {
      context
          .read<UserViewModel>()
          .setNetworkService(context.read<NetworkService>()); //for fetchUser()
      context
          .read<LogoutViewModel>()
          .setNetworkService(context.read<NetworkService>()); //for doLogout()
      context.read<UserViewModel>().fetchUser();
    });
  }

  Widget build(BuildContext context) {
    // final Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: CustomColor.whitebg,
        title: Text(
          'Logout Page',
          style: CustomFont.appBar,
        ),
        automaticallyImplyLeading: false,
      ),
      body: Consumer<WillPopViewModel>(
        builder: (_, WillPopViewModel _vm, __) => WillPopScope(
          onWillPop: () => _vm.onBackPressed('/homepageguru'),
          child: Consumer<UserViewModel>(
            builder: (_, UserViewModel vm, __) => Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(16),
                  ),
                  Center(
                    child: Text(
                      'Anda login sebagai:',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                  ),
                  Center(
                    child: Text(
                      vm?.user?.name ?? 'null',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10),
                  ),
                  Consumer<LogoutViewModel>(
                    builder: (_, LogoutViewModel vm_, __) => Center(
                      child: MaterialButton(
                        elevation: 10,
                        onPressed: vm_?.doLogout ?? "NO LOGOUT",
                        color: CustomColor.themedarker,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        child: Text('Logout'),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
