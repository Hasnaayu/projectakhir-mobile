import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/models/message_model.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/view_models/chat_vm.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MyMessageCard extends StatelessWidget {
  final MessageModel message;
  const MyMessageCard({
    Key key,
    this.message,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<ChatViewModel>(
      builder: (_, ChatViewModel vm, __) => Container(
        width: 310,
        padding: EdgeInsets.all(21),
        margin: EdgeInsets.only(bottom: 12),
        decoration: BoxDecoration(
            color: CustomColor.themelightest,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(28),
              topRight: Radius.circular(28),
              bottomLeft: Radius.circular(28),
            )),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                message?.content ?? '-',
              ),
            ),
          ],
        ),
      ),
    );
  }
}
