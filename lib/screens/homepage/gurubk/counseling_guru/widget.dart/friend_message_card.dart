import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/models/message_model.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/view_models/chat_vm.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FriendMessageCard extends StatelessWidget {
  final MessageModel message;
  const FriendMessageCard({
    Key key,
    this.message,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<ChatViewModel>(
      builder: (_, ChatViewModel vm, __) => Padding(
        padding: EdgeInsets.only(bottom: 12),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            CircleAvatar(
              child: Image(image: CustomImage.avatar),
            ),
            SizedBox(
              width: 12,
            ),
            Expanded(
              child: Container(
                width: 310,
                padding: EdgeInsets.all(21),
                decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      CustomColor.mutedButton,
                      CustomColor.themelightest,
                    ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(28),
                      topRight: Radius.circular(28),
                      bottomRight: Radius.circular(28),
                    )),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        message?.content ?? '-',
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
