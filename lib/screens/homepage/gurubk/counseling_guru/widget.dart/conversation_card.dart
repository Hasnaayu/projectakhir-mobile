import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/models/chat_model.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/view_models/chat_vm.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:timeago/timeago.dart' as timeago;

class ConversationCard extends StatelessWidget {
  final ChatModel chatting;
  final Function onTap;
  const ConversationCard({
    this.chatting,
    Key key,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<ChatViewModel>(
      builder: (_, ChatViewModel vm, __) => ListTile(
        onTap: onTap,
        leading: ClipOval(
          child: Image(image: CustomImage.avatar),
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              chatting?.user?.name,
              style: TextStyle(
                color: Colors.black.withOpacity(0.9),
                fontSize: 18,
              ),
            ),
            Text(
              timeago.format(chatting.createdAt),
              style: TextStyle(
                color: Colors.black.withOpacity(0.7),
                fontSize: 14,
              ),
            )
          ],
        ),
        subtitle: Text(chatting?.messages?.last?.content ?? '-'),
      ),
    );
  }
}
