class MessageModel {
  MessageModel({
    this.id,
    this.content,
    //this.read,
    this.accountId,
    this.id_chat,
    this.createdAt,
    this.updatedAt,
  });

  MessageModel.fromJson(Map<String, dynamic> json) {
    id = json['id_message'] as int;
    content = json['content'] as String;
    //read = json['read'] as bool;
    id_chat = json['id_chat'] as int;
    accountId = json['id_account'] as int;
    createdAt = json["created_at"] as String;
    updatedAt = json["updated_at"] as String;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['content'] = this.content;
    //data['read'] = this.read;
    data['id_account'] = this.accountId;
    data['id_chat'] = this.id_chat;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }

  int id;
  String content;
  //bool read;
  int accountId;
  int id_chat;
  String createdAt;
  String updatedAt;

  @override
  String toString() =>
      'id: $id chat_id: $id_chat content: $content accountId: $accountId';
}
