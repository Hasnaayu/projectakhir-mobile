import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/models/message_model.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/user_model.dart';

class ChatModel {
  ChatModel(
      {this.id, this.user, this.messages, this.createdAt, this.messagess});

  ChatModel.fromJson(Map<String, dynamic> json) {
    id = json['id_chat'] as int;
    user = json['user'] != null ? new UserModel.fromJson(json['user']) : null;
    createdAt = DateTime.tryParse(json["created_at"] as String);
    if (json['messages'] != null) {
      messages = new List<MessageModel>();
      json['messages'].forEach((v) {
        messages.add(new MessageModel.fromJson(v));
      });
    }
  }

  int id;
  UserModel user;
  DateTime createdAt;
  List<MessageModel> messages;
  List<String> messagess;


  @override
  String toString() => 'id: $id user: $user msg: $messages';
}
