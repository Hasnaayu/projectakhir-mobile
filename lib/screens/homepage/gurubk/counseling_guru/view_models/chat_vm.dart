import 'dart:io';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/models/chat_model.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/models/message_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:get/route_manager.dart';

class ChatViewModel extends ChangeNotifier {
  ChatViewModel();
  NetworkService _net;
  ChatModel activeChat;
  // MessageModel message;

  List<ChatModel> _chats = [];
  List<ChatModel> get chats => _chats;

  List<MessageModel> _messagee = [];
  List<MessageModel> get messagee => _messagee;

  // List<int> accountIds = [];
  // List<String> createdAts = [];
  //List<String> createdAts = [];

  // String content;
  // int id_chat;
  bool _busy = false;
  int chatId;

  bool get busy => _busy;
  void setBusy(bool val) {
    _busy = val;
    notifyListeners();
  }

  void setNetworkService(NetworkService net) {
    _net = net;
    notifyListeners();
  }

  // void onContentChange(String val) {
  //   content = val;
  //   notifyListeners();
  // }

  // void onChat(MessageModel message, ChatModel chatting) async {
  //   message.id_chat = chatting.id;
  //   notifyListeners();
  // }

  void onChat(
    ChatModel val,
    // dynamic val2,
  ) async {
    activeChat = val;
    //message = val2;
    Get.toNamed('/chatting');
    notifyListeners();
  }

  // void onMessage(
  //   dynamic val,
  // ) async {
  //   message = val;
  //   notifyListeners();
  // }

  // void onSendMessage(dynamic chatId) async {
  //   msg.id_chat = chatId;
  //   notifyListeners();
  // }

  Future<void> getChats() async {
    if (_chats.isNotEmpty) return _chats;
    setBusy(false);
    final Response<dynamic> resp = await _net.request('/chats');
    List<dynamic> listData = resp.data['data'];

    for (dynamic data in listData) {
      _chats.add(ChatModel.fromJson(data as Map<String, dynamic>));
    }

    if (resp.data['success'] == true) {
      setBusy(true);
    }

    // for (var i = 0; i < chats.length; i++) {
    //   for (var j = 0; j < chats[i].messages.length; j++) {
    //     accountIds.add(chats[i].messages[j].accountId);
    //     createdAts.add(chats[i].messages[j].createdAt);
    //     //messagee.add(chats[i].messages[j]);
    //   }
    // }

    setBusy(false);
    notifyListeners();
  }

  Future<void> storeMessage(String msg) async {
    //setBusy(true);
    var formData = FormData.fromMap({
      'id_chat': activeChat?.id,
      'content': msg,
    });

    var response =
        await _net.request('/messages', requestMethod: 'post', data: formData);

    //activeChat.messages.add(MessageModel.fromJson(response.data['data']));

    addMessageToConversation(
        activeChat?.id, MessageModel.fromJson(response.data['data']));

    notifyListeners();
  }

  void addMessageToConversation(int chatId, MessageModel message) {
    var livechat = _chats.firstWhere((livechat) => livechat.id == chatId);
    livechat.messages.add(message);
    toTheTop(livechat);
    notifyListeners();
  }

  void toTheTop(ChatModel chat) {
    var index = _chats.indexOf(chat);

    for (var i = index; i > 0; i--) {
      var x = _chats[i];
      _chats[i] = _chats[i - 1];
      _chats[i - 1] = x;
    }
  }
}
