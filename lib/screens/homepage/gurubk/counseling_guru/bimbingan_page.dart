import 'dart:convert';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/chat_page.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/chatting_screen.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/models/chat_model.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/models/message_model.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/view_models/chat_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/user_vm.dart';
import 'package:bubble_bottom_bar/bubble_bottom_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:flutter/scheduler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:get/route_manager.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class ChatGuru extends StatefulWidget {
  @override
  _ChatGuruState createState() => _ChatGuruState();
}

class _ChatGuruState extends State<ChatGuru> {
  int currentIndex = 0;
  final PageController _pageController = new PageController();
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context
          .read<ChatViewModel>()
          .setNetworkService(context.read<NetworkService>());
      context
          .read<UserViewModel>()
          .setNetworkService(context.read<NetworkService>());
      _firebaseMessaging.getToken().then((token) {
        print(token);
        context.read<UserViewModel>().setFcmToken();
      });
      notification();
    });
  }

  notification() {
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        //handleNotification(message['data'], false);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
      },
    );
  }

  handleNotification(data, bool push) {
    var messageJson = json.decode(data['message']);
    var message = MessageModel.fromJson(messageJson);
    context.read<ChatViewModel>().addMessageToConversation(
        context.read<ChatViewModel>().activeChat.id, message);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Bimbingan',
          style: CustomFont.appBar,
        ),
        centerTitle: true,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: _pageController,
        children: <Widget>[
          ChatPage(),
          Container(
            color: CustomColor.graybg,
          ),
          ChattingScreen(),
          Container(
            color: CustomColor.graybg,
          ),
        ],
      ),
      // bottomNavigationBar: BubbleBottomBar(
      //   opacity: .2,
      //   currentIndex: currentIndex,
      //   backgroundColor: CustomColor.graybg,
      //   onTap: (i) {
      //     setState(() {
      //       currentIndex = i;
      //       _pageController.animateToPage(i,
      //           duration: kTabScrollDuration, curve: Curves.easeIn);
      //     });
      //   },
      //   elevation: 8,
      //   inkColor: Colors.black12, //optional, uses theme color if not specified
      //   items: <BubbleBottomBarItem>[
      //     BubbleBottomBarItem(
      //         backgroundColor: CustomColor.themedarker,
      //         icon: Icon(
      //           Icons.chat_bubble_outline,
      //           color: Colors.black,
      //         ),
      //         activeIcon: Icon(Icons.chat_bubble_outline,
      //             color: CustomColor.themedarker),
      //         title: Text(
      //           "Home",
      //           style: TextStyle(color: Colors.black),
      //         )),
      //     BubbleBottomBarItem(
      //       backgroundColor: CustomColor.themedarker,
      //       icon: Icon(
      //         Icons.people_outline,
      //         color: Colors.black,
      //       ),
      //       activeIcon: Icon(
      //         Icons.people_outline,
      //         color: CustomColor.themedarker,
      //       ),
      //       title: Text(
      //         "Chats",
      //         style: TextStyle(color: Colors.black),
      //       ),
      //     ),
      //   ],
      // ),
    );
  }
}
