import 'dart:convert';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/models/chat_model.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/models/message_model.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/view_models/chat_vm.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/widget.dart/conversation_card.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/widget.dart/friend_message_card.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/widget.dart/my_message_card.dart';
import 'package:bk_mobile_app/styles/size_config.dart';
import 'package:bubble_bottom_bar/bubble_bottom_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:flutter/scheduler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:get/route_manager.dart';

class ChattingScreen extends StatefulWidget {
  final ChatModel chatting;
  const ChattingScreen({Key key, this.chatting}) : super(key: key);
  @override
  _ChattingScreenState createState() => _ChattingScreenState(this.chatting);
}

class _ChattingScreenState extends State<ChattingScreen> {
  TextEditingController messageTextEditController = TextEditingController();

  final ChatModel chatting;
  MessageModel message;
  ScrollController _scrollController = ScrollController();

  _ChattingScreenState(this.chatting);

  @override
  Widget build(BuildContext context) {
    @override
    void initState() {
      super.initState();
      message = MessageModel();
      message.id_chat = chatting.id;
      SchedulerBinding.instance.addPostFrameCallback((_) {
        context
            .read<ChatViewModel>()
            .setNetworkService(context.read<NetworkService>());
        context.read<ChatViewModel>().getChats();
        _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
      });
    }

    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          '${context.read<ChatViewModel>().activeChat.user.name}',
          style: CustomFont.appBar,
        ),
        centerTitle: true,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: false,
      ),
      body: Consumer<ChatViewModel>(
        builder: (_, ChatViewModel vm, __) => Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            const Padding(
              padding: EdgeInsets.only(top: 10.0),
            ),
            //for (var i = 0; i < vm.accountIds.length; i++)
            Expanded(
              child: ListView.builder(
                controller: _scrollController,
                padding: EdgeInsets.symmetric(
                    horizontal: SizeConfig.safeBlockHorizontal * 3,
                    vertical: SizeConfig.safeBlockVertical * 3),
                itemCount:
                    context.read<ChatViewModel>().activeChat.messages.length,
                itemBuilder: (BuildContext context, int index) =>
                    (vm.activeChat.messages[index]?.accountId ?? -1) ==
                            vm.activeChat.user.id
                        ? FriendMessageCard(
                            message: vm.activeChat.messages[index],
                          )
                        : MyMessageCard(
                            message: vm.activeChat.messages[index],
                          ),
              ),
            ),
            // FriendMessageCard(),
            // MyMessageCard(),
            Container(
              padding: EdgeInsets.all(12),
              margin: EdgeInsets.all(12),
              decoration: BoxDecoration(
                  border:
                      Border.all(width: 2.0, color: CustomColor.themedarker),
                  color: CustomColor.graybg,
                  borderRadius: BorderRadius.circular(32)),
              // color: CustomColor.mutedButton,
              // borderRadius: BorderRadius.circular(32)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: 12,
                  ),
                  Expanded(
                    child: TextField(
                      controller: messageTextEditController,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Tuliskan pesan Anda...',
                        hintStyle: TextStyle(color: CustomColor.mutedButton),
                      ),
                    ),
                  ),
                  vm.busy
                      ? CircularProgressIndicator()
                      : InkWell(
                          onTap: () async {
                            FocusScope.of(context).requestFocus(FocusNode());

                            if (messageTextEditController.text.isEmpty) return;
                            await vm.storeMessage(
                              messageTextEditController.text.trim(),
                            );

                            messageTextEditController.clear();
                            _scrollController.jumpTo(
                              _scrollController.position.maxScrollExtent + 23,
                            );
                          },

                          //MaterialButton(
                          child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: CustomColor.themelighter,
                            ),
                            child: Icon(
                              Icons.send,
                              size: 36,
                              color: CustomColor.graybg,
                            ),
                          ),
                          //onPressed: () => vm.storeMessage(),
                        ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
