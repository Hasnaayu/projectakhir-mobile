import 'dart:io';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/models/gurubk_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:get/route_manager.dart';
import 'package:image_picker/image_picker.dart';

class GurubkViewModel extends ChangeNotifier {
  GurubkViewModel();
  NetworkService _net;
  GurubkModel gurubk;

  List<GurubkModel> _fotos = [];
  List<GurubkModel> get fotos => _fotos;

  File foto;
  final picker = ImagePicker();

  void setNetworkService(NetworkService net) {
    _net = net;
  }

  Future<void> getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      foto = File(pickedFile.path);
    } else {
      print('No photo selected.');
    }
    print('Image Path $foto');
    notifyListeners();
  }

  Future<void> addFoto() async {
    var formData = FormData.fromMap({
      if (foto != null) 'foto': await MultipartFile.fromFile(foto.path),
    });

    var response = await _net.request('/siswa/storegurubk',
        requestMethod: 'post', data: formData);

    print(response);

    Get.back<void>();
    notifyListeners();
  }

  Future<void> fetchGurubk() async {
    final Response<dynamic> resp = await _net.request('/gurubk');
    final Map<String, dynamic> respData =
        resp.data['data'] as Map<String, dynamic>;
    gurubk = GurubkModel.fromJson(respData);
    print(respData);
    notifyListeners();
  }
}
