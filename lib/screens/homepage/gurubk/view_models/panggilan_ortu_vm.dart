import 'package:bk_mobile_app/auth/mixins/dialog_mixin.dart';
import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/models/panggilan_ortu_model.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/user_model.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/user_vm.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:intl/intl.dart';
import 'dart:io';

class PanggilanOrtuViewModel extends ChangeNotifier with DialogMixin {
  PanggilanOrtuViewModel();

  NetworkService _net;

  UserViewModel _user;
  UserModel usr;

  List<PanggilanOrtuModel> _panggilans = [];
  List<PanggilanOrtuModel> get panggilans => _panggilans;

  List<int> spanHours = [];
  List<int> spanDays = [];
  List<String> panggilanTopik = [];
  List<int> panggilanSpanHours = [];
  List<int> panggilanSpanDays = [];

  PanggilanOrtuModel _selectedPanggilan;
  PanggilanOrtuModel get selectedPanggilan => _selectedPanggilan;

  UserModel _users;
  UserModel get users => _users;

  DateTime tanggal;
  bool isDone;
  DateTime today = DateTime.now();
  String topik;
  String nama_siswa;

  String toStringDue() {
    return DateFormat('EEEEEE, d - M - y').format(today);
  }

  void setUserViewModel(UserViewModel user) {
    _user = user;
    notifyListeners();
  }

  void onTopikChange(String val) {
    topik = val;
    print(topik);
    notifyListeners();
  }

  void onNamaChange(String val) {
    nama_siswa = val;
    notifyListeners();
  }

  void setNetworkViewModel(NetworkService net) {
    _net = net;
    notifyListeners();
  }

  void onEditPanggilan(
    dynamic val,
  ) async {
    _selectedPanggilan = val;
    Get.toNamed<void>('/editpanggilan');
    notifyListeners();
  }

  void onPanggilanTapped(UserModel userr) async {
    usr = userr;
    print('findMe: ${userr.id} - ${userr.name}');
    print(usr.id);
    notifyListeners();
    Get.toNamed<void>('/addpanggilan');
  }

  Future<void> selectDate(BuildContext context) async {
    tanggal = await showDatePicker(
      context: context,
      initialDate: today,
      firstDate: DateTime.now(),
      lastDate: DateTime(3000),
    );
    if (tanggal != null && tanggal != today) today = tanggal;
    print(tanggal);
    notifyListeners();
  }

  Future<void> selectEditedDate(BuildContext context) async {
    tanggal = null;
    tanggal = await showDatePicker(
      context: context,
      initialDate: today,
      firstDate: DateTime.now(),
      lastDate: DateTime(3000),
    );
    if (tanggal != null && tanggal != today) today = tanggal;
    print(tanggal);
    notifyListeners();
  }

  Future<void> addNewPanggilan(int _id) async {
    print('findMe: ${_id}');
    var formData = FormData.fromMap({
      'tanggal': tanggal ?? null,
      'topik': topik,
      'nama_siswa': nama_siswa,
      'isDone': isDone,
      'id_siswa': _id,
    });
    var response = await _net.request('/storepanggilan',
        requestMethod: 'post', data: formData);
    print(response);
    if (response.data['success'] == true) {
      tanggal = null;
      topik = null;
      nama_siswa = null;
      isDone = null;
      Get.back<void>();
      showSuccessSnackbar('Sukses mengirim panggilan orang tua siswa.');
    } else
      showErrorSnackbar('Isi form panggilan orang tua siswa.');

    print(response);

    notifyListeners();
  }

  Future<void> fetchPanggilan(int id) async {
    final Response<dynamic> resp = await _net.request('/panggilan/$id');

    List<dynamic> listData = resp.data['data'];

    for (dynamic data in listData) {
      _panggilans
          .add(PanggilanOrtuModel.fromJson(data as Map<String, dynamic>));
    }

    if (panggilans.isNotEmpty) {
      if (panggilanTopik.isEmpty ||
          panggilanSpanDays.isEmpty ||
          panggilanSpanHours.isEmpty) {
        for (var i = 0; i < _panggilans.length; i++) {
          if (panggilans[i].isDone == null)
            panggilanTopik.add(_panggilans[i].topik);
          spanDays = [_panggilans[i].tanggal.difference(today).inDays];
          spanHours = [_panggilans[i].tanggal.difference(today).inHours];
          for (var i = 0; i < spanDays.length; i++) {
            panggilanSpanDays.add(spanDays[i]);
            panggilanSpanHours.add(spanHours[i]);
          }
        }
        showTargetReminderPopUp(
            panggilanTopik, panggilanSpanDays, panggilanSpanHours);
      }
    }
    print(resp);
    notifyListeners();
  }

  Future<void> removeEarlierPanggilan(int id) async {
    _panggilans.clear();
    print(_panggilans);
    fetchListPanggilan(id);
    notifyListeners();
  }

  Future<void> fetchListPanggilan(int id) async {
    if (_panggilans.isEmpty) {
      fetchPanggilan(id);
    } else {
      print('datas have been fetched.');
    }
    notifyListeners();
  }

  Future<void> addToDone(int id) async {
    var form = FormData.fromMap({
      'isDone': 1,
    });
    var response = await _net.request('/add_to_done/$id',
        requestMethod: 'post', data: form, queryParameter: {'_method': 'put'});
    print(response.data);

    notifyListeners();
  }

  Future<void> addToFails(int id) async {
    var form = FormData.fromMap({
      'isDone': 0,
    });
    var response = await _net.request('/add_to_fail/$id',
        requestMethod: 'post', data: form, queryParameter: {'_method': 'put'});
    print(response.data);

    notifyListeners();
  }

  Future<void> editPanggilan(int id) async {
    var form = FormData.fromMap({
      'tanggal': tanggal ?? selectedPanggilan.tanggal,
      'topik': selectedPanggilan?.topik,
      'isConfirmed': isDone,
    });
    var response = await _net.request('/updatepanggilan/$id',
        requestMethod: 'post', data: form, queryParameter: {'_method': 'put'});
    print(response.data);
    if (selectedPanggilan?.topik == null) {
      showErrorSnackbar('Isi topik untuk edit.');
    } else if (topik == selectedPanggilan.topik &&
        tanggal == selectedPanggilan.tanggal &&
        isDone == null) {
      topik = null;
      tanggal = null;
      isDone = null;

      Get.back<void>();
      showErrorSnackbar('Tidak terdapat perubahan penggilan orang tua siswa.');
    } else if (topik != selectedPanggilan.topik ||
        tanggal != selectedPanggilan.tanggal) {
      topik = null;
      tanggal = null;
      isDone = null;
      Get.back<void>();
      showSuccessSnackbar(
          'Berhasil membuat perubahan panggilan orang tua. Muat ulang untuk memperbarui data.');
    }

    //removeEarlierData();
    notifyListeners();
  }

  Future<void> removeEarlierEdit(int id) async {
    _panggilans.clear();
    print(_panggilans);
    fetchListEdit(id);
    notifyListeners();
  }

  Future<void> fetchListEdit(int id) async {
    if (_panggilans.isEmpty) {
      editPanggilan(id);
    } else {
      print('datas have been fetched.');
    }
    notifyListeners();
  }

  // Future<void> deletePanggilan(int id) async {
  //   final Response<dynamic> resp =
  //       await _net.request('/deletepanggilan/$id', requestMethod: 'delete');

  //   _panggilans.remove(id);

  //   showSuccessSnackbar(
  //       'Panggilan orang tua berhasil dihapus. Muat ulang untuk memperbarui data.');

  //   print(resp);
  //   //removeEarlierData();
  //   notifyListeners();
  // }

  Future<void> deletePanggilan(int id) async {
    print(id);
    final Response<dynamic> resp =
        await _net.request('/deletepanggilan/$id', requestMethod: 'delete');
    for (var i = 0; i < _panggilans.length; i++) {
      _panggilans.remove(_panggilans[i].id);
    }
    print(resp);
    notifyListeners();
  }

  Future<void> fetchPanggilanOrtu() async {
    final Response<dynamic> resp = await _net.request('/panggilanortu');
    List<dynamic> listData = resp.data['data'];
    for (dynamic data in listData)
      _panggilans
          .add(PanggilanOrtuModel.fromJson(data as Map<String, dynamic>));
    print(resp);
    notifyListeners();
  }

  Future<void> removeEarlierPanggilanOrtu() async {
    _panggilans.clear();
    print(_panggilans);
    fetchListPanggilanOrtu();
    notifyListeners();
  }

  Future<void> fetchListPanggilanOrtu() async {
    if (_panggilans.isEmpty) {
      fetchPanggilanOrtu();
    } else {
      print('datas have been fetched.');
    }
    notifyListeners();
  }
}
