import 'dart:io';

import 'package:bk_mobile_app/auth/mixins/dialog_mixin.dart';
import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/models/pelanggaran_chart_model.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/models/pelanggaran_model.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/kelas_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/siswa_model.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/siswa_vm.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:intl/intl.dart';

class PelanggaranViewModel extends ChangeNotifier with DialogMixin {
  PelanggaranViewModel();
  NetworkService _net;
  PelanggaranModel pelanggaranSiswa;
  PelanggaranViewModel _pel;

  List<PelanggaranModel> _pelanggarans = [];
  List<PelanggaranModel> get pelanggarans => _pelanggarans;

  PelanggaranModel _selectedPelanggaran;
  PelanggaranModel get selectedPelanggaran => _selectedPelanggaran;

  List<PelanggaranChartModel> _pelanggaran_charts = [];
  List<PelanggaranChartModel> get pelanggaran_charts => _pelanggaran_charts;

  SiswaViewModel svm;

  KelasViewModel kcm;

  String pelanggaran;
  String tindak_lanjut;
  int id;
  int year;

  DateTime tanggal_pelanggaran;
  DateTime today = DateTime.now();

  final years = List<int>.generate(
    5,
    (i) => i += (DateTime.now().year) - 2,
  );

  String toStringDue() {
    return DateFormat('EEEEEE, d - M - y').format(today);
  }

  void setNetworkService(NetworkService net) {
    _net = net;
    notifyListeners();
  }

  void setPelanggaranViewModel(PelanggaranViewModel pel) {
    _pel = pel;
    notifyListeners();
  }

  void setUserViewModel(SiswaViewModel siswa) {
    svm = siswa;
    notifyListeners();
  }

  void onYearChange(
    dynamic val,
  ) async {
    year = val;
    Get.back<void>();
    await getCharts(year);
    notifyListeners();
  }

  void onPelanggaranChange(String val) {
    pelanggaran = val;
    notifyListeners();
  }

  void onTindakanChange(String val) {
    tindak_lanjut = val;
    notifyListeners();
  }

  void onId(dynamic val) {
    id = val;
    notifyListeners();
  }

  void onEditPelanggaranChange(
    dynamic val,
  ) async {
    _selectedPelanggaran = val;
    Get.toNamed<void>('/editpelanggaran');
    notifyListeners();
  }

  Future<void> selectDate(BuildContext context) async {
    tanggal_pelanggaran = await showDatePicker(
      context: context,
      initialDate: today,
      firstDate: DateTime.now(),
      lastDate: DateTime(3000),
    );
    if (tanggal_pelanggaran != null && tanggal_pelanggaran != today)
      today = tanggal_pelanggaran;
    print(tanggal_pelanggaran);
    notifyListeners();
  }

  Future<void> selectEditedDate(BuildContext context) async {
    tanggal_pelanggaran = null;
    tanggal_pelanggaran = await showDatePicker(
      context: context,
      initialDate: today,
      firstDate: DateTime.now(),
      lastDate: DateTime(3000),
    );
    if (tanggal_pelanggaran != null && tanggal_pelanggaran != today)
      today = tanggal_pelanggaran;
    print(tanggal_pelanggaran);
    notifyListeners();
  }

  Future<void> addNewPelanggaran(int _id) async {
    print('findMe: ${_id}');
    var formData = FormData.fromMap({
      'tanggal_pelanggaran': tanggal_pelanggaran,
      'pelanggaran': pelanggaran,
      'tindak_lanjut': tindak_lanjut,
      'id_siswa': _id,
    });
    var response = await _net.request('/pelanggaran/storepelanggaran',
        requestMethod: 'post', data: formData);

    print(response);
    if (response.data['success'] == true) {
      tanggal_pelanggaran = null;
      pelanggaran = null;
      tindak_lanjut = null;
      Get.back<void>();
      showSuccessSnackbar('Catatan pelanggaran berhasil ditambahkan.');
    } else {
      showErrorSnackbar('Isi form catatan pelanggaran.');
    }
    removeEarlierData(id);

    notifyListeners();
  }

  Future<void> fetchPelanggaran(int id) async {
    final Response<dynamic> resp = await _net.request('/pelanggaran/$id');
    _pelanggarans.clear();
    List<dynamic> listData = resp.data['data'];
    for (dynamic data in listData)
      _pelanggarans
          .add(PelanggaranModel.fromJson(data as Map<String, dynamic>));
    print(resp);
    notifyListeners();
  }

  Future<void> fetchPelanggaranSiswa() async {
    final Response<dynamic> resp = await _net.request('/pelanggaransiswa');
    _pelanggarans.clear();
    List<dynamic> listData = resp.data['data'];
    for (dynamic data in listData)
      _pelanggarans
          .add(PelanggaranModel.fromJson(data as Map<String, dynamic>));
    print(resp);
    notifyListeners();
  }

  Future<void> fetchLaporanOrtu() async {
    final Response<dynamic> resp = await _net.request('/pelanggaranortu');
    _pelanggarans.clear();
    List<dynamic> listData = resp.data['data'];
    for (dynamic data in listData)
      _pelanggarans
          .add(PelanggaranModel.fromJson(data as Map<String, dynamic>));
    print(resp);
    notifyListeners();
  }

  Future<void> deletePelanggaran(int id) async {
    print(id);
    final Response<dynamic> resp = await _net.request(
        '/pelanggaran/deletepelanggaran/data/$id',
        requestMethod: 'delete');
    for (var i = 0; i < _pelanggarans.length; i++) {
      _pelanggarans.remove(_pelanggarans[i].id);
    }
    removeEarlierData(id);
    print(resp);
  }

  Future<void> editPelanggaran(int id) async {
    var form = FormData.fromMap({
      'tanggal_pelanggaran':
          tanggal_pelanggaran ?? selectedPelanggaran.tanggal_pelanggaran,
      'pelanggaran': pelanggaran ?? selectedPelanggaran.pelanggaran,
      'tindak_lanjut': tindak_lanjut ?? selectedPelanggaran.tindak_lanjut,
    });
    print('Sending $form');
    var response = await _net.request('/pelanggaran/updatepelanggaran/$id',
        requestMethod: 'post', data: form, queryParameter: {'_method': 'put'});
    print(response.data);
    if (selectedPelanggaran?.pelanggaran == null ||
        selectedPelanggaran?.tindak_lanjut == null) {
      showErrorSnackbar('Isi form untuk edit.');
    } else if (tanggal_pelanggaran == selectedPelanggaran.tanggal_pelanggaran ||
        pelanggaran == selectedPelanggaran.pelanggaran ||
        tindak_lanjut == selectedPelanggaran.tindak_lanjut) {
      tanggal_pelanggaran = null;
      pelanggaran = null;
      tindak_lanjut = null;
      Get.back<void>();
      showErrorSnackbar('Tidak terdapat perubahan jadwal.');
    } else if (tanggal_pelanggaran != selectedPelanggaran.tanggal_pelanggaran ||
        pelanggaran != selectedPelanggaran.pelanggaran ||
        tindak_lanjut != selectedPelanggaran.tindak_lanjut) {
      tanggal_pelanggaran = null;
      pelanggaran = null;
      tindak_lanjut = null;
      Get.back<void>();
      showSuccessSnackbar(
          'Berhasil membuat perubahan jadwal. Muat ulang untuk memperbarui data.');
    }
    //removeEarlierData(id);
    notifyListeners();
  }

  Future<void> removeEarlierData(int id) async {
    _pelanggarans.clear();
    print(_pelanggarans);
    fetchDataList(id);
    notifyListeners();
  }

  Future<void> fetchDataList(int id) async {
    if (_pelanggarans.isEmpty) {
      fetchPelanggaran(id);
    } else {
      print('datas have been fetched.');
    }
    notifyListeners();
  }

  Future<void> removeData() async {
    _pelanggarans.clear();
    print(_pelanggarans);
    fetchDataPelanggaran();
    notifyListeners();
  }

  Future<void> fetchDataPelanggaran() async {
    if (_pelanggarans.isEmpty) {
      fetchPelanggaranSiswa();
    } else {
      print('datas have been fetched.');
    }
    notifyListeners();
  }

  Future<void> removeLaporan() async {
    _pelanggarans.clear();
    print(_pelanggarans);
    fetchDataLaporan();
    notifyListeners();
  }

  Future<void> fetchDataLaporan() async {
    if (_pelanggarans.isEmpty) {
      fetchLaporanOrtu();
    } else {
      print('datas have been fetched.');
    }
    notifyListeners();
  }

  Future<void> getCharts(int year) async {
    final Response<dynamic> resp =
        await _net.request('/pelanggaran/ortu/grafik/siswa/$year');
    final List<dynamic> respData = resp.data['data'] as List<dynamic>;

    if (respData != null) {
      for (final dynamic respData in respData) {
        _pelanggaran_charts.add(
            PelanggaranChartModel.fromJson(respData as Map<String, dynamic>));
      }
    }

    await removeData();
    print(resp);
    notifyListeners();
  }
}
