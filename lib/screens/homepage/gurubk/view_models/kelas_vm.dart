import 'dart:io';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/models/kelas_model.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/models/pelanggaran_model.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/siswa_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:get/route_manager.dart';
import 'package:image_picker/image_picker.dart';

class KelasViewModel extends ChangeNotifier {
  KelasViewModel();
  NetworkService _net;
  KelasModel kelasSiswa;
  SiswaModel siswa;

  List<KelasModel> _kelass = [];
  List<KelasModel> get kelass => _kelass;
  final List<SiswaModel> namaSiswaDikelas = <SiswaModel>[];
  SiswaModel selectedSiswaModel;

  String kelas;
  String jurusan;

  void setNetworkService(NetworkService net) {
    _net = net;
    notifyListeners();
  }

  void listNamaSiswa(KelasModel sm) async {
    kelasSiswa = sm;
    // print(siswa);
    Get.toNamed<void>('/listsiswa');
    notifyListeners();
  }

//#1
  Future<void> lihatSiswa(int id) async {
    final Response<dynamic> resp = await _net.request('/kelas/$id');
    namaSiswaDikelas.clear();
    List<dynamic> listData = resp.data['data'];
    for (dynamic data in listData)
      namaSiswaDikelas.add(SiswaModel.fromJson(data));
    //print(resp);
    Get.toNamed<void>('/listsiswa');
    notifyListeners();
  }

//#2
  Future<void> lihatSiswaBimbingan(int id) async {
    final Response<dynamic> resp = await _net.request('/kelas/$id');
    namaSiswaDikelas.clear();
    List<dynamic> listData = resp.data['data'];
    for (dynamic data in listData)
      namaSiswaDikelas.add(SiswaModel.fromJson(data));
    //print(resp);
    Get.toNamed<void>('/listsiswabimbingan');
    notifyListeners();
  }

//#3
  Future<void> lihatSiswaAum(int id) async {
    final Response<dynamic> resp = await _net.request('/kelas/$id');
    namaSiswaDikelas.clear();
    List<dynamic> listData = resp.data['data'];
    for (dynamic data in listData)
      namaSiswaDikelas.add(SiswaModel.fromJson(data));
    //print(resp);
    Get.toNamed<void>('/listsiswaaum');
    notifyListeners();
  }

  //#2
  Future<void> lihatSiswaPanggilan(int id) async {
    final Response<dynamic> resp = await _net.request('/kelas/$id');
    namaSiswaDikelas.clear();
    List<dynamic> listData = resp.data['data'];
    for (dynamic data in listData)
      namaSiswaDikelas.add(SiswaModel.fromJson(data));
    //print(resp);
    Get.toNamed<void>('/listsiswapanggilan');
    notifyListeners();
  }

//#1
  void onSiswaTapped(SiswaModel siswa) async {
    selectedSiswaModel = siswa;
    print('findMe: ${siswa.id} - ${siswa.name}');
    print(selectedSiswaModel.id);
    notifyListeners();
    Get.toNamed('/createpelanggaran');
  }

//#2
  void onSiswaTappedBim(SiswaModel siswa) async {
    selectedSiswaModel = siswa;
    print('findMe: ${siswa.id} - ${siswa.name}');
    print(selectedSiswaModel.id);
    notifyListeners();
    Get.toNamed('/createbimbingan');
  }

//#3
  void onSiswaTappedAum(SiswaModel siswa) async {
    selectedSiswaModel = siswa;
    print('findMe: ${siswa.id} - ${siswa.name}');
    print(selectedSiswaModel.id);
    notifyListeners();
    Get.toNamed('/resultaum');
  }

  //#4
  void onSiswaTappedPanggilan(SiswaModel siswa) async {
    selectedSiswaModel = siswa;
    print('findMe: ${siswa.id} - ${siswa.name}');
    print(selectedSiswaModel.id);
    notifyListeners();
    Get.toNamed('/createpanggilan');
  }

  Future<void> fetchNamaSiswa(int id) async {
    //print(id);
    final Response<dynamic> resp = await _net.request('/kelas/$id');
    List<dynamic> listData = resp.data['data'];
    _kelass.clear();
    for (dynamic data in listData)
      _kelass.add(KelasModel.fromJson(data as Map<String, dynamic>));
    print(resp);
    notifyListeners();
  }

  Future<void> fetchListKelas() async {
    final Response<dynamic> resp = await _net.request('/kelas/listkelas');
    // List<Map<String, Object>> listData = resp.data['data'];
    List<dynamic> listData = resp.data['data'];
    _kelass.clear();
    for (dynamic data in listData)
      _kelass.add(KelasModel.fromJson(data as Map<String, dynamic>));
    print(listData);
    notifyListeners();
  }

  Future<void> removeEarlierData() async {
    _kelass.clear();
    print(_kelass);
    await fetchDataList();
    notifyListeners();
  }

  Future<void> fetchDataList() async {
    if (_kelass.isEmpty) {
      await fetchListKelas();
    } else {
      print('datas have been fetched.');
    }
    notifyListeners();
  }

  // Future<void> removeEarlierNama() async {
  //   _kelass.clear();
  //   print(_kelass);
  //   fetchDataList();
  //   notifyListeners();
  // }

  // Future<void> fetchDataNama() async {
  //   if (_kelass.isEmpty) {
  //     fetchNamaSiswa();
  //   } else {
  //     print('datas have been fetched.');
  //   }
  //   notifyListeners();
  // }
}
