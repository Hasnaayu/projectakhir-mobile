import 'dart:io';

import 'package:bk_mobile_app/auth/mixins/dialog_mixin.dart';
import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/models/bimbingan_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:intl/intl.dart';

class BimbinganViewModel extends ChangeNotifier with DialogMixin {
  BimbinganViewModel();
  NetworkService _net;
  BimbinganModel bimbinganSiswa;
  BimbinganViewModel _bim;

  List<BimbinganModel> _bimbingans = [];
  List<BimbinganModel> get bimbingans => _bimbingans;

  BimbinganModel _selectedBimbingan;
  BimbinganModel get selectedBimbingan => _selectedBimbingan;

  String permasalahan;
  String solusi;
  int id;

  DateTime tanggal_bimbingan;
  DateTime today = DateTime.now();

  String toStringDue() {
    return DateFormat('EEEEEE, d - M - y').format(today);
  }

  void setNetworkService(NetworkService net) {
    _net = net;
    notifyListeners();
  }

  void setBimbinganViewModel(BimbinganViewModel bim) {
    _bim = bim;
    notifyListeners();
  }

  // void onDateChange(String val) {
  //   tanggal_bimbingan = val;
  //   notifyListeners();
  // }

  void onPermasalahanChange(String val) {
    permasalahan = val;
    print(permasalahan);
    notifyListeners();
  }

  void onSolusiChange(String val) {
    solusi = val;
    print(solusi);
    notifyListeners();
  }

  void onEditBimbinganChange(
    dynamic val,
  ) async {
    _selectedBimbingan = val;
    Get.toNamed<void>('/editbimbingan');
    notifyListeners();
  }

  Future<void> selectDate(BuildContext context) async {
    tanggal_bimbingan = await showDatePicker(
      context: context,
      initialDate: today,
      firstDate: DateTime.now(),
      lastDate: DateTime(3000),
    );
    if (tanggal_bimbingan != null && tanggal_bimbingan != today)
      today = tanggal_bimbingan;
    print(tanggal_bimbingan);
    notifyListeners();
  }

  Future<void> selectEditedDate(BuildContext context) async {
    tanggal_bimbingan = null;
    tanggal_bimbingan = await showDatePicker(
      context: context,
      initialDate: today,
      firstDate: DateTime.now(),
      lastDate: DateTime(3000),
    );
    if (tanggal_bimbingan != null && tanggal_bimbingan != today)
      today = tanggal_bimbingan;
    print(tanggal_bimbingan);
    notifyListeners();
  }

  Future<void> fetchBimbingan(int id) async {
    final Response<dynamic> resp = await _net.request('/bimbingan/$id');
    _bimbingans.clear();
    List<dynamic> listData = resp.data['data'];
    for (dynamic data in listData)
      _bimbingans.add(BimbinganModel.fromJson(data as Map<String, dynamic>));
    print(resp);
    notifyListeners();
  }

  Future<void> fetchBimbinganSiswa() async {
    final Response<dynamic> resp = await _net.request('/bimbingansiswa');
    _bimbingans.clear();
    List<dynamic> listData = resp.data['data'];
    for (dynamic data in listData)
      _bimbingans.add(BimbinganModel.fromJson(data as Map<String, dynamic>));
    print(resp);
    notifyListeners();
  }

  Future<void> addNewBimbingan(int _id) async {
    print('findMe: ${_id}');
    var formData = FormData.fromMap({
      'tanggal_bimbingan': tanggal_bimbingan,
      'permasalahan': permasalahan,
      'solusi': solusi,
      'id_siswa': _id,
    });
    var response = await _net.request('/bimbingan/storebimbingan',
        requestMethod: 'post', data: formData);

    print(response);
    if (response.data['success'] == true) {
      tanggal_bimbingan = null;
      permasalahan = null;
      solusi = null;
      Get.back<void>();
      showSuccessSnackbar('Catatan bimbingan berhasil ditambahkan.');
    } else {
      showErrorSnackbar('Isi form catatan bimbingan.');
    }
    removeEarlierData(id);
    notifyListeners();
  }

  Future<void> deleteBimbingan(int id) async {
    print(id);
    final Response<dynamic> resp = await _net.request(
        '/bimbingan/deletebimbingan/data/$id',
        requestMethod: 'delete');
    for (var i = 0; i < _bimbingans.length; i++) {
      _bimbingans.remove(_bimbingans[i].id);
    }
    removeEarlierData(id);
    print(resp);
  }

  Future<void> editBimbingan(int id) async {
    var form = FormData.fromMap({
      'tanggal_bimbingan':
          tanggal_bimbingan ?? selectedBimbingan.tanggal_bimbingan,
      'permasalahan': permasalahan ?? selectedBimbingan?.permasalahan,
      'solusi': solusi ?? selectedBimbingan?.solusi,
    });
    print('Sending $form');
    var response = await _net.request('/bimbingan/updatebimbingan/$id',
        requestMethod: 'post', data: form, queryParameter: {'_method': 'put'});
    print(response.data);
    if (selectedBimbingan?.permasalahan == null ||
        selectedBimbingan?.solusi == null) {
      showErrorSnackbar('Isi form untuk edit.');
    } else if (tanggal_bimbingan == selectedBimbingan.tanggal_bimbingan ||
        permasalahan == selectedBimbingan.permasalahan ||
        solusi == selectedBimbingan.solusi) {
      tanggal_bimbingan = null;
      permasalahan = null;
      solusi = null;
      Get.back<void>();
      showErrorSnackbar('Tidak terdapat perubahan jadwal.');
    } else if (tanggal_bimbingan != selectedBimbingan.tanggal_bimbingan ||
        permasalahan != selectedBimbingan.permasalahan ||
        solusi != selectedBimbingan.solusi) {
      tanggal_bimbingan = null;
      permasalahan = null;
      solusi = null;
      Get.back<void>();
      showSuccessSnackbar(
          'Berhasil membuat perubahan jadwal. Muat ulang untuk memperbarui data.');
    }
    //removeEarlierData(id);
    notifyListeners();
  }

  Future<void> removeEarlierData(int id) async {
    _bimbingans.clear();
    print(_bimbingans);
    fetchDataList(id);
    notifyListeners();
  }

  Future<void> fetchDataList(int id) async {
    if (_bimbingans.isEmpty) {
      fetchBimbingan(id);
    } else {
      print('datas have been fetched.');
    }
    notifyListeners();
  }

  Future<void> removeData() async {
    _bimbingans.clear();
    print(_bimbingans);
    fetchDataBimbingan();
    notifyListeners();
  }

  Future<void> fetchDataBimbingan() async {
    if (_bimbingans.isEmpty) {
      fetchBimbinganSiswa();
    } else {
      print('datas have been fetched.');
    }
    notifyListeners();
  }
}
