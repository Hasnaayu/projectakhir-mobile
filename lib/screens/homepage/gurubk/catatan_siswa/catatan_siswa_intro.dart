import 'dart:convert';

import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/list_kelas.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/list_kelas_bimbingan.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/pilih_kelas.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:get/route_manager.dart';

class CatatanSiswa extends StatefulWidget {
  @override
  _CatatanSiswaState createState() => _CatatanSiswaState();
}

class _CatatanSiswaState extends State<CatatanSiswa> {
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Catatan Siswa',
          style: CustomFont.appBar,
        ),
        centerTitle: true,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(16),
              child: Card(
                color: CustomColor.whitebg,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Center(
                      child: Padding(
                        padding: EdgeInsets.all(10),
                        child: Image(
                          image: CustomImage.pelanggaran,
                          width: 280,
                          height: 140,
                        ),
                      ),
                    ),
                    Center(
                      child: MaterialButton(
                        minWidth: 120.0,
                        height: 40.0,
                        elevation: 10,
                        onPressed: () => Get.toNamed('/listkelas'),
                        color: CustomColor.themelighter,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        child: Text(
                          'Catatan Pelanggaran',
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(16),
              child: Card(
                color: CustomColor.whitebg,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Center(
                      child: Padding(
                        padding: EdgeInsets.all(10),
                        child: Image(
                          image: CustomImage.bimbingan,
                          width: 360,
                          height: 180,
                        ),
                      ),
                    ),
                    Center(
                      child: MaterialButton(
                        minWidth: 120.0,
                        height: 40.0,
                        elevation: 10,
                        onPressed: () => Get.toNamed('/listkelasbimbingan'),
                        color: CustomColor.themelighter,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        child: Text(
                          'Catatan Bimbingan',
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(16),
              child: Card(
                color: CustomColor.whitebg,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Center(
                      child: Padding(
                        padding: EdgeInsets.all(10),
                        child: Image(
                          image: CustomImage.guru2,
                          width: 360,
                          height: 180,
                        ),
                      ),
                    ),
                    Center(
                      child: MaterialButton(
                        minWidth: 120.0,
                        height: 40.0,
                        elevation: 10,
                        onPressed: () => Get.toNamed('/listkelaspanggilan'),
                        color: CustomColor.themelighter,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        child: Text(
                          'Panggilan Orang Tua',
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
