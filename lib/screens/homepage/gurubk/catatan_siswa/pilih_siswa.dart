import 'dart:convert';

import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/create_pelanggaran.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/pilih_kelas.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/screens/welcome/login.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PilihSiswa extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _PilihSiswaState();
  }
}

class _PilihSiswaState extends State<PilihSiswa> {
  ScrollController _controller;

  String name;

  void _createPelanggaran() {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => CreatePelanggaran()),
    );
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Catatan Siswa',
          style: CustomFont.appBar,
        ),
        centerTitle: true,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: false,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(
              context,
              MaterialPageRoute(builder: (context) => PilihKelas()),
            );
          },
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        controller: _controller,
        children: ListTile.divideTiles(
          context: context,
          tiles: [
            //Padding(padding: EdgeInsets.only(top: 30.0)),
            Container(
              padding: EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                  color: CustomColor.whitebg,
                  border: Border(
                      top: BorderSide(
                          color: CustomColor.mutedButton.withOpacity(0.3)),
                      bottom: BorderSide(
                          color: CustomColor.mutedButton.withOpacity(0.3)))),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  // Padding(
                  //   padding: const EdgeInsets.all(15.0),
                  //   child: Image(
                  //     image: CustomImage.profileNav,
                  //     width: 100.0,
                  //     height: 100.0,
                  //   ),
                  // ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 25.0),
                    child: Text('Pilih Siswa',
                        style: TextStyle(
                            fontSize: 30, color: CustomColor.mutedText)),
                  ),
                ],
              ),
            ),
            Padding(padding: EdgeInsets.only(top: 30.0)),
            Container(
              padding: EdgeInsets.all(5.0),
              decoration: BoxDecoration(
                  color: CustomColor.whitebg,
                  border: Border(
                      top: BorderSide(
                          color: CustomColor.mutedButton.withOpacity(0.3)),
                      bottom: BorderSide(
                          color: CustomColor.mutedButton.withOpacity(0.3)))),
              child: ListTile(
                tileColor: CustomColor.whitebg,
                // leading: Image(
                //     image: CustomImage.nisn,
                //     height: 35,
                //     width: 35,
                //     color: CustomColor.mutedButton),
                title: Text(
                  '1. Zefanya User',
                  style: CustomFont.smallMuted,
                ),
                onTap: _createPelanggaran,
              ),
            ),
            Container(
              padding: EdgeInsets.all(5.0),
              decoration: BoxDecoration(
                  color: CustomColor.whitebg,
                  border: Border(
                      top: BorderSide(
                          color: CustomColor.mutedButton.withOpacity(0.3)),
                      bottom: BorderSide(
                          color: CustomColor.mutedButton.withOpacity(0.3)))),
              child: ListTile(
                tileColor: CustomColor.whitebg,
                // leading: Image(
                //     image: CustomImage.kelas,
                //     height: 35,
                //     width: 35,
                //     color: CustomColor.mutedButton),
                title: Text(
                  '2. Reza',
                  style: CustomFont.smallMuted,
                ),
                // subtitle: Text('X Multimedia 1'),
              ),
            ),
            Container(
              padding: EdgeInsets.all(5.0),
              decoration: BoxDecoration(
                  color: CustomColor.whitebg,
                  border: Border(
                      top: BorderSide(
                          color: CustomColor.mutedButton.withOpacity(0.3)),
                      bottom: BorderSide(
                          color: CustomColor.mutedButton.withOpacity(0.3)))),
              child: ListTile(
                tileColor: CustomColor.whitebg,
                // leading: Image(
                //     image: CustomImage.email,
                //     height: 35,
                //     width: 35,
                //     color: CustomColor.mutedButton),
                title: Text(
                  '3. Dinda',
                  style: CustomFont.smallMuted,
                ),
                // subtitle: Text('zefanya@gmail.com'),
              ),
            ),
            Container(
              padding: EdgeInsets.all(5.0),
              decoration: BoxDecoration(
                  color: CustomColor.whitebg,
                  border: Border(
                      top: BorderSide(
                          color: CustomColor.mutedButton.withOpacity(0.3)),
                      bottom: BorderSide(
                          color: CustomColor.mutedButton.withOpacity(0.3)))),
              child: ListTile(
                tileColor: CustomColor.whitebg,
                // leading: Image(
                //     image: CustomImage.address,
                //     height: 35,
                //     width: 35,
                //     color: CustomColor.mutedButton),
                title: Text(
                  '4. Amelia',
                  style: CustomFont.smallMuted,
                ),
                // subtitle: Text('Jalan Garuda 91, Magetan'),
              ),
            ),
            Container(
              padding: EdgeInsets.all(5.0),
              decoration: BoxDecoration(
                  color: CustomColor.whitebg,
                  border: Border(
                      top: BorderSide(
                          color: CustomColor.mutedButton.withOpacity(0.3)),
                      bottom: BorderSide(
                          color: CustomColor.mutedButton.withOpacity(0.3)))),
              child: ListTile(
                tileColor: CustomColor.whitebg,
                // leading: Image(
                //     image: CustomImage.changepw,
                //     height: 35,
                //     width: 35,
                //     color: CustomColor.mutedButton),
                title: Text(
                  '5. Zefanya',
                  style: CustomFont.smallMuted,
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(0, 5, 5, 5),
              decoration: BoxDecoration(
                  color: CustomColor.whitebg,
                  border: Border(
                      top: BorderSide(
                          color: CustomColor.mutedButton.withOpacity(0.3)),
                      bottom: BorderSide(
                          color: CustomColor.mutedButton.withOpacity(0.3)))),
              child: ListTile(
                tileColor: CustomColor.whitebg,
                // leading: Image(
                //     image: CustomImage.logout,
                //     height: 35,
                //     width: 35,
                //     color: CustomColor.mutedButton),
                title: Text(
                  '6. Angga',
                  style: CustomFont.smallMuted,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
            ),
          ],
        ).toList(),
      ),
    );
  }
}
