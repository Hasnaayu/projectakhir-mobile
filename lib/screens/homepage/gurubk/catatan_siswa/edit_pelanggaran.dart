import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/pelanggaran_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/will_pop_vm.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';

class EditPelanggaran extends StatefulWidget {
  @override
  _EditPelanggaranState createState() => _EditPelanggaranState();
}

class _EditPelanggaranState extends State<EditPelanggaran> {
  @override
  void initState() {
    super.initState();
    pelanggaranPelanggaranController = TextEditingController();
    pelanggaranTindakanController = TextEditingController();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      final PelanggaranViewModel vm = context.read<PelanggaranViewModel>();
      pelanggaranPelanggaranController.text =
          vm.selectedPelanggaran.pelanggaran ?? '';
      pelanggaranTindakanController.text =
          vm.selectedPelanggaran.tindak_lanjut ?? '';

      context
          .read<PelanggaranViewModel>()
          .setNetworkService(context.read<NetworkService>());
    });
  }

  @override
  void dispose() {
    super.dispose();
    pelanggaranPelanggaranController.dispose();
    pelanggaranTindakanController.dispose();
  }

  TextEditingController pelanggaranPelanggaranController;
  TextEditingController pelanggaranTindakanController;

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Consumer<PelanggaranViewModel>(
      builder: (_, PelanggaranViewModel _vm, __) => WillPopScope(
        onWillPop: () => context.read<WillPopViewModel>().onGoBack(),
        child: Scaffold(
          appBar: AppBar(
            title: Text(
              'Edit Catatan Pelanggaran',
              style: CustomFont.appBar,
            ),
            centerTitle: true,
            backgroundColor: CustomColor.themedarker,
            automaticallyImplyLeading: false,
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.pop(context),
            ),
          ),
          body: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Consumer<PelanggaranViewModel>(
              builder: (_, PelanggaranViewModel vm, __) => ConstrainedBox(
                constraints: BoxConstraints(maxHeight: size.height / 1.2),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    const Padding(
                      padding: EdgeInsets.only(top: 30.0),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            ListTile(
                              tileColor: CustomColor.whitebg,
                              leading: Icon(
                                Icons.calendar_today,
                                size: 35,
                              ),
                              onTap: () => vm.selectDate(context),
                              title: Row(
                                children: [
                                  if (vm.tanggal_pelanggaran != null &&
                                      vm?.selectedPelanggaran
                                              ?.tanggal_pelanggaran !=
                                          null)
                                    Text(
                                      vm.toStringDue(),
                                      style: CustomFont.noticeText,
                                    )
                                  else if (vm?.selectedPelanggaran
                                          ?.toStringDue() !=
                                      vm.toStringDue())
                                    Text(
                                      vm?.selectedPelanggaran?.toStringDue(),
                                      style: CustomFont.noticeText,
                                    )
                                  else
                                    Text(
                                      vm?.selectedPelanggaran?.toStringDue(),
                                      style: CustomFont.noticeText,
                                    ),
                                ],
                              ),
                            ),
                            Divider(
                              color: CustomColor.mutedButton.withOpacity(0.5),
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 20.0),
                                  child: Text(
                                    'Pelanggaran',
                                    style: CustomFont.basicText,
                                  ),
                                ),
                                Container(
                                  child: TextFormField(
                                    keyboardType: TextInputType.text,
                                    maxLines: 1,
                                    maxLength: 30,
                                    autofocus: false,
                                    controller:
                                        pelanggaranPelanggaranController,
                                    onChanged: vm.onPelanggaranChange,
                                    style: CustomFont.basicText,
                                    decoration: const InputDecoration(
                                      hintText: 'Pelanggaran',
                                      focusedBorder: InputBorder.none,
                                      border: InputBorder.none,
                                      hintStyle: CustomFont.noticeText,
                                      contentPadding: EdgeInsets.fromLTRB(
                                          20.0, 10.0, 20.0, 10.0),
                                    ),
                                  ),
                                ),
                                Divider(
                                  color:
                                      CustomColor.mutedButton.withOpacity(0.5),
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(left: 20.0),
                                      child: Text(
                                        'Tindak Lanjut',
                                        style: CustomFont.basicText,
                                      ),
                                    ),
                                    Container(
                                      child: TextFormField(
                                        keyboardType: TextInputType.text,
                                        maxLines: 1,
                                        maxLength: 30,
                                        autofocus: false,
                                        controller:
                                            pelanggaranTindakanController,
                                        onChanged: vm.onTindakanChange,
                                        style: CustomFont.basicText,
                                        decoration: const InputDecoration(
                                          hintText: 'Tindak Lanjut',
                                          focusedBorder: InputBorder.none,
                                          border: InputBorder.none,
                                          hintStyle: CustomFont.noticeText,
                                          contentPadding: EdgeInsets.fromLTRB(
                                              20.0, 10.0, 20.0, 10.0),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 40.0),
                                    ),
                                    Center(
                                      child: MaterialButton(
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(5.0)),
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 60.0),
                                            child: Text(
                                              'Simpan',
                                              style: CustomFont.signIn,
                                            ),
                                          ),
                                          padding: const EdgeInsets.only(
                                              top: 10.0, bottom: 10.0),
                                          color: CustomColor.themedarker,
                                          onPressed: () => vm.editPelanggaran(
                                              vm.selectedPelanggaran.id)),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
