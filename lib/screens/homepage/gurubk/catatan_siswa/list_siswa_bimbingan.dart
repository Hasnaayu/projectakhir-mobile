import 'dart:convert';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/models/kelas_model.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/kelas_vm.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/pelanggaran_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/siswa_model.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/siswa_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:flutter/scheduler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ListSiswaBimbingan extends StatefulWidget {
  @override
  _ListnamaSiswaBimbinganDikelastate createState() =>
      _ListnamaSiswaBimbinganDikelastate();
}

class _ListnamaSiswaBimbinganDikelastate extends State<ListSiswaBimbingan> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context.read<SiswaViewModel>().setNetworkService(
            context.read<NetworkService>(),
          ); //for fetchListSiswa()
      //   context.read<SiswaViewModel>().fetchDataNama();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Pilih Siswa',
          style: CustomFont.appBar,
        ),
        centerTitle: true,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: SingleChildScrollView(
        child: Consumer<SiswaViewModel>(
          builder: (_, SiswaViewModel vm, __) => Column(
            children: [
              Padding(
                padding: EdgeInsets.all(16),
                child: Card(
                  color: CustomColor.whitebg,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Center(
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
                          child: Image(
                            image: CustomImage.students,
                            width: 300,
                            height: 140,
                          ),
                        ),
                      ),
                      Center(
                        child: Container(
                          padding: const EdgeInsets.only(bottom: 25.0),
                          child: Text(
                            'Pilih Siswa',
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.w500),
                          ),
                        ),
                      ),
                      for (var i = 0;
                          i <
                              context
                                  .watch<KelasViewModel>()
                                  .namaSiswaDikelas
                                  .length;
                          i++)
                        Card(
                          child: MaterialButton(
                            onPressed: () =>
                                context.read<KelasViewModel>().onSiswaTappedBim(
                                      context
                                          .read<KelasViewModel>()
                                          .namaSiswaDikelas[i],
                                    ),
                            // vm.createPelanggaran(vm.namaSiswaDikelas[i].id),
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Container(
                                width: 480,
                                decoration: BoxDecoration(
                                  color: CustomColor.whitebg,
                                  border: Border.all(
                                      width: 2.0,
                                      color: CustomColor.themedarker),
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(16),
                                  ),
                                ),
                                child: Column(
                                  children: [
                                    Container(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                          context
                                              .read<KelasViewModel>()
                                              .namaSiswaDikelas[i]
                                              .name,
                                          style: TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.w700)),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
