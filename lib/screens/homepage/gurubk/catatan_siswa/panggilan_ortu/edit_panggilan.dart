import 'package:bk_mobile_app/auth/mixins/dialog_mixin.dart';
import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/panggilan_ortu_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/jadwal_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/will_pop_vm.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class EditPanggilanPage extends StatefulWidget {
  @override
  _EditPanggilanPageState createState() => _EditPanggilanPageState();
}

class _EditPanggilanPageState extends State<EditPanggilanPage>
    with DialogMixin {
  @override
  void initState() {
    super.initState();
    panggilanTopikController = TextEditingController();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      final PanggilanOrtuViewModel pg = context.read<PanggilanOrtuViewModel>();
      panggilanTopikController.text = pg.selectedPanggilan.topik ?? '';
      context
          .read<PanggilanOrtuViewModel>()
          .setNetworkViewModel(context.read<NetworkService>());
    });
  }

  @override
  void dispose() {
    super.dispose();
    panggilanTopikController.dispose();
  }

  TextEditingController panggilanTopikController;

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Consumer<PanggilanOrtuViewModel>(
      builder: (_, PanggilanOrtuViewModel _vm, __) => WillPopScope(
        onWillPop: () => context.read<WillPopViewModel>().onGoBack(),
        child: Scaffold(
          backgroundColor: CustomColor.graybg,
          appBar: AppBar(
            title: const Text(
              'Edit Panggilan Orang Tua',
              style: CustomFont.appBar,
            ),
            centerTitle: true,
            backgroundColor: CustomColor.whitebg,
            automaticallyImplyLeading: false,
          ),
          body: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Consumer<PanggilanOrtuViewModel>(
              builder: (_, PanggilanOrtuViewModel vm, __) => ConstrainedBox(
                constraints: BoxConstraints(maxHeight: size.height / 1.2),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    const Padding(
                      padding: EdgeInsets.only(top: 30.0),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            ListTile(
                              tileColor: CustomColor.whitebg,
                              leading: Icon(
                                Icons.calendar_today,
                                size: 35,
                              ),
                              onTap: () => vm.selectDate(context),
                              title: Row(
                                children: [
                                  if (vm.tanggal != null &&
                                      vm?.selectedPanggilan?.tanggal != null)
                                    Text(
                                      vm.toStringDue(),
                                      style: CustomFont.noticeText,
                                    )
                                  else if (vm?.selectedPanggilan
                                          ?.toStringDue() !=
                                      vm.toStringDue())
                                    Text(
                                      vm?.selectedPanggilan?.toStringDue(),
                                      style: CustomFont.noticeText,
                                    )
                                  else
                                    Text(
                                      vm?.selectedPanggilan?.toStringDue(),
                                      style: CustomFont.noticeText,
                                    ),
                                ],
                              ),
                            ),
                            Divider(
                              color: CustomColor.mutedButton.withOpacity(0.5),
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 20.0),
                                  child: Text(
                                    'Agenda',
                                    style: CustomFont.basicText,
                                  ),
                                ),
                                Container(
                                  child: TextFormField(
                                    keyboardType: TextInputType.text,
                                    maxLines: 1,
                                    maxLength: 30,
                                    autofocus: false,
                                    controller: panggilanTopikController,
                                    onChanged: vm.onTopikChange,
                                    style: CustomFont.basicText,
                                    decoration: const InputDecoration(
                                      hintText: 'Tentukan Agenda.',
                                      focusedBorder: InputBorder.none,
                                      border: InputBorder.none,
                                      hintStyle: CustomFont.noticeText,
                                      contentPadding: EdgeInsets.fromLTRB(
                                          20.0, 10.0, 20.0, 10.0),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Center(
                                    child: MaterialButton(
                                        minWidth: size.width / 2,
                                        elevation: 5.0,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0)),
                                        child: Text(
                                          'Kirim'.toUpperCase(),
                                          style: CustomFont.signIn,
                                        ),
                                        padding: EdgeInsets.only(
                                            top: 10.0, bottom: 10.0),
                                        color: CustomColor.themedarker,
                                        onPressed: () => vm.editPanggilan(
                                            vm.selectedPanggilan.id)),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
