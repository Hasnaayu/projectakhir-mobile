import 'dart:convert';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/bimbingan_vm.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/kelas_vm.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/panggilan_ortu_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/siswa_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/route_manager.dart';
import 'package:provider/provider.dart';
import 'package:flutter/scheduler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class CreatePanggilan extends StatefulWidget {
  @override
  _CreatePanggilanState createState() => _CreatePanggilanState();
}

class _CreatePanggilanState extends State<CreatePanggilan> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context.read<PanggilanOrtuViewModel>().setNetworkViewModel(
            context.read<NetworkService>(),
          ); //for fetchPelanggaran()
      int id = context.read<KelasViewModel>().selectedSiswaModel.id;
      context.read<PanggilanOrtuViewModel>().removeEarlierPanggilan(id);
    });
  }

  void _onRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    int id = context.read<KelasViewModel>().selectedSiswaModel.id;
    context.read<PanggilanOrtuViewModel>().removeEarlierPanggilan(id);
    _refreshController.refreshCompleted();
  }

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  void dispose() {
    super.dispose();
    _refreshController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Panggilan Orang Tua',
          style: CustomFont.appBar,
        ),
        centerTitle: true,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => context.read<SiswaViewModel>().onAddPanggilanChange(
              context.read<SiswaViewModel>().siswa,
            ),
      ),
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: false,
        controller: _refreshController,
        onRefresh: _onRefresh,
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Consumer<PanggilanOrtuViewModel>(
            builder: (_, PanggilanOrtuViewModel vm, __) => Column(
              children: [
                Padding(padding: EdgeInsets.only(top: 20.0)),
                Text(
                  'Muat ulang untuk menampilkan data terbaru.',
                  style: CustomFont.noticeText,
                ),
                if (vm.panggilans.isEmpty)
                  Center(
                    child: Container(
                      margin: EdgeInsets.only(top: 300.0),
                      child: Text(
                        'Panggilan orang tua kosong.',
                        style: CustomFont.noticeText,
                      ),
                    ),
                  )
                else if (vm.panggilans.isNotEmpty)
                  for (var i = 0; i < vm.panggilans.length; i++)
                    if (vm.panggilans[i].isDone == null)
                      Column(
                        children: [
                          if ((vm.panggilans[i].tanggal
                                          .difference(vm.today)
                                          .inDays ==
                                      0 &&
                                  vm.panggilans[i].tanggal
                                          .difference(vm.today)
                                          .inHours ==
                                      0) ||
                              (vm.panggilans[i].tanggal
                                          .difference(vm.today)
                                          .inDays ==
                                      0 &&
                                  vm.panggilans[i].tanggal
                                          .difference(vm.today)
                                          .inHours <
                                      0) ||
                              (vm.panggilans[i].tanggal
                                      .difference(vm.today)
                                      .inDays <
                                  0))
                            Stack(
                              children: [
                                Card(
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.lightBlueAccent
                                          .withOpacity(0.2),
                                    ),
                                    child: Slidable(
                                      actionPane: SlidableDrawerActionPane(),
                                      actionExtentRatio: 0.25,
                                      child: Column(
                                        children: [
                                          Container(
                                            width: size.width,
                                            padding: EdgeInsets.only(
                                                top: 5.0, bottom: 5.0),
                                            child: Text(
                                              'Akan berlangsung.',
                                              style: CustomFont.waiting,
                                            ),
                                          ),
                                          Container(
                                            width: size.width,
                                            padding: EdgeInsets.only(
                                                top: 5.0, bottom: 5.0),
                                            child: Text(
                                              vm?.panggilans[i]?.topik ?? '',
                                              style: CustomFont.basicTitle,
                                            ),
                                          ),
                                          Container(
                                            width: size.width,
                                            padding: EdgeInsets.only(
                                                top: 5.0, bottom: 5.0),
                                            child: Text(
                                              vm?.panggilans[i]
                                                      ?.toStringDue() ??
                                                  '',
                                              style: CustomFont.basicTitle,
                                            ),
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: [
                                              Icon(Icons.timer),
                                              Text(
                                                  '${vm.panggilans[i].tanggal.difference(vm.today).inDays}'),
                                            ],
                                          ),
                                          Container(
                                            width: size.width,
                                            alignment: Alignment.bottomRight,
                                            padding: EdgeInsets.only(
                                                top: 5.0, bottom: 5.0),
                                            child: Text(
                                              vm?.panggilans[i]
                                                      ?.toStringMade() ??
                                                  '',
                                              style: CustomFont.smallMuted,
                                            ),
                                          ),
                                        ],
                                      ),
                                      secondaryActions: <Widget>[
                                        IconSlideAction(
                                          color: CustomColor.theme,
                                          icon: Icons.check,
                                          foregroundColor: CustomColor.whitebg,
                                          onTap: () =>
                                              vm.addToDone(vm.panggilans[i].id),
                                        ),
                                        IconSlideAction(
                                          color: Colors.red,
                                          icon: Icons.cancel,
                                          foregroundColor: CustomColor.whitebg,
                                          onTap: () => vm
                                              .addToFails(vm.panggilans[i].id),
                                        ),
                                        IconSlideAction(
                                          color: CustomColor.theme,
                                          icon: Icons.edit,
                                          foregroundColor: CustomColor.whitebg,
                                          onTap: () => vm.onEditPanggilan(
                                              vm.panggilans[i]),
                                        ),
                                        IconSlideAction(
                                          color: Colors.red,
                                          icon: Icons.delete,
                                          foregroundColor: CustomColor.whitebg,
                                          onTap: () => vm.deletePanggilan(
                                              vm.panggilans[i].id),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                        ],
                      )
                    else if (vm.panggilans[i].isDone == 1)
                      Column(
                        children: [
                          Card(
                            child: Container(
                              decoration: BoxDecoration(
                                color:
                                    CustomColor.themelighter.withOpacity(0.2),
                              ),
                              child: Column(
                                children: [
                                  Container(
                                    width: size.width,
                                    padding:
                                        EdgeInsets.only(top: 5.0, bottom: 5.0),
                                    child: Text(
                                      'Selesai.',
                                      style: CustomFont.accept,
                                    ),
                                  ),
                                  Container(
                                    width: size.width,
                                    padding:
                                        EdgeInsets.only(top: 5.0, bottom: 5.0),
                                    child: Text(
                                      vm?.panggilans[i]?.topik ?? '',
                                      style: CustomFont.basicTitle,
                                    ),
                                  ),
                                  Align(
                                      alignment: Alignment.bottomRight,
                                      child: Icon(Icons.check)),
                                  Container(
                                    width: size.width,
                                    alignment: Alignment.bottomRight,
                                    padding:
                                        EdgeInsets.only(top: 5.0, bottom: 5.0),
                                    child: Text(
                                      vm?.panggilans[i]?.toStringDue() ?? '',
                                      style: CustomFont.smallMuted,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      )
                    else if (vm.panggilans[i].isDone == 0)
                      Column(
                        children: [
                          Card(
                            child: Container(
                              decoration: BoxDecoration(
                                color:
                                    CustomColor.orangeaccent.withOpacity(0.2),
                              ),
                              child: Slidable(
                                actionPane: SlidableDrawerActionPane(),
                                actionExtentRatio: 0.25,
                                child: Column(
                                  children: [
                                    Container(
                                      width: size.width,
                                      padding: EdgeInsets.only(
                                          top: 5.0, bottom: 5.0),
                                      child: Text(
                                        'Tidak terpenuhi.',
                                        style: CustomFont.reject,
                                      ),
                                    ),
                                    Container(
                                      width: size.width,
                                      padding: EdgeInsets.only(
                                          top: 5.0, bottom: 5.0),
                                      child: Text(
                                        vm?.panggilans[i]?.topik ?? '',
                                        style: CustomFont.basicTitle,
                                      ),
                                    ),
                                    Align(
                                        alignment: Alignment.bottomRight,
                                        child: Icon(Icons.cancel)),
                                    Container(
                                      width: size.width,
                                      alignment: Alignment.bottomRight,
                                      padding: EdgeInsets.only(
                                          top: 5.0, bottom: 5.0),
                                      child: Text(
                                        vm?.panggilans[i]?.toStringMade() ?? '',
                                        style: CustomFont.smallMuted,
                                      ),
                                    ),
                                  ],
                                ),
                                secondaryActions: <Widget>[
                                  IconSlideAction(
                                    color: CustomColor.theme,
                                    icon: Icons.edit,
                                    foregroundColor: CustomColor.whitebg,
                                    onTap: () =>
                                        vm.onEditPanggilan(vm.panggilans[i]),
                                  ),
                                  IconSlideAction(
                                    color: Colors.red,
                                    icon: Icons.delete,
                                    foregroundColor: CustomColor.whitebg,
                                    onTap: () =>
                                        vm.deletePanggilan(vm.panggilans[i].id),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
