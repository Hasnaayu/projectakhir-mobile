import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/kelas_vm.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/panggilan_ortu_vm.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/pelanggaran_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/siswa_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:get/route_manager.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:flutter/scheduler.dart';

class AddPanggilan extends StatefulWidget {
  @override
  _AddPanggilanState createState() => _AddPanggilanState();
}

class _AddPanggilanState extends State<AddPanggilan> {
  TextEditingController controllerAgenda = TextEditingController();

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context.read<PanggilanOrtuViewModel>().setNetworkViewModel(
            context.read<NetworkService>(),
          ); //for fetchPelanggaran()
      // context.read<PelanggaranViewModel>().fetchPelanggaran();
      // context.read<SiswaViewModel>().fetchNamaSiswa();
    });
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Form Panggilan Orang Tua',
          style: CustomFont.appBar,
        ),
        centerTitle: true,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Consumer<PanggilanOrtuViewModel>(
          builder: (_, PanggilanOrtuViewModel vm, __) => ConstrainedBox(
            constraints: BoxConstraints(maxHeight: size.height / 1.2),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                const Padding(
                  padding: EdgeInsets.only(top: 30.0),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        ListTile(
                          tileColor: CustomColor.whitebg,
                          leading: Icon(
                            Icons.calendar_today,
                            size: 35,
                          ),
                          onTap: () => vm.selectDate(context),
                          title: Row(
                            children: [
                              if (vm.tanggal == null &&
                                  vm?.selectedPanggilan?.tanggal == null)
                                Text(
                                  'Tentukan tanggal.',
                                  style: CustomFont.noticeText,
                                )
                              else if (vm.tanggal != null ||
                                  vm?.selectedPanggilan?.tanggal != null)
                                Text(
                                  vm.toStringDue(),
                                  style: CustomFont.noticeText,
                                ),
                            ],
                          ),
                        ),
                        Divider(
                          color: CustomColor.mutedButton.withOpacity(0.5),
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 20.0),
                              child: Text(
                                'Agenda',
                                style: CustomFont.basicText,
                              ),
                            ),
                            Container(
                              child: TextFormField(
                                keyboardType: TextInputType.text,
                                autofocus: false,
                                maxLines: 1,
                                controller: controllerAgenda,
                                onChanged: vm.onTopikChange,
                                decoration: const InputDecoration(
                                  hintText: 'Agenda',
                                  focusedBorder: InputBorder.none,
                                  border: InputBorder.none,
                                  hintStyle: CustomFont.noticeText,
                                  contentPadding: EdgeInsets.fromLTRB(
                                      20.0, 10.0, 20.0, 10.0),
                                ),
                              ),
                            ),
                            Divider(
                              color: CustomColor.mutedButton.withOpacity(0.5),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(40.0),
                            ),
                            Center(
                              child: MaterialButton(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5.0),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 60.0),
                                  child: Text(
                                    'Tambahkan Data',
                                    style: CustomFont.signIn,
                                  ),
                                ),
                                padding: const EdgeInsets.only(
                                    top: 10.0, bottom: 10.0),
                                color: CustomColor.themedarker,
                                onPressed: () => vm.addNewPanggilan(
                                  context
                                      .read<KelasViewModel>()
                                      .selectedSiswaModel
                                      .id,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
