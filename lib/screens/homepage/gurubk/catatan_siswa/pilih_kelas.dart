import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/list_siswa.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/pilih_siswa.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/aum_first_page.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/screens/splash/splash.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sticky_headers/sticky_headers.dart';

class PilihKelas extends StatefulWidget {
  @override
  _PilihKelasState createState() => _PilihKelasState();
}

class _PilihKelasState extends State<PilihKelas> {
  ScrollController _controller;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Catatan Siswa',
          style: CustomFont.appBar,
        ),
        centerTitle: true,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: false,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(
              context,
              MaterialPageRoute(builder: (context) => AumFirstPage()),
            );
          },
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        controller: _controller,
        children: ListTile.divideTiles(
          context: context,
          tiles: [
            Column(
              children: [
                Padding(
                  padding: EdgeInsets.all(16),
                  child: Card(
                    color: CustomColor.whitebg,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Center(
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
                            child: Image(
                              image: CustomImage.menu3,
                              width: 280,
                              height: 140,
                            ),
                          ),
                        ),
                        Center(
                          child: Container(
                            padding: const EdgeInsets.only(bottom: 25.0),
                            child: Text(
                              'Pilih Kelas Siswa',
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.w500),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => PilihSiswa()),
                    );
                  },
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(18.0, 8.0, 18.0, 8.0),
                    child: Container(
                      padding: EdgeInsets.all(10.0),
                      decoration: BoxDecoration(
                        color: CustomColor.lightgrey,
                        border: Border.all(
                            width: 3.0, color: CustomColor.themedarker),
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: Center(
                        child: Text(
                          'X Kendaraan Ringan 1',
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(18.0, 8.0, 18.0, 8.0),
                  child: Container(
                    padding: EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                      color: CustomColor.lightgrey,
                      border: Border.all(
                          width: 3.0, color: CustomColor.themedarker),
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: Center(
                      child: Text(
                        'X Kendaraan Ringan 2',
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w400),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(18.0, 8.0, 18.0, 8.0),
                  child: Container(
                    padding: EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                      color: CustomColor.lightgrey,
                      border: Border.all(
                          width: 3.0, color: CustomColor.themedarker),
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: Center(
                      child: Text(
                        'XI Multimedia 1',
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w400),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(18.0, 8.0, 18.0, 8.0),
                  child: Container(
                    padding: EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                      color: CustomColor.lightgrey,
                      border: Border.all(
                          width: 3.0, color: CustomColor.themedarker),
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: Center(
                      child: Text(
                        'XI Multimedia 2',
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w400),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(18.0, 8.0, 18.0, 8.0),
                  child: Container(
                    padding: EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                      color: CustomColor.lightgrey,
                      border: Border.all(
                          width: 3.0, color: CustomColor.themedarker),
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: Center(
                      child: Text(
                        'XI Teknik Permesinan 1',
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w400),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(18.0, 8.0, 18.0, 8.0),
                  child: Container(
                    padding: EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                      color: CustomColor.lightgrey,
                      border: Border.all(
                          width: 3.0, color: CustomColor.themedarker),
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: Center(
                      child: Text(
                        'XI Teknik Permesinan 2',
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w400),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(18.0, 8.0, 18.0, 8.0),
                  child: Container(
                    padding: EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                      color: CustomColor.lightgrey,
                      border: Border.all(
                          width: 3.0, color: CustomColor.themedarker),
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: Center(
                      child: Text(
                        'XI Teknik Permesinan 3',
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w400),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 30),
                ),
              ],
            )
          ],
        ).toList(),
      ),
    );
  }
}
