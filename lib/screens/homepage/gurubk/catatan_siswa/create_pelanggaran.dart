import 'dart:convert';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/add_pelanggaran.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/catatan_siswa_intro.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/kelas_vm.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/pelanggaran_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/siswa_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/route_manager.dart';
import 'package:provider/provider.dart';
import 'package:flutter/scheduler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class CreatePelanggaran extends StatefulWidget {
  @override
  _CreatePelanggaranState createState() => _CreatePelanggaranState();
}

class _CreatePelanggaranState extends State<CreatePelanggaran> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context.read<PelanggaranViewModel>().setNetworkService(
            context.read<NetworkService>(),
          ); //for fetchPelanggaran()
      int id = context.read<KelasViewModel>().selectedSiswaModel.id;
      context.read<PelanggaranViewModel>().fetchPelanggaran(id);
    });
  }

  void _onRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    int id = context.read<KelasViewModel>().selectedSiswaModel.id;
    context.read<PelanggaranViewModel>().removeEarlierData(id);
    _refreshController.refreshCompleted();
  }

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  void dispose() {
    super.dispose();
    _refreshController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Catatan Pelanggaran',
          style: CustomFont.appBar,
        ),
        centerTitle: true,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => context.read<SiswaViewModel>().onAddPelanggaranChange(
              context.read<SiswaViewModel>().siswa,
            ),
      ),
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: false,
        controller: _refreshController,
        onRefresh: _onRefresh,
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Consumer<PelanggaranViewModel>(
            builder: (_, PelanggaranViewModel vm, __) => Column(
              children: [
                Padding(padding: EdgeInsets.only(top: 20.0)),
                Center(
                  child: Text(
                    'Muat ulang untuk menampilkan data terbaru',
                    style: CustomFont.noticeText,
                  ),
                ),
                for (var i = 0; i < vm.pelanggarans.length; i++)
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        width: 480,
                        decoration: BoxDecoration(
                          color: CustomColor.whitebg,
                          border: Border.all(
                              width: 2.0, color: CustomColor.themedarker),
                          borderRadius: BorderRadius.all(
                            Radius.circular(16),
                          ),
                        ),
                        child: Column(
                          children: [
                            Container(
                              padding:
                                  const EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
                              child: ListTile(
                                title: Text('Tanggal Pelanggaran'),
                                subtitle: Text(
                                    '${vm.pelanggarans[i].tanggal_pelanggaran}'),
                              ),
                            ),
                            Container(
                              padding:
                                  const EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
                              child: ListTile(
                                title: Text('Pelanggaran'),
                                subtitle:
                                    Text('${vm.pelanggarans[i].pelanggaran}'),
                              ),
                            ),
                            Container(
                              padding:
                                  const EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
                              child: ListTile(
                                title: Text('Tindak Lanjut'),
                                subtitle:
                                    Text('${vm.pelanggarans[i].tindak_lanjut}'),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.all(2.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  RaisedButton(
                                    child: Text('Edit'),
                                    color: CustomColor.themelighter,
                                    onPressed: () => vm.onEditPelanggaranChange(
                                        vm.pelanggarans[i]),
                                  ),
                                  RaisedButton(
                                    child: Text('Delete'),
                                    color: Colors.redAccent[700],
                                    onPressed: () => vm.deletePelanggaran(
                                        vm.pelanggarans[i].id),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
