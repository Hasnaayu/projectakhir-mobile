import 'dart:convert';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/bimbingan_vm.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/kelas_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/siswa_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/route_manager.dart';
import 'package:provider/provider.dart';
import 'package:flutter/scheduler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class CreateBimbingan extends StatefulWidget {
  @override
  _CreateBimbinganState createState() => _CreateBimbinganState();
}

class _CreateBimbinganState extends State<CreateBimbingan> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context.read<BimbinganViewModel>().setNetworkService(
            context.read<NetworkService>(),
          ); //for fetchPelanggaran()
      int id = context.read<KelasViewModel>().selectedSiswaModel.id;
      context.read<BimbinganViewModel>().fetchBimbingan(id);
    });
  }

  void _onRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    int id = context.read<KelasViewModel>().selectedSiswaModel.id;
    context.read<BimbinganViewModel>().removeEarlierData(id);
    _refreshController.refreshCompleted();
  }

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  void dispose() {
    super.dispose();
    _refreshController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Catatan Bimbingan',
          style: CustomFont.appBar,
        ),
        centerTitle: true,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => context.read<SiswaViewModel>().onAddBimbinganChange(
              context.read<SiswaViewModel>().siswa,
            ),
      ),
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: false,
        controller: _refreshController,
        onRefresh: _onRefresh,
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Consumer<BimbinganViewModel>(
            builder: (_, BimbinganViewModel vm, __) => Column(
              children: [
                Padding(padding: EdgeInsets.only(top: 20.0)),
                Center(
                  child: Text(
                    'Muat ulang untuk menampilkan data terbaru',
                    style: CustomFont.noticeText,
                  ),
                ),
                for (var i = 0; i < vm.bimbingans.length; i++)
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        width: 480,
                        decoration: BoxDecoration(
                          color: CustomColor.whitebg,
                          border: Border.all(
                              width: 2.0, color: CustomColor.themedarker),
                          borderRadius: BorderRadius.all(
                            Radius.circular(16),
                          ),
                        ),
                        child: Column(
                          children: [
                            Container(
                              padding:
                                  const EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
                              child: ListTile(
                                title: Text('Tanggal Bimbingan'),
                                subtitle: Text(
                                    '${vm.bimbingans[i].tanggal_bimbingan}'),
                              ),
                            ),
                            Container(
                              padding:
                                  const EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
                              child: ListTile(
                                title: Text('Permasalahan'),
                                subtitle:
                                    Text('${vm.bimbingans[i].permasalahan}'),
                              ),
                            ),
                            Container(
                              padding:
                                  const EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
                              child: ListTile(
                                title: Text('Solusi'),
                                subtitle: Text('${vm.bimbingans[i].solusi}'),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.all(10.0),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  RaisedButton(
                                    child: Text('Edit'),
                                    color: CustomColor.themelighter,
                                    onPressed: () => vm.onEditBimbinganChange(
                                        vm.bimbingans[i]),
                                  ),
                                  RaisedButton(
                                    child: Text('Delete'),
                                    color: Colors.redAccent[700],
                                    onPressed: () =>
                                        vm.deleteBimbingan(vm.bimbingans[i].id),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
