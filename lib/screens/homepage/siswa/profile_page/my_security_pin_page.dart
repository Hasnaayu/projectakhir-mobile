import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/auth/view_models/user_security_pin_view_model.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/will_pop_vm.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:lite_rolling_switch/lite_rolling_switch.dart';
import 'package:provider/provider.dart';

class MySecurityPinPage extends StatefulWidget {
  @override
  _MySecurityPinPageState createState() => _MySecurityPinPageState();
}

class _MySecurityPinPageState extends State<MySecurityPinPage> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context
          .read<UserSecurityPinViewModel>()
          .setNetworkService(context.read<NetworkService>());
      context.read<UserSecurityPinViewModel>().removeEarlierData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<WillPopViewModel>(
      builder: (_, WillPopViewModel _vm, __) => WillPopScope(
        onWillPop: () => _vm.onGoBack(),
        child: Scaffold(
            backgroundColor: CustomColor.graybg,
            appBar: AppBar(
              backgroundColor: CustomColor.whitebg,
              title: Text(
                'PIN Keamanan',
                style: CustomFont.appBar,
              ),
              automaticallyImplyLeading: false,
            ),
            body: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Consumer<UserSecurityPinViewModel>(
                  builder: (_, UserSecurityPinViewModel vm, __) => Column(
                    children: [
                      if (vm.userpins.isNotEmpty)
                        for (var i = 0; i < vm.userpins.length; i++)
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Aplikasi Keamanan Ganda',
                                style: CustomFont.basicTitle,
                              ),
                              LiteRollingSwitch(
                                  value: vm?.userpins[i]?.isOn ?? false,
                                  iconOff: null,
                                  iconOn: null,
                                  colorOn: CustomColor.theme,
                                  colorOff: CustomColor.mutedButton,
                                  textSize: 16.0,
                                  onChanged: (bool value) {
                                    vm.onChangeIsOff(
                                        context, value, vm.userpins[i].id);
                                  })
                            ],
                          )
                      else if (vm.userpins.isEmpty)
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Aplikasi Keamanan Ganda',
                              style: CustomFont.basicTitle,
                            ),
                            LiteRollingSwitch(
                                iconOff: null,
                                iconOn: null,
                                colorOn: CustomColor.theme,
                                colorOff: CustomColor.mutedButton,
                                textSize: 16.0,
                                onChanged: (bool value) {
                                  vm.onChangeIsOn(context, value);
                                })
                          ],
                        ),
                      Padding(padding: EdgeInsets.all(20.0)),
                      Text(
                        'Aplikasi Keamanan Ganda memungkinkan Anda memasukkan pin setiap kali Anda membuka aplikasi BiKos.',
                        style: CustomFont.smallerMuted,
                      )
                    ],
                  ),
                ),
              ),
            )),
      ),
    );
  }
}
