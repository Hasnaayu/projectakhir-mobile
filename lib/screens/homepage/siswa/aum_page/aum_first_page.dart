import 'dart:convert';

import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/aum_second_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:bk_mobile_app/screens/welcome/login.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sticky_headers/sticky_headers.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/main_page/main_page.dart';
import 'package:bk_mobile_app/screens/homepage/home_page.dart';

import 'isi/screens/splash/splash.dart';

class AumFirstPage extends StatefulWidget {
  @override
  _AumFirstPageState createState() => _AumFirstPageState();
}

class _AumFirstPageState extends State<AumFirstPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Alat Ungkap Masalah',
          style: CustomFont.appBar,
        ),
        centerTitle: true,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: false,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(
              context,
              MaterialPageRoute(builder: (context) => MainPage()),
            );
          },
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(40.0, 0, 20.0, 0),
              child: Image(
                image: CustomImage.menu1,
                width: 360.0,
                height: 360.0,
              ),
            ),
          ),
          Container(
            width: 600,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(26.0, 0, 26.0, 50.0),
              child: Text(
                'Alat Ungkap Masalah (AUM) adalah sebuah instrumen standar yang digunakan dalam rangka memahami dan memperkirakan masalah-masalah yang dihadapi konseli. Alat Ungkap Masalah ini didesain untuk mengungkap 10 bidang masalah yang mungkin dihadapi konseli.',
                textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16),
              ),
            ),
          ),
          Center(
            child: MaterialButton(
              minWidth: 120.0,
              height: 44.0,
              elevation: 10,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => SplashScreen()),
                );
              },
              color: CustomColor.themedarker,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: Text(
                'Mulai'.toUpperCase(),
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
