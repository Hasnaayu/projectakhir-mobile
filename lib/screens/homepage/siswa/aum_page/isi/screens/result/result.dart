import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/aum_model.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/screens/bidang/jdk.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/screens/filter/filter.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/screens/form_aum/form_aum.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/anm_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/dpi_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/edk_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/hmm_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/hso_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/jdk_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/kdp_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/khk_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/pdp_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/wsg_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/aum_vm.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:get/route_manager.dart';

class ResultScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Alat Ungkap Masalah',
          style: CustomFont.appBar,
        ),
        centerTitle: true,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: false,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.all(16),
                child: Text(
                    'Perhatikan dan baca kembali jawaban yang telah Anda pilih, kemudian pilih masalah-masalah yang menurut Anda dirasakan paling mengganggu.',
                    style: CustomFont.basicText2),
              ),
              Consumer<AumViewModel>(
                builder: (_, AumViewModel vm, __) => Column(
                  children: [
                    for (AumModel jdk in vm.resultF)
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          TextButton(
                            onPressed: () => vm.addToResultF(jdk),
                            child: Container(
                              padding: EdgeInsets.all(8),
                              width: 464,
                              decoration: BoxDecoration(
                                color: CustomColor.whitebg,
                                border: Border.all(
                                    width: 2.0, color: CustomColor.themedarker),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(16),
                                ),
                              ),
                              child: CheckboxListTile(
                                //contentPadding: EdgeInsets.all(10),
                                title: Text('${jdk.title}'),
                                value: jdk.selected,
                                onChanged: (bool newVal) {
                                  vm.switchSelectedtStatusF(jdk, newVal);
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    Column(
                      children: [
                        for (AumModel dpi in vm.resultB)
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              TextButton(
                                onPressed: () => vm.addToResultB(dpi),
                                child: Container(
                                  padding: EdgeInsets.all(8),
                                  width: 464,
                                  decoration: BoxDecoration(
                                    color: CustomColor.whitebg,
                                    border: Border.all(
                                        width: 2.0,
                                        color: CustomColor.themedarker),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(16),
                                    ),
                                  ),
                                  child: CheckboxListTile(
                                    //contentPadding: EdgeInsets.all(10),
                                    title: Text('${dpi.title}'),
                                    value: dpi.selected,
                                    onChanged: (bool newVal) {
                                      vm.switchSelectedtStatusB(dpi, newVal);
                                    },
                                  ),
                                ),
                              ),
                            ],
                          ),
                        Column(
                          children: [
                            for (AumModel hso in vm.resultE)
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  TextButton(
                                    onPressed: () => vm.addToResultE(hso),
                                    child: Container(
                                      padding: EdgeInsets.all(8),
                                      width: 464,
                                      decoration: BoxDecoration(
                                        color: CustomColor.whitebg,
                                        border: Border.all(
                                            width: 2.0,
                                            color: CustomColor.themedarker),
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(16),
                                        ),
                                      ),
                                      child: CheckboxListTile(
                                        //contentPadding: EdgeInsets.all(10),
                                        title: Text('${hso.title}'),
                                        value: hso.selected,
                                        onChanged: (bool newVal) {
                                          vm.switchSelectedtStatusE(
                                              hso, newVal);
                                        },
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            Column(
                              children: [
                                for (AumModel edk in vm.resultC)
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      TextButton(
                                        onPressed: () => vm.addToResultC(edk),
                                        child: Container(
                                          padding: EdgeInsets.all(8),
                                          width: 464,
                                          decoration: BoxDecoration(
                                            color: CustomColor.whitebg,
                                            border: Border.all(
                                                width: 2.0,
                                                color: CustomColor.themedarker),
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(16),
                                            ),
                                          ),
                                          child: CheckboxListTile(
                                            //contentPadding: EdgeInsets.all(10),
                                            title: Text('${edk.title}'),
                                            value: edk.selected,
                                            onChanged: (bool newVal) {
                                              vm.switchSelectedtStatusC(
                                                  edk, newVal);
                                            },
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                Column(
                                  children: [
                                    for (AumModel kdp in vm.resultG)
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          TextButton(
                                            onPressed: () =>
                                                vm.addToResultG(kdp),
                                            child: Container(
                                              padding: EdgeInsets.all(8),
                                              width: 464,
                                              decoration: BoxDecoration(
                                                color: CustomColor.whitebg,
                                                border: Border.all(
                                                    width: 2.0,
                                                    color: CustomColor
                                                        .themedarker),
                                                borderRadius: BorderRadius.all(
                                                  Radius.circular(16),
                                                ),
                                              ),
                                              child: CheckboxListTile(
                                                //contentPadding: EdgeInsets.all(10),
                                                title: Text('${kdp.title}'),
                                                value: kdp.selected,
                                                onChanged: (bool newVal) {
                                                  vm.switchSelectedtStatusG(
                                                      kdp, newVal);
                                                },
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    Column(
                                      children: [
                                        for (AumModel pdp in vm.resultI)
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              TextButton(
                                                onPressed: () =>
                                                    vm.addToResultI(pdp),
                                                child: Container(
                                                  padding: EdgeInsets.all(8),
                                                  width: 464,
                                                  decoration: BoxDecoration(
                                                    color: CustomColor.whitebg,
                                                    border: Border.all(
                                                        width: 2.0,
                                                        color: CustomColor
                                                            .themedarker),
                                                    borderRadius:
                                                        BorderRadius.all(
                                                      Radius.circular(16),
                                                    ),
                                                  ),
                                                  child: CheckboxListTile(
                                                    //contentPadding: EdgeInsets.all(10),
                                                    title: Text('${pdp.title}'),
                                                    value: pdp.selected,
                                                    onChanged: (bool newVal) {
                                                      vm.switchSelectedtStatusI(
                                                          pdp, newVal);
                                                    },
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        Column(
                                          children: [
                                            for (AumModel anm in vm.resultA)
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  TextButton(
                                                    onPressed: () =>
                                                        vm.addToResultA(anm),
                                                    child: Container(
                                                      padding:
                                                          EdgeInsets.all(8),
                                                      width: 464,
                                                      decoration: BoxDecoration(
                                                        color:
                                                            CustomColor.whitebg,
                                                        border: Border.all(
                                                            width: 2.0,
                                                            color: CustomColor
                                                                .themedarker),
                                                        borderRadius:
                                                            BorderRadius.all(
                                                          Radius.circular(16),
                                                        ),
                                                      ),
                                                      child: CheckboxListTile(
                                                        //contentPadding: EdgeInsets.all(10),
                                                        title: Text(
                                                            '${anm.title}'),
                                                        value: anm.selected,
                                                        onChanged:
                                                            (bool newVal) {
                                                          vm.switchSelectedtStatusA(
                                                              anm, newVal);
                                                        },
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            Column(
                                              children: [
                                                for (AumModel hmm in vm.resultD)
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      TextButton(
                                                        onPressed: () => vm
                                                            .addToResultD(hmm),
                                                        child: Container(
                                                          padding:
                                                              EdgeInsets.all(8),
                                                          width: 464,
                                                          decoration:
                                                              BoxDecoration(
                                                            color: CustomColor
                                                                .whitebg,
                                                            border: Border.all(
                                                                width: 2.0,
                                                                color: CustomColor
                                                                    .themedarker),
                                                            borderRadius:
                                                                BorderRadius
                                                                    .all(
                                                              Radius.circular(
                                                                  16),
                                                            ),
                                                          ),
                                                          child:
                                                              CheckboxListTile(
                                                            //contentPadding: EdgeInsets.all(10),
                                                            title: Text(
                                                                '${hmm.title}'),
                                                            value: hmm.selected,
                                                            onChanged:
                                                                (bool newVal) {
                                                              vm.switchSelectedtStatusD(
                                                                  hmm, newVal);
                                                            },
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                Column(
                                                  children: [
                                                    for (AumModel khk
                                                        in vm.resultH)
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          TextButton(
                                                            onPressed: () =>
                                                                vm.addToResultH(
                                                                    khk),
                                                            child: Container(
                                                              padding:
                                                                  EdgeInsets
                                                                      .all(8),
                                                              width: 464,
                                                              decoration:
                                                                  BoxDecoration(
                                                                color:
                                                                    CustomColor
                                                                        .whitebg,
                                                                border: Border.all(
                                                                    width: 2.0,
                                                                    color: CustomColor
                                                                        .themedarker),
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .all(
                                                                  Radius
                                                                      .circular(
                                                                          16),
                                                                ),
                                                              ),
                                                              child:
                                                                  CheckboxListTile(
                                                                //contentPadding: EdgeInsets.all(10),
                                                                title: Text(
                                                                    '${khk.title}'),
                                                                value: khk
                                                                    .selected,
                                                                onChanged: (bool
                                                                    newVal) {
                                                                  vm.switchSelectedtStatusH(
                                                                      khk,
                                                                      newVal);
                                                                },
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    Column(
                                                      children: [
                                                        for (AumModel wsg
                                                            in vm.resultJ)
                                                          Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: [
                                                              TextButton(
                                                                onPressed: () =>
                                                                    vm.addToResultJ(
                                                                        wsg),
                                                                child:
                                                                    Container(
                                                                  padding:
                                                                      EdgeInsets
                                                                          .all(
                                                                              8),
                                                                  width: 464,
                                                                  decoration:
                                                                      BoxDecoration(
                                                                    color: CustomColor
                                                                        .whitebg,
                                                                    border: Border.all(
                                                                        width:
                                                                            2.0,
                                                                        color: CustomColor
                                                                            .themedarker),
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .all(
                                                                      Radius.circular(
                                                                          16),
                                                                    ),
                                                                  ),
                                                                  child:
                                                                      CheckboxListTile(
                                                                    //contentPadding: EdgeInsets.all(10),
                                                                    title: Text(
                                                                        '${wsg.title}'),
                                                                    value: wsg
                                                                        .selected,
                                                                    onChanged: (bool
                                                                        newVal) {
                                                                      vm.switchSelectedtStatusJ(
                                                                          wsg,
                                                                          newVal);
                                                                    },
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  top: 20),
                                                        ),
                                                        Center(
                                                          child: MaterialButton(
                                                            minWidth: 100.0,
                                                            height: 40.0,
                                                            elevation: 10,
                                                            onPressed: () =>
                                                                vm.addNewAumJ(),
                                                            color: CustomColor
                                                                .themedarker,
                                                            shape: RoundedRectangleBorder(
                                                                borderRadius: BorderRadius
                                                                    .all(Radius
                                                                        .circular(
                                                                            10))),
                                                            child: Text(
                                                              'Next',
                                                              style: TextStyle(
                                                                  fontSize: 18,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w600),
                                                            ),
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  bottom: 20),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
