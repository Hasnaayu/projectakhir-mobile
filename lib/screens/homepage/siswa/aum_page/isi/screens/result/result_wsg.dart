import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/aum_model.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/screens/bidang/jdk.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/screens/filter/filter.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/screens/form_aum/form_aum.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/anm_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/dpi_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/edk_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/hmm_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/hso_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/jdk_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/kdp_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/khk_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/pdp_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/wsg_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/aum_vm.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:get/route_manager.dart';

import 'package:flutter/scheduler.dart';

class ResultWsgScreen extends StatefulWidget {
  @override
  _ResultWsgScreenState createState() => _ResultWsgScreenState();
}

class _ResultWsgScreenState extends State<ResultWsgScreen> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context
          .read<AumViewModel>()
          .setNetworkService(context.read<NetworkService>());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'WSG',
          style: CustomFont.appBar,
        ),
        centerTitle: true,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: false,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.all(16),
                child: Text(
                    'Perhatikan dan baca kembali jawaban yang telah Anda pilih, kemudian pilih masalah-masalah yang menurut Anda dirasakan paling mengganggu.',
                    style: CustomFont.basicText2),
              ),
              Consumer<AumViewModel>(
                builder: (_, AumViewModel vm, __) => Column(
                  children: [
                    for (AumModel wsg in vm.resultJ)
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          TextButton(
                            onPressed: () => vm.addToResultJ(wsg),
                            child: Container(
                              padding: EdgeInsets.all(8),
                              width: 464,
                              decoration: BoxDecoration(
                                color: CustomColor.whitebg,
                                border: Border.all(
                                    width: 2.0, color: CustomColor.themedarker),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(16),
                                ),
                              ),
                              child: CheckboxListTile(
                                //contentPadding: EdgeInsets.all(10),
                                title: Text('${wsg.title}'),
                                value: wsg?.selected ?? false,
                                onChanged: (bool newVal) {
                                  vm.switchSelectedtStatusJ(wsg, newVal);
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                    ),
                    Center(
                      child: MaterialButton(
                        minWidth: 100.0,
                        height: 40.0,
                        elevation: 10,
                        onPressed: () => vm.addNewAumJ(),
                        color: CustomColor.themedarker,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        child: Text(
                          'Next',
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 20),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
