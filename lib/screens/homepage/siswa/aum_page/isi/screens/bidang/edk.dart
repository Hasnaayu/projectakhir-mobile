import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/aum_model.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/screens/bidang/kdp.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/screens/result/result.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/edk_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/aum_vm.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter/scheduler.dart';

class EdkScreen extends StatefulWidget {
  @override
  _EdkScreenState createState() => _EdkScreenState();
}

class _EdkScreenState extends State<EdkScreen> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context
          .read<EdkService>()
          .setNetworkService(context.read<NetworkService>()); //for fetchAum()
      //context.read<EdkService>().fetchAum();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Alat Ungkap Masalah',
          style: CustomFont.appBar,
        ),
        centerTitle: true,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: false,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Card(
                  color: CustomColor.whitebg,
                  child: Center(
                    child: Padding(
                      padding: EdgeInsets.all(16),
                      child: Text(
                        'Ekonomi dan Keuangan (EDK)',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 26,
                            color: Colors.black),
                      ),
                    ),
                  )),
              Consumer<AumViewModel>(
                builder: (_, AumViewModel vm, __) => Column(
                  children: [
                    for (AumModel edk in vm.edks)
                      TextButton(
                        onPressed: () => vm.addToResultC(edk),
                        child: Container(
                          width: 480,
                          decoration: BoxDecoration(
                            color: CustomColor.whitebg,
                            border: Border.all(
                                width: 2.0, color: CustomColor.themedarker),
                            borderRadius: BorderRadius.all(
                              Radius.circular(16),
                            ),
                          ),
                          child: Padding(
                            padding: EdgeInsets.all(16),
                            child: Text(
                              '${edk.title}',
                              style: TextStyle(
                                  fontSize: 18,
                                  fontFamily: 'Roboto',
                                  color: Colors.black),
                            ),
                          ),
                        ),
                      ),
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                    ),
                    Center(
                      child: MaterialButton(
                        minWidth: 100.0,
                        height: 40.0,
                        elevation: 10,
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => KdpScreen(),
                            ),
                          );
                        },
                        color: CustomColor.themedarker,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        child: Text(
                          'Next',
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 20),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
