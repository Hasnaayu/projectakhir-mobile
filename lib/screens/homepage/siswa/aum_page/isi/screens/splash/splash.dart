import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/aum_first_page.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/screens/bidang/jdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/route_manager.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((Duration _) {
      onFinishLoading();
    });
  }

  Future<void> onFinishLoading() async {
    await Future<void>.delayed(const Duration(milliseconds: 1000));
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => JdkScreen()));
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const SizedBox(
              height: 48,
              width: 48,
              child: CircularProgressIndicator(
                backgroundColor: Colors.transparent,
                valueColor: AlwaysStoppedAnimation<Color>(Color(0xFFFEDCBA)),
              ),
            ),
            const SizedBox(height: 16),
            Text(
              'Loading . . .',
              style: TextStyle(fontSize: size.width * 0.04),
            ),
          ],
        ),
      ),
    );
  }
}
