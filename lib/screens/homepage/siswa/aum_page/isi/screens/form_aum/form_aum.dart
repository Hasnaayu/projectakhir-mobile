import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/aum_form_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/will_pop_vm.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class FormPage extends StatefulWidget {
  @override
  _FormPageState createState() => _FormPageState();
}

class _FormPageState extends State<FormPage> {
  TextEditingController controllerProblem = TextEditingController();
  TextEditingController controllerDesc = TextEditingController();

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context
          .read<AumFormViewModel>()
          .setNetworkService(context.read<NetworkService>());
    });
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Alat Ungkap Masalah',
          style: CustomFont.appBar,
        ),
        centerTitle: true,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: false,
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Consumer<AumFormViewModel>(
          builder: (_, AumFormViewModel vm, __) => ConstrainedBox(
            constraints: BoxConstraints(maxHeight: size.height / 1.2),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                const Padding(
                  padding: EdgeInsets.only(top: 30.0),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 20.0),
                              child: Text(
                                "Apakah sudah menggambarkan seluruh masalah Anda? Sertakan alasan Anda!",
                                style: CustomFont.basicText,
                              ),
                            ),
                            Container(
                              child: TextFormField(
                                keyboardType: TextInputType.text,
                                autofocus: false,
                                maxLines: 1,
                                controller: controllerProblem,
                                onChanged: vm.onProblem,
                                decoration: const InputDecoration(
                                  hintText: "Jelaskan dan sertakan alasan.",
                                  focusedBorder: InputBorder.none,
                                  border: InputBorder.none,
                                  hintStyle: CustomFont.noticeText,
                                  contentPadding: EdgeInsets.fromLTRB(
                                      20.0, 10.0, 20.0, 10.0),
                                ),
                              ),
                            ),
                            Divider(
                              color: CustomColor.mutedButton.withOpacity(0.5),
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 20.0),
                                  child: Text(
                                    "Masalah lain yang Anda hadapi?",
                                    style: CustomFont.basicText,
                                  ),
                                ),
                                Container(
                                  child: TextFormField(
                                    keyboardType: TextInputType.text,
                                    autofocus: false,
                                    maxLines: 1,
                                    controller: controllerDesc,
                                    onChanged: vm.onDesc,
                                    decoration: const InputDecoration(
                                      hintText: "Sebutkan.",
                                      focusedBorder: InputBorder.none,
                                      border: InputBorder.none,
                                      hintStyle: CustomFont.noticeText,
                                      contentPadding: EdgeInsets.fromLTRB(
                                          20.0, 10.0, 20.0, 10.0),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(40.0),
                                ),
                                Center(
                                  child: MaterialButton(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5.0),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 60.0),
                                      child: Text(
                                        'Simpan',
                                        style: CustomFont.signIn,
                                      ),
                                    ),
                                    padding: const EdgeInsets.only(
                                        top: 10.0, bottom: 10.0),
                                    color: CustomColor.themedarker,
                                    onPressed: () => vm.addNewForm(),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
