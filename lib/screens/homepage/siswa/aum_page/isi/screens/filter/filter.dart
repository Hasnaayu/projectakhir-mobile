import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/aum_model.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/jdk_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FilterScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Consumer<JdkService>(
            builder: (BuildContext context, JdkService value, Widget child) =>
                Column(
              children: [
                for (final AumModel jdk in value.result)
                  if (jdk.selected) Text(jdk.title),
                TextButton(
                  onPressed: () {
                    //
                  },
                  child: const Text('ok'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
