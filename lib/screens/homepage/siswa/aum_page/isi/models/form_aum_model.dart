import 'package:intl/intl.dart';

class FormAumModel {
  FormAumModel({
    this.desc,
    this.problem,
    this.createdAt,
  });

  FormAumModel.fromJson(Map<String, dynamic> json) {
    desc = json['desc'] as String;
    problem = json['problem'] as String;
    createdAt = DateTime.parse(json["created_at"] as String);
  }

  String desc;
  String problem;
  DateTime createdAt;

  String createdAtToString() {
    return DateFormat('EEEEEE, d - M - y').format(createdAt);
  }
}
