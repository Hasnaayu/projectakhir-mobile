import 'package:intl/intl.dart';

class JawabBotModel {
  JawabBotModel({
    this.id,
    this.jawaban,
  });

  JawabBotModel.fromJson(Map<String, dynamic> json) {
    id = json['id_jawaban'] as int;
    jawaban = json['jawaban'] as String;
  }

  int id;
  String jawaban;

  @override
  String toString() => 'id: $id, jawaban: $jawaban';
}
