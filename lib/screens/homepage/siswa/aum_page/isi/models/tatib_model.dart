class TatibModel {
  TatibModel({
    this.isi,
  });

  TatibModel.fromJson(Map<String, dynamic> json) {
    isi = json['isi_tatib'] as String;
  }

  String isi;
}
