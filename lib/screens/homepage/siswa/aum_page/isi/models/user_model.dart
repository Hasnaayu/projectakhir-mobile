class UserModel {
  UserModel({
    this.id,
    this.name,
    this.email,
    this.password,
    this.roleId,
    this.createdAt,
    this.updatedAt,
  });

  UserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'] as int;
    name = json['name'] as String;
    email = json['email'] as String;
    password = json['password'] as String;
    roleId = json['id_role'] as int;
    createdAt = json["created_at"] as String;
    updatedAt = json["updated_at"] as String;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    return data;
  }

  int id;
  String name;
  String email;
  String password;// password???????
  int roleId;
  String createdAt;
  String updatedAt;

  @override
  String toString() => 'id: $id name: $name';
}
