import 'package:intl/intl.dart';

class AumModel {
  AumModel({
    this.id,
    this.siswaId,
    this.title,
    this.selected,
    this.createdAt,
    this.updatedAt,
  });

  // final String title;
  // bool selected;

  AumModel.fromJson(Map<String, dynamic> json) {
    id = json['id_aum'] as int;
    siswaId = json['id_siswa'] as int;
    title = json['title'] as String;
    selected = json['selected'] as bool;
    createdAt = DateTime.parse(json["created_at"] as String);
    updatedAt = DateTime.parse(json["updated_at"] as String);
  }

  int id;
  int siswaId;
  String title;
  bool selected;
  DateTime createdAt;
  DateTime updatedAt;

  String createdAtToString() {
    return DateFormat('EEEEEE, d - M - y').format(createdAt);
  }
}
