import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/response_model.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/topik_model.dart';

class ChatbotModel {
  ChatbotModel(
      {this.message,
      this.isBot = false,
      this.topic,
      this.response,
      this.timeStamp,
      this.topicString,
      this.pertanyaanString});

  String message;
  List<ResponseModel> response;
  List<TopicModel> topic;
  List<String> topicString;
  List<String> pertanyaanString;
  DateTime timeStamp;
  bool isBot;
}
