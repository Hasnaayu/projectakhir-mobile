import 'package:intl/intl.dart';

class UserSecurityPinModel {
  UserSecurityPinModel({
    this.id,
    this.userId,
    this.pin,
    this.isOn,
  });

  UserSecurityPinModel.fromJson(Map<String, dynamic> json) {
    id = json['id'] as int;
    userId = json['user_id'] as int;
    pin = json['pin'].toString();
    isOn = (json['is_on'] ?? false) as bool;
    createdAt = DateTime.tryParse(json['created_at'] as String);
    updatedAt = DateTime.tryParse(json['updated_at'] as String);
  }

  int id;
  int userId;
  String pin;
  bool isOn;
  DateTime createdAt;
  DateTime updatedAt;
}
