class ResponseModel {
  ResponseModel({this.pertanyaan, this.jawaban});

  ResponseModel.fromJson(Map<String, dynamic> json) {
    pertanyaan = json['pertanyaan'] as String;
    jawaban = json['jawaban'] as String;
  }

  String pertanyaan;
  String jawaban;
}
