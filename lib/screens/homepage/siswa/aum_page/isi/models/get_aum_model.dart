import 'package:intl/intl.dart';

class GetAumModel {
  GetAumModel({
    this.title,
    this.createdAt,
  });

  GetAumModel.fromJson(Map<String, dynamic> json) {
    title = json['title'] as String;
    createdAt = DateTime.parse(json["created_at"] as String);
  }

  String title;
  DateTime createdAt;

  String createdAtToString() {
    return DateFormat('EEEEEE, d - M - y').format(createdAt);
  }
}
