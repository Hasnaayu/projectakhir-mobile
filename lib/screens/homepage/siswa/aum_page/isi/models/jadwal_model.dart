import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class JadwalModel {
  JadwalModel({
    this.id,
    this.tanggal,
    this.topik,
    this.id_siswa,
    this.id_gurubk,
    this.nama_siswa,
    this.isConfirmed,
    this.createdAt,
    this.updatedAt,
  });

  JadwalModel.fromJson(Map<String, dynamic> json) {
    id = json['id_jadwal'] as int;
    tanggal = DateTime.parse(json['tanggal'] as String);
    topik = json['topik'] as String;
    id_siswa = json['id_siswa'] as int;
    id_gurubk = json['id_gurubk'] as int;
    nama_siswa = json['nama_siswa'] as String;
    isConfirmed = json['isConfirmed'] as int;
    createdAt = DateTime.parse(json['created_at'] as String);
    updatedAt = DateTime.parse(json['updated_at'] as String);
  }

  int id;
  DateTime tanggal;
  String topik;
  String nama_siswa;
  int id_siswa;
  int id_gurubk;
  int isConfirmed;
  DateTime createdAt;
  DateTime updatedAt;

  @override
  String toStringDue() {
    return DateFormat('EEEEEE, d - M - y').format(tanggal);
  }

  @override
  String toStringMade() {
    return DateFormat('EEEEEE, d - M - y').format(updatedAt);
  }
}
