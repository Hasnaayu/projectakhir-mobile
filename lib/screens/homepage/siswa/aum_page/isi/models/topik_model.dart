class TopicModel {
  TopicModel({
    this.id,
    this.topic,
  });

  TopicModel.fromJson(Map<String, dynamic> json) {
    id = json['id_topik'] as int;
    topic = json['topik'] as String;
  }

  int id;
  String topic;
}
