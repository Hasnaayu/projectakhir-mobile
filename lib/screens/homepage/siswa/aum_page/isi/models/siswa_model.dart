class SiswaModel {
  SiswaModel({
    this.id,
    this.name,
    this.nisn,
    this.gender,
    this.placebirth,
    this.datebirth,
    this.nik,
    this.agama,
    this.address,
    this.telp,
    this.tinggal,
    this.school,
    this.anakke,
    this.saudara,
    this.status,
    this.classId,
    this.ortuId,
    this.accId,
    this.foto,
    this.createdAt,
    this.updatedAt,
  });

  SiswaModel.fromJson(Map<String, dynamic> json) {
    id = json['id_siswa'] as int;
    name = json['nama_siswa'] as String;
    nisn = json['nisn'] as String;
    gender = json['jenis_kelamin'] as String;
    placebirth = json['tempat_lahir'] as String;
    datebirth = json['tanggal_lahir'] as String;
    nik = json['nik'] as String;
    agama = json['agama'] as String;
    address = json['alamat'] as String;
    telp = json['telp'] as String;
    tinggal = json['jenis_tinggal'] as String;
    school = json['asal_sekolah'] as String;
    anakke = json['anak_keberapa'] as int;
    saudara = json['jumlah_saudara_kandung'] as int;
    status = json['status_siswa'] as String;
    classId = json['id_kelas'] as int;
    ortuId = json['id_ortu'] as int;
    accId = json['id_account'] as int;
    foto = json['foto'] as String;
    createdAt = json["created_at"] as String;
    updatedAt = json["updated_at"] as String;
  }

  int id;
  String name;
  String nisn;
  String gender;
  String placebirth;
  String datebirth;
  String nik;
  String agama;
  String address;
  String telp;
  String tinggal;
  String school;
  int anakke;
  int saudara;
  String status;
  int classId;
  int ortuId;
  int accId;
  String foto;
  String createdAt;
  String updatedAt;

  @override
  String toString() {
    // TODO: implement toString
    return '$foto';
  }
}
