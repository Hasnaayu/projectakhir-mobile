import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/aum_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:dio/dio.dart';

class DpiService extends ChangeNotifier {
  DpiService() {
    _dpis = <AumModel>[
      AumModel(title: '076. Sering mimpi buruk'),
      AumModel(
          title: '077. Cemas atau khawatir tentang sesuatu yang belum pasti'),
      AumModel(title: '078. Mudah lupa'),
      AumModel(title: '079. Sering melamun atau berkhayal'),
      AumModel(title: '080. Ceroboh atau kurang berhati-hati'),
      AumModel(title: '091. Sering murung dan/atau mudah patah semangat'),
      AumModel(
          title:
              '092. Mengalami kerugian atau kesulitan karena terlampau hati-hati'),
      AumModel(title: '093. Kurang serius menghadapi sesuatu yang penting'),
      AumModel(title: '094. Merasa hidup ini kurang berarti'),
      AumModel(title: '095. Sering gagal dan/atau mudah patah semangat '),
      AumModel(
          title:
              '106. Mudah gentar atau khawatir dalam menghadapi dan/atau mengemukakan sesuatu'),
      AumModel(title: '107. Penakut, pemalu, dan/atau mudah menjadi bingung'),
      AumModel(
          title:
              '108. Keras kepala atau sukar mengubah pendapat sendiri meskipun kata orang lain pendapat itu salah'),
      AumModel(title: '109. Takut mencoba sesuatu yang baru'),
      AumModel(title: '110. Mudah marah atau tidak mampu mengendalikan diri'),
      AumModel(title: '121. Merasa kesepian dan/atau takut ditinggal sendiri'),
      AumModel(
          title:
              '122. Sering bertingkah laku, bertindak, atau bersikap kekanak-kanakan'),
      AumModel(title: '123. Rendah diri atau kurang percaya diri'),
      AumModel(title: '124. Kurang terbuka terhadap orang lain'),
      AumModel(
          title:
              '125. Sering membesar-besarkan sesuatu yang sebenarnya tidak perlu'),
    ];
    _result = <AumModel>[];
  }

  NetworkService _net;

  List<AumModel> _dpis;
  List<AumModel> get dpis => _dpis;

  List<AumModel> _result;
  List<AumModel> get result => _result;

  AumModel am;

  String title;
  bool selected;

  void setNetworkService(NetworkService net) {
    _net = net;
  }

  void onTitleChange(String val) {
    title = val;
  }

  void onSeledtedChange(bool val) {
    selected = val;
  }

  void addToResult(AumModel dpi) {
    _result.add(dpi);
    _dpis.remove(dpi);
    notifyListeners();
  }

  void removeFromResult(AumModel dpi) {
    _dpis.add(dpi);
    _result.remove(dpi);
    notifyListeners();
  }

  void switchSelectedtStatus(AumModel dpi, bool newVal) {
    final int dpiIndex = _result.indexOf(dpi);
    _result[dpiIndex].selected = newVal;
    notifyListeners();
  }

  Future<void> addNewAum() async {
    final Response<dynamic> resp = await _net.request(
      '/aum/store',
      requestMethod: 'post',
      data: <String, dynamic>{
        'title': title,
        'selected': selected,
      },
    );
    final Map<String, dynamic> respData = resp.data as Map<String, dynamic>;

    if (respData != null) {
      Get.back<void>();
      print(respData);
    }
    notifyListeners();
  }

  Future<void> fetchAum() async {
    final Response<dynamic> resp = await _net.request('/aum/indexaum');

    final List<dynamic> respData = resp.data['data'] as List<dynamic>;

    for (final dynamic respData in respData) {
      _dpis.add(AumModel.fromJson(respData as Map<String, dynamic>));
    }
    print(respData);
    notifyListeners();
  }
}
