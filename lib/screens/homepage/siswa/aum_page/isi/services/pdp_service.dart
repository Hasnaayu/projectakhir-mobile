import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/aum_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:get/route_manager.dart';

class PdpService extends ChangeNotifier {
  PdpService() {
    _pdps = <AumModel>[
      AumModel(title: '011. Terpaksa atau ragu-ragu memasuki sekolah ini'),
      AumModel(title: '012. Meragukan kemanfaatan memasuki sekolah ini'),
      AumModel(title: '013. Sukar menyesuaikan diri dengan keadaan sekolah'),
      AumModel(
          title:
              '014. Kurang meminati pelajaran atau jurusan atau program yang diikuti'),
      AumModel(
          title:
              '015. Khawatir tidak dapat menamatkan sekolah pada waktu yang direncanakan'),
      AumModel(title: '026. Sering tidak masuk sekolah'),
      AumModel(title: '027. Tugas-tugas pelajaran tidak selesai pada waktunya'),
      AumModel(
          title:
              '028. Sukar memahami penjelasan guru sewaktu pelajaran berlangsung'),
      AumModel(
          title: '029. Mengalami kesulitan dalam membuat catatan pelajaran'),
      AumModel(
          title: '030. Terpaksa mengikuti mata pelajaran yang tidak disukai'),
      AumModel(
          title:
              '041. Gelisah dan/atau melakukan kegiatan tidak menentu sewaktu pelajaran berlangsung, misalnya membuat coret-coretan dalam buku, cenderung mengganggu teman'),
      AumModel(title: '042. Sering malas belajar'),
      AumModel(title: '043. Kurang konsentrasi dalam mengikuti pelajaran'),
      AumModel(
          title:
              '044. Khawatir tugas-tugas pelajaran hasilnya kurang memuaskan atau rendah'),
      AumModel(
          title:
              '045. Mengalami masalah karena kemajuan atau hasil belajar hanya diberitahukan pada akhir catur wulan'),
      AumModel(title: '056. Mengalami masalah dalam menjawab pertanyaan ujian'),
      AumModel(
          title:
              '057. Tidak mengetahui dan/atau tidak mampu menerapkan cara-cara belajar yang baik'),
      AumModel(title: '058. Kekurangan waktu belajar'),
      AumModel(
          title:
              '059. Mengalami masalah dalam menyusun makalah, laporan, atau karya tulis lainnya'),
      AumModel(title: '060. Sukar mendapatkan buku pelajaran yang diperlukan'),
      AumModel(title: '071. Seringkali tidak siap menghadapi ujian'),
      AumModel(title: '072. Sarana belajar di sekolah kurang memadai'),
      AumModel(
          title:
              '073. Orang tua kurang peduli dan/atau kurang membantu kegiatan belajar di sekolah dan/atau di rumah'),
      AumModel(
          title:
              '074. Anggota keluarga kurang peduli dan/atau kurang membantu kegiatan belajar di sekolah dan/atau di rumah'),
      AumModel(title: '075. Sarana belajar di rumah kurang memadai'),
      AumModel(title: '086. Guru kurang adil atau pilih kasih'),
      AumModel(title: '087. Ingin dekat dengan guru'),
      AumModel(
          title:
              '088. Guru kurang memperhatikan kebutuhan dan/atau keadaan siswa'),
      AumModel(title: '089. Mendapatkan perhatian khusus dari guru tertentu'),
      AumModel(
          title:
              '090. Dalam memberikan pelajaran dan/atau berhubungan dengan siswa, sikap dan/atau tindakan guru sering berubah-ubah, sehingga membingungkan siswa'),
      AumModel(
          title:
              '101. Khawatir tidak tersedia biaya untuk melanjutkan pekerjaan setamat sekolah ini'),
      AumModel(
          title:
              '102. Tidak dapat mengambil keputusan tentang apakah akan mencari pekerjaan atau melanjutkan pelajaran setamat sekolah ini'),
      AumModel(
          title:
              '103. Khawatir tuntutan dan proses pendidikan lanjutansetamat sekolah ini sangat berat'),
      AumModel(
          title:
              '104. Terdapat pertentangan pendapat dengan orang tua dan/atau anggota keluarga lain tentang rencana melanjutkan pelajaran setamat sekolah ini'),
      AumModel(
          title:
              '105. Khawatir tidak mampu bersaing dalam upaya memasuki pendidikan lanjutan setamat sekolah ini'),
    ];
    _result = <AumModel>[];
  }

  NetworkService _net;

  List<AumModel> _pdps;
  List<AumModel> get pdps => _pdps;

  List<AumModel> _result;
  List<AumModel> get result => _result;

  AumModel am;

  String title;
  bool selected;

  void setNetworkService(NetworkService net) {
    _net = net;
  }

  void onTitleChange(String val) {
    title = val;
  }

  void onSeledtedChange(bool val) {
    selected = val;
  }

  void addToResult(AumModel pdp) {
    _result.add(pdp);
    _pdps.remove(pdp);
    notifyListeners();
  }

  void removeFromResult(AumModel pdp) {
    _pdps.add(pdp);
    _result.remove(pdp);
    notifyListeners();
  }

  void switchSelectedtStatus(AumModel pdp, bool newVal) {
    final int pdpIndex = _result.indexOf(pdp);
    _result[pdpIndex].selected = newVal;
    notifyListeners();
  }

  Future<void> addNewAum() async {
    final Response<dynamic> resp = await _net.request(
      '/aum/store',
      requestMethod: 'post',
      data: <String, dynamic>{
        'title': title,
        'selected': selected,
      },
    );
    final Map<String, dynamic> respData = resp.data as Map<String, dynamic>;

    if (respData != null) {
      Get.back<void>();
      print(respData);
    }
    notifyListeners();
  }

  Future<void> fetchAum() async {
    final Response<dynamic> resp = await _net.request('/aum/indexaum');

    final List<dynamic> respData = resp.data['data'] as List<dynamic>;

    for (final dynamic respData in respData) {
      _pdps.add(AumModel.fromJson(respData as Map<String, dynamic>));
    }
    print(respData);
    notifyListeners();
  }
}
