import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/aum_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:dio/dio.dart';

class EdkService extends ChangeNotifier {
  EdkService() {
    _edks = <AumModel>[
      AumModel(
          title:
              '181. Mengalami masalah karena kurang mampu berhemat atau kemampuan keuangan sangat tidak mencukupi, baik untuk keperluan sehari-hari maupun keperluan pekerjaan'),
      AumModel(
          title:
              '182. Khawatir tidak mampu menamatkan sekolah ini atau putus sekolah dan harus segera bekerja'),
      AumModel(
          title:
              '183. Mengalami masalah karena terlalu berhemat dan/atau ingin menabung'),
      AumModel(
          title:
              '184. Kekurangan dalam keuangan menyebabkan dalam pengembangan diri terhambat'),
      AumModel(
          title:
              '185. Untuk memenuhi keuangan terpaksa sekolah sambil bekerja'),
      AumModel(
          title: '196. Mengalami masalah karena ingin berpenghasilan sendiri'),
      AumModel(title: '197. Berhutang yang cukup memberatkan'),
      AumModel(
          title:
              '198. Besarnya uang yang diperoleh dan sumber-sumbernya tidak menentu'),
      AumModel(
          title:
              '199. Khawatir akan kondisi keuangan orang tua atau orang yang menjadi sumber keuangan; jangan-jangan harus menjual atau menggadaikan harta keluarga'),
      AumModel(
          title:
              '200. Mengalami masalah karena keuangan dikendalikan oleh orang lain'),
      AumModel(
          title:
              '211. Mengalami masalah karena membanding-bandingkan kondisi keuangan sendiri dengan kondisi keuangan orang lain'),
      AumModel(
          title:
              '212. Kesulitan dalam mendapatkan penghasilan sendiri sambil sekolah'),
      AumModel(
          title:
              '213. Mempertanyakan kemungkinan memperoleh beasiswa atau dana bantuan belajar lainnya'),
      AumModel(
          title:
              '214. Orang lain menganggap pelit dan/atau tidak mau membantu kawan yang sedang mengalami kesulitan keuangan'),
      AumModel(
          title:
              '215. Terpaksa berbagi pengeluaran keuangan dengan kakak atau adik atau anggota keluarga lain yang sama-sama membutuhkan biaya'),
    ];
    _result = <AumModel>[];
  }

  NetworkService _net;

  List<AumModel> _edks;
  List<AumModel> get edks => _edks;

  List<AumModel> _result;
  List<AumModel> get result => _result;

  AumModel am;

  String title;
  bool selected;

  void setNetworkService(NetworkService net) {
    _net = net;
  }

  void onTitleChange(String val) {
    title = val;
  }

  void onSeledtedChange(bool val) {
    selected = val;
  }

  void addToResult(AumModel edk) {
    _result.add(edk);
    _edks.remove(edk);
    notifyListeners();
  }

  void removeFromResult(AumModel edk) {
    _edks.add(edk);
    _result.remove(edk);
    notifyListeners();
  }

  void switchSelectedtStatus(AumModel edk, bool newVal) {
    final int edkIndex = _result.indexOf(edk);
    _result[edkIndex].selected = newVal;
    notifyListeners();
  }

  Future<void> addNewAum() async {
    final Response<dynamic> resp = await _net.request(
      '/aum/store',
      requestMethod: 'post',
      data: <String, dynamic>{
        'title': title,
        'selected': selected,
      },
    );
    final Map<String, dynamic> respData = resp.data as Map<String, dynamic>;

    if (respData != null) {
      Get.back<void>();
      print(respData);
    }
    notifyListeners();
  }

  Future<void> fetchAum() async {
    final Response<dynamic> resp = await _net.request('/aum/indexaum');

    final List<dynamic> respData = resp.data['data'] as List<dynamic>;

    for (final dynamic respData in respData) {
      _edks.add(AumModel.fromJson(respData as Map<String, dynamic>));
    }
    print(respData);
    notifyListeners();
  }
}
