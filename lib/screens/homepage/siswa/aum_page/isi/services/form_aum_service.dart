import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/form_aum_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class FormAumService extends ChangeNotifier {
  ArticleViewModel() {
    _forms = <FormAumModel>[];
    fetchForms();
  }

  List<FormAumModel> _forms;
  List<FormAumModel> get forms => _forms;

  void addDesc(String desc, String problem) {
    _forms.add(FormAumModel(desc: desc));
    notifyListeners();
  }

  Future<void> fetchForms() async {
    _forms.add(FormAumModel(desc: 'okayokay', problem: 'hahahahah'));
  }
}
