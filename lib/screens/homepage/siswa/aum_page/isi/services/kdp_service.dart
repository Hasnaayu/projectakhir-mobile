import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/aum_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:get/route_manager.dart';

class KdpService extends ChangeNotifier {
  KdpService() {
    _kdps = <AumModel>[
      AumModel(
          title:
              '006. Belum mampu memikirkan dan memilih pekerjaan yang akan dijabat nantinya'),
      AumModel(
          title:
              '007. Belum mengetahui bakat diri sendiri untuk jabatan/pekerjaan apa'),
      AumModel(
          title:
              '008. Kurang memiliki pengetahuan yang luas tentang lapangan pekerjaan dan seluk beluk jenis-jenis pekerjaan'),
      AumModel(
          title:
              '009. Ingin memperoleh bantuan dalam mendapatkan pekerjaan sambilan untuk melatih diri bekerja sambil sekolah'),
      AumModel(
          title:
              '010. Khawatir akan pekerjaan yang dijabat nanti; jangan-jangan memberikan penghasilan yang tidak mencukupi'),
      AumModel(
          title: '021. Ragu akan kemampuan saya untuk sukses dalam bekerja'),
      AumModel(title: '022. Belum mampu merencanakan masa depan'),
      AumModel(title: '023. Takut akan bayangan masa depan'),
      AumModel(
          title:
              '024. Mengalami masalah karena membanding-bandingkan pekerjaan yang layak atau tidak layak untuk dijabat'),
      AumModel(
          title:
              '025. Khawatir diperlakukan secara tidak wajar atau tidak adil dalam mencari dan/atau melamar pekerjaan'),
      AumModel(
          title:
              '036. Kurang yakin terhadap kemampuan pendidikan sekarang ini dalam menyiapkan jebatan tertentu nantinya'),
      AumModel(
          title:
              '037. Ragu tentang kesempatan memperoleh pekerjaan sesuai dengan pendidikan yang diikuti sekarang ini'),
      AumModel(
          title:
              '038. Ingin mengikuti kegiatan pelajaran dan/atau latihan khusus tertentu yang benar-benar menunjang proses mencari dan melamar pekerjaan setamat pendidikan ini'),
      AumModel(
          title: '039. Cemas kalau menjadi penganggur setamat pendidikan ini'),
      AumModel(
          title:
              '040. Ragu apakah setamat pendidikan ini dapat bekerja secara mandiri'),
      AumModel(title: '051. Hasil belajar atau nilai-nilai kurang memuaskan'),
      AumModel(title: '052. Mengalami masalah dalam belajar kelompok'),
      AumModel(
          title:
              '053. Kurang berminat dan/atau kurang mampu mempelajari buku pelajaran'),
      AumModel(
          title:
              '054. Takut dan/atau kurang mampu berbicara di dalam kelas dan/atau di luar kelas'),
      AumModel(
          title:
              '055. Mengalami kesulitan dalam ejaan, tata bahasa dan/atau perbendaharaan katadalam Bahasa Indonesia'),
      AumModel(
          title:
              '066. Mengalami kesulitan dalam pemahaman dan penggunaan istilah dan/atau Bahasa Inggris dan/atau bahasa asing lainnya'),
      AumModel(
          title:
              '067. Kesulitan dalam membaca cepat dan/atau memahami isi buku pelajaran'),
      AumModel(title: '068. Takut menghadapi ulangan/ujian'),
      AumModel(
          title:
              '069. Khawatir memperoleh nilai rendah dalam ulangan/ujian ataupun tugas-tugas'),
      AumModel(title: '070. Kesulitan dalam mengingat materi pelajaran'),
      AumModel(
          title:
              '081. Cara guru menyajikan pelajaran terlalu kaku dan/atau membosankan'),
      AumModel(title: '082. Guru kurang bersahabat dan/atau membimbing siswa'),
      AumModel(
          title:
              '083. Mengalami masalah karena disiplin yang ditetapkan oleh guru'),
      AumModel(
          title:
              '084. Dirugikan karena dalam menilai kemajuan atau keberhasilan siswa guru kurang objektif'),
      AumModel(
          title: '085. Guru kurang memberikan tanggung jawab kepada siswa'),
      AumModel(
          title:
              '096. Khawatir akan dipaksa melanjutkan pelajaran setamat sekolah ini'),
      AumModel(
          title:
              '097. Kekurangan informasi tentang pendidikan lanjutan yang dapat dimasuki setamat sekolah ini'),
      AumModel(
          title:
              '098. Ragu tentang kemanfaatan pendidikan lanjutan setamat sekolah ini'),
      AumModel(
          title:
              '099. Khawatir tidak mampu melanjutkan pelajaran setamat dari sekolah ini dan/atau terlalu memikirkan pendidikan lanjutan setamat sekolah ini'),
      AumModel(
          title:
              '100. Ragu apakah sekolah sekarang ini mampu memberikan modal yang kuat bagi para siswanya untuk menempuh pendidikan yang lebih lanjut'),
    ];
    _result = <AumModel>[];
  }

  NetworkService _net;

  List<AumModel> _kdps;
  List<AumModel> get kdps => _kdps;

  List<AumModel> _result;
  List<AumModel> get result => _result;

  AumModel am;

  String title;
  bool selected;

  void setNetworkService(NetworkService net) {
    _net = net;
  }

  void onTitleChange(String val) {
    title = val;
  }

  void onSeledtedChange(bool val) {
    selected = val;
  }

  void addToResult(AumModel kdp) {
    _result.add(kdp);
    _kdps.remove(kdp);
    notifyListeners();
  }

  void removeFromResult(AumModel kdp) {
    _kdps.add(kdp);
    _result.remove(kdp);
    notifyListeners();
  }

  void switchSelectedtStatus(AumModel kdp, bool newVal) {
    final int kdpIndex = _result.indexOf(kdp);
    _result[kdpIndex].selected = newVal;
    notifyListeners();
  }

  Future<void> addNewAum() async {
    final Response<dynamic> resp = await _net.request(
      '/aum/store',
      requestMethod: 'post',
      data: <String, dynamic>{
        'title': title,
        'selected': selected,
      },
    );
    final Map<String, dynamic> respData = resp.data as Map<String, dynamic>;

    if (respData != null) {
      Get.back<void>();
      print(respData);
    }
    notifyListeners();
  }

  Future<void> fetchAum() async {
    final Response<dynamic> resp = await _net.request('/aum/indexaum');

    final List<dynamic> respData = resp.data['data'] as List<dynamic>;

    for (final dynamic respData in respData) {
      _kdps.add(AumModel.fromJson(respData as Map<String, dynamic>));
    }
    print(respData);
    notifyListeners();
  }
}
