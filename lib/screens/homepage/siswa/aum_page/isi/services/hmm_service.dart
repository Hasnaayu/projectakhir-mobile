import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/aum_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:dio/dio.dart';

class HmmService extends ChangeNotifier {
  HmmService() {
    _hmms = <AumModel>[
      AumModel(
          title:
              '156. Membutuhkan keterangan tentang persoalan seks, pacaran, dan/atau perkawinan'),
      AumModel(
          title:
              '157. Mengalami masalah karena malu dan kurang terbuka dalam membicarakan soal seks, pacar, dan/atau jodoh'),
      AumModel(
          title:
              '158. Khawatir tidak mendapatkan pacar atau jodoh yang baik/cocok'),
      AumModel(
          title:
              '159. Terlalu memikirkan tentang seks, percintaan, pacaran, atau perkawinan'),
      AumModel(
          title:
              '160. Mengalami masalah karena dilarang atau merasa tidak patut berpacaran'),
      AumModel(
          title:
              '171. Kurang mendapat perhatian dari jenis kelamin lain atau pacar'),
      AumModel(title: '172. Mengalami masalah karena ingin mempunyai pacar'),
      AumModel(
          title:
              '173. Canggung dalam menghadapi jenis kelamin lain atau pacar'),
      AumModel(title: '174. Sukar mengendalikan dorongan seksual'),
      AumModel(
          title:
              '175. Mengalami masalah dalam memilih teman akrab dari jenis kelamin lain atau pacar'),
      AumModel(
          title:
              '186. Mengalami masalah karena takut atau sudah terlalu jauh berhubungan dengan jenis kelamin lain atau pacar'),
      AumModel(
          title: '187. Bertepuk sebelah tangan dengan kawan akrab atau pacar'),
      AumModel(
          title:
              '188. Takut ditinggalkan pacar atau patah hati, cemburu atau cinta segitiga'),
      AumModel(title: '189. Khawatir akan dipaksa kawin'),
      AumModel(
          title:
              '190. Mengalami masalah karena sering dan mudah jatuh cinta dan/atau rindu kepada pacar'),
    ];
    _result = <AumModel>[];
  }

  NetworkService _net;

  List<AumModel> _hmms;
  List<AumModel> get hmms => _hmms;

  List<AumModel> _result;
  List<AumModel> get result => _result;

  AumModel am;

  String title;
  bool selected;

  void setNetworkService(NetworkService net) {
    _net = net;
  }

  void onTitleChange(String val) {
    title = val;
  }

  void onSeledtedChange(bool val) {
    selected = val;
  }

  void addToResult(AumModel hmm) {
    _result.add(hmm);
    _hmms.remove(hmm);
    notifyListeners();
  }

  void removeFromResult(AumModel hmm) {
    _hmms.add(hmm);
    _result.remove(hmm);
    notifyListeners();
  }

  void switchSelectedtStatus(AumModel hmm, bool newVal) {
    final int hmmIndex = _result.indexOf(hmm);
    _result[hmmIndex].selected = newVal;
    notifyListeners();
  }

  Future<void> addNewAum() async {
    final Response<dynamic> resp = await _net.request(
      '/aum/store',
      requestMethod: 'post',
      data: <String, dynamic>{
        'title': title,
        'selected': selected,
      },
    );
    final Map<String, dynamic> respData = resp.data as Map<String, dynamic>;

    if (respData != null) {
      Get.back<void>();
      print(respData);
    }
    notifyListeners();
  }

  Future<void> fetchAum() async {
    final Response<dynamic> resp = await _net.request('/aum/indexaum');

    final List<dynamic> respData = resp.data['data'] as List<dynamic>;

    for (final dynamic respData in respData) {
      _hmms.add(AumModel.fromJson(respData as Map<String, dynamic>));
    }
    print(respData);
    notifyListeners();
  }
}
