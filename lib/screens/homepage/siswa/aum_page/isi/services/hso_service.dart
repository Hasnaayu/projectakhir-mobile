import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/aum_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:dio/dio.dart';

class HsoService extends ChangeNotifier {
  HsoService() {
    _hsos = <AumModel>[
      AumModel(title: '136. Tidak menyukai atau tidak disukai seseorang'),
      AumModel(
          title:
              '137. Merasa diperhatikan, dibicarakan atau diperolokkan orang lain'),
      AumModel(
          title:
              '138. Mengalami masalah karena ingin lebih terkenal atau lebih menarik atau lebih menyenangkan bagi orang lain'),
      AumModel(title: '139. Mempunyai kawan yang kurang disukai orang lain'),
      AumModel(
          title:
              '140. Tidak menyukai kawan akrab, hubungan sosial terbatas atau terisolir'),
      AumModel(title: '151. Kurang peduli terhadap orang lain'),
      AumModel(title: '152. Rapuh dalam berteman'),
      AumModel(
          title:
              '153. Merasa tidak dianggap penting, diremehkan atau dikecam oleh orang lain'),
      AumModel(
          title:
              '154. Mengalami masalah dengan orang lain karena kurang peduli terhadap diri sendiri'),
      AumModel(
          title:
              '155. Canggung dan/atau tidak lancar berkomunikasi dengan orang lain'),
      AumModel(
          title:
              '166. Tidak lincah dan kurang mengetahui tentang tata krama pergaulan'),
      AumModel(
          title:
              '167. Kurang pandai memimpin dan/atau mudah dipengaruhi orang lain'),
      AumModel(
          title:
              '168. Sering membantah atau tidak menyukai sesuatu yang dikatakan/dirasakan orang lain atau dikatakan sombong'),
      AumModel(
          title:
              '169. Mudah tersinggung atau sakit hati dalam berhubungan dengan orang lain'),
      AumModel(title: '170. Lambat menjalin persahabatan'),
    ];
    _result = <AumModel>[];
  }

  NetworkService _net;

  List<AumModel> _hsos;
  List<AumModel> get hsos => _hsos;

  List<AumModel> _result;
  List<AumModel> get result => _result;

  AumModel am;

  String title;
  bool selected;

  void setNetworkService(NetworkService net) {
    _net = net;
  }

  void onTitleChange(String val) {
    title = val;
  }

  void onSeledtedChange(bool val) {
    selected = val;
  }

  void addToResult(AumModel hso) {
    _result.add(hso);
    _hsos.remove(hso);
    notifyListeners();
  }

  void removeFromResult(AumModel hso) {
    _hsos.add(hso);
    _result.remove(hso);
    notifyListeners();
  }

  void switchSelectedtStatus(AumModel hso, bool newVal) {
    final int hsoIndex = _result.indexOf(hso);
    _result[hsoIndex].selected = newVal;
    notifyListeners();
  }

  Future<void> addNewAum() async {
    final Response<dynamic> resp = await _net.request(
      '/aum/store',
      requestMethod: 'post',
      data: <String, dynamic>{
        'title': title,
        'selected': selected,
      },
    );
    final Map<String, dynamic> respData = resp.data as Map<String, dynamic>;

    if (respData != null) {
      Get.back<void>();
      print(respData);
    }
    notifyListeners();
  }

  Future<void> fetchAum() async {
    final Response<dynamic> resp = await _net.request('/aum/indexaum');

    final List<dynamic> respData = resp.data['data'] as List<dynamic>;

    for (final dynamic respData in respData) {
      _hsos.add(AumModel.fromJson(respData as Map<String, dynamic>));
    }
    print(respData);
    notifyListeners();
  }
}
