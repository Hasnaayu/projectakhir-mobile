import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/aum_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:get/route_manager.dart';

class WsgService extends ChangeNotifier {
  WsgService() {
    _wsgs = <AumModel>[
      AumModel(
          title:
              '201. Kekurangan waktu senggang, seperti waktu istirahat, waktu luang di sekolah ataupun di rumah, waktu libur untuk bersikap santai dan/atau melakukan kegiatan yang menyenangkan atau rekreasi'),
      AumModel(
          title:
              '202. Tidak diperkenankan atau kurang bebas dalam menggunakan waktu senggang yang tersedia untuk kegiatan yang disukai/diingini'),
      AumModel(
          title:
              '203. Mengalami masalah untuk mengikuti kegiatan acara-acara gembira dan santai bersama kawan-kawan'),
      AumModel(
          title:
              '204. Tidak mempunyai kawan akrab untuk bersama-sama mengisi waktu senggang'),
      AumModel(
          title:
              '205. Mengalami masalah karena memikirkan atau membayangkan kesempatan waktu berlibur ditempat yang jauh, indah, tenang, dan menyenangkan'),
      AumModel(
          title:
              '216. Tidak mengetahui cara menggunakan waktu senggang yang ada'),
      AumModel(
          title:
              '217. Kekurangan sarana, seperti biaya, kendaraan, televisi, buku-buku bacaan, dan lain-lain untuk memanfaatkan waktu senggang'),
      AumModel(
          title:
              '218. Mengalami masalah karena cara melaksanakan kegiatan atau acara yang kurang tepat dalam menggunakan waktu senggang'),
      AumModel(
          title:
              '219. Mengalami masalah dalam menggunakan waktu senggang karena tidak memiliki keterampilan tertentu, seperti bermain musik, olahraga, menari, dan sebagainya'),
      AumModel(
          title:
              '220. Kurang berminat atau tidak ada hal yang menarik dalam memanfaatkan waktu senggang yang tersedia'),
    ];
    _result = <AumModel>[];
  }

  NetworkService _net;

  List<AumModel> _wsgs;
  List<AumModel> get wsgs => _wsgs;

  List<AumModel> _result;
  List<AumModel> get result => _result;

  AumModel am;

  String title;
  bool selected;

  void setNetworkService(NetworkService net) {
    _net = net;
  }

  void onTitleChange(String val) {
    title = val;
  }

  void onSeledtedChange(bool val) {
    selected = val;
  }

  void addToResult(AumModel wsg) {
    _result.add(wsg);
    _wsgs.remove(wsg);
    notifyListeners();
  }

  void removeFromResult(AumModel wsg) {
    _wsgs.add(wsg);
    _result.remove(wsg);
    notifyListeners();
  }

  void switchSelectedtStatus(AumModel wsg, bool newVal) {
    final int wsgIndex = _result.indexOf(wsg);
    _result[wsgIndex].selected = newVal;
    notifyListeners();
  }

  Future<void> addNewAum() async {
    final Response<dynamic> resp = await _net.request(
      '/aum/store',
      requestMethod: 'post',
      data: <String, dynamic>{
        'title': title,
        'selected': selected,
      },
    );
    final Map<String, dynamic> respData = resp.data as Map<String, dynamic>;

    if (respData != null) {
      Get.back<void>();
      print(respData);
    }
    notifyListeners();
  }

  Future<void> fetchAum() async {
    final Response<dynamic> resp = await _net.request('/aum/indexaum');

    final List<dynamic> respData = resp.data['data'] as List<dynamic>;

    for (final dynamic respData in respData) {
      _wsgs.add(AumModel.fromJson(respData as Map<String, dynamic>));
    }
    print(respData);
    notifyListeners();
  }
}
