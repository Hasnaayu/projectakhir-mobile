import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/aum_model.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/get_aum_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:dio/dio.dart';

class AumViewModel extends ChangeNotifier {
  AumViewModel() {
    _anms = <AumModel>[
      AumModel(
          title: '111. Mengalami masalah untuk pergi ke tempat peribadatan'),
      AumModel(
          title:
              '112. Mempunyai pandangan dan/atau kebiasaan yang tidak sesuai dengan kaidah-kaidah agama'),
      AumModel(
          title:
              '113. Tidak mampu melaksanakan tuntutan keagamaan dan/atau khawatir tidak mampu menghindari larangan yang ditentukan oleh agama'),
      AumModel(title: '114. Kurang menyukai pembicaraan tentang agama'),
      AumModel(
          title:
              '115. Ragu dan ingin memperoleh penjelasan lebih banyak tentang kaidah-kaidah agama'),
      AumModel(title: '116. Mengalami kesulitan dalam mendalami agama'),
      AumModel(
          title:
              '117. Tidak memiliki kecakapan dan/atau sarana untuk melaksanakan ibadah agama'),
      AumModel(
          title:
              '118. Mengalami masalah karena membandingkan agama yang satu dengan yang lainnya'),
      AumModel(title: '119. Bermasalah karena anggota keluarga tidak seagama'),
      AumModel(
          title: '120. Belum menjalankan ibadah agama sebagaimana diharapkan'),
      AumModel(
          title:
              '126. Berkata dusta dan/atau berbuat tidak jujur untuk tujuan-tujuan tertentu, seperti membohongi teman, berlaku curang dalam ujian'),
      AumModel(
          title:
              '127. Kurang mengetahui hal-hal yang menurut orang lain dianggap baik atau buruk, benar atau salah'),
      AumModel(
          title:
              '128. Tidak dapat mengambil keputusan tentang sesuatu karena kurang memahami baik-buruknya atau benar-salahnya sesuatu itu'),
      AumModel(
          title:
              '129. Merasa terganggu oleh kesalahan atau keburukan orang lain'),
      AumModel(
          title:
              '130. Tidak mengetahui cara-cara yang tepat untuk mengatakan kepada orang lain tentang sesuatu yang baik atau buruk, benar atau salah'),
      AumModel(
          title:
              '131. Khawatir atau merasa ketakutan akan akibat perbuatan melanggar kaidah-kaidah agama'),
      AumModel(
          title:
              '132. Kurang menyukai pembicaraan yang dilontarkan di tempat peribadatan'),
      AumModel(
          title:
              '133. Kurang taat dan/atau kurang khusyuk dalam menjalankan ibadah agama'),
      AumModel(
          title:
              '134. Mengalami masalah karena memiliki pandangan dan/atau sikap keagamaan yang cenderung fanatik atau berprasangka'),
      AumModel(
          title: '135. Meragukan manfaat ibadah dan/atau upacara keagamaan'),
      AumModel(
          title:
              '141. Merasa terganggu karena melakukan sesuatu yang menjadikan orang lain tidak senang'),
      AumModel(
          title:
              '142. Terlanjur berbicara, bertindak atau bersikap yang tidak layak kepada orang tua dan/atau orang lain'),
      AumModel(
          title:
              '143. Sering ditegur karena dianggap melakukan kesalahan, pelanggaran atau sesuatu yang tidak layak'),
      AumModel(
          title:
              '144. Mengalami masalah karena berbohong atau berkata tidak layak meskipun sebenarnya dengan maksud sekedar berolok-olok atau menimbulkan suasana gembira'),
      AumModel(
          title:
              '145. Tidak melakukan sesuatu yang sesungguhnya perlu dilakukan'),
      AumModel(title: '146. Takut dipersalahkan karena melanggar adat'),
      AumModel(
          title:
              '147. Mengalami masalah karena memiliki kebiasaan yang berbeda dari orang lain'),
      AumModel(
          title:
              '148. Terlanjur melakukan sesuatu perbuatan yang salah, atau melanggar nilai-nilai moral atau adat'),
      AumModel(title: '149. Merasa bersalah karena terpaksa mengingkari janji'),
      AumModel(
          title:
              '150. Mengalami persoalan karena berbeda pendapat tentang suatu aturan dalam adat'),
    ];
    _resultA = <AumModel>[];
    _dpis = <AumModel>[
      AumModel(title: '076. Sering mimpi buruk'),
      AumModel(
          title: '077. Cemas atau khawatir tentang sesuatu yang belum pasti'),
      AumModel(title: '078. Mudah lupa'),
      AumModel(title: '079. Sering melamun atau berkhayal'),
      AumModel(title: '080. Ceroboh atau kurang berhati-hati'),
      AumModel(title: '091. Sering murung dan/atau mudah patah semangat'),
      AumModel(
          title:
              '092. Mengalami kerugian atau kesulitan karena terlampau hati-hati'),
      AumModel(title: '093. Kurang serius menghadapi sesuatu yang penting'),
      AumModel(title: '094. Merasa hidup ini kurang berarti'),
      AumModel(title: '095. Sering gagal dan/atau mudah patah semangat '),
      AumModel(
          title:
              '106. Mudah gentar atau khawatir dalam menghadapi dan/atau mengemukakan sesuatu'),
      AumModel(title: '107. Penakut, pemalu, dan/atau mudah menjadi bingung'),
      AumModel(
          title:
              '108. Keras kepala atau sukar mengubah pendapat sendiri meskipun kata orang lain pendapat itu salah'),
      AumModel(title: '109. Takut mencoba sesuatu yang baru'),
      AumModel(title: '110. Mudah marah atau tidak mampu mengendalikan diri'),
      AumModel(title: '121. Merasa kesepian dan/atau takut ditinggal sendiri'),
      AumModel(
          title:
              '122. Sering bertingkah laku, bertindak, atau bersikap kekanak-kanakan'),
      AumModel(title: '123. Rendah diri atau kurang percaya diri'),
      AumModel(title: '124. Kurang terbuka terhadap orang lain'),
      AumModel(
          title:
              '125. Sering membesar-besarkan sesuatu yang sebenarnya tidak perlu'),
    ];
    _resultB = <AumModel>[];
    _edks = <AumModel>[
      AumModel(
          title:
              '181. Mengalami masalah karena kurang mampu berhemat atau kemampuan keuangan sangat tidak mencukupi, baik untuk keperluan sehari-hari maupun keperluan pekerjaan'),
      AumModel(
          title:
              '182. Khawatir tidak mampu menamatkan sekolah ini atau putus sekolah dan harus segera bekerja'),
      AumModel(
          title:
              '183. Mengalami masalah karena terlalu berhemat dan/atau ingin menabung'),
      AumModel(
          title:
              '184. Kekurangan dalam keuangan menyebabkan dalam pengembangan diri terhambat'),
      AumModel(
          title:
              '185. Untuk memenuhi keuangan terpaksa sekolah sambil bekerja'),
      AumModel(
          title: '196. Mengalami masalah karena ingin berpenghasilan sendiri'),
      AumModel(title: '197. Berhutang yang cukup memberatkan'),
      AumModel(
          title:
              '198. Besarnya uang yang diperoleh dan sumber-sumbernya tidak menentu'),
      AumModel(
          title:
              '199. Khawatir akan kondisi keuangan orang tua atau orang yang menjadi sumber keuangan; jangan-jangan harus menjual atau menggadaikan harta keluarga'),
      AumModel(
          title:
              '200. Mengalami masalah karena keuangan dikendalikan oleh orang lain'),
      AumModel(
          title:
              '211. Mengalami masalah karena membanding-bandingkan kondisi keuangan sendiri dengan kondisi keuangan orang lain'),
      AumModel(
          title:
              '212. Kesulitan dalam mendapatkan penghasilan sendiri sambil sekolah'),
      AumModel(
          title:
              '213. Mempertanyakan kemungkinan memperoleh beasiswa atau dana bantuan belajar lainnya'),
      AumModel(
          title:
              '214. Orang lain menganggap pelit dan/atau tidak mau membantu kawan yang sedang mengalami kesulitan keuangan'),
      AumModel(
          title:
              '215. Terpaksa berbagi pengeluaran keuangan dengan kakak atau adik atau anggota keluarga lain yang sama-sama membutuhkan biaya'),
    ];
    _resultC = <AumModel>[];
    _hmms = <AumModel>[
      AumModel(
          title:
              '156. Membutuhkan keterangan tentang persoalan seks, pacaran, dan/atau perkawinan'),
      AumModel(
          title:
              '157. Mengalami masalah karena malu dan kurang terbuka dalam membicarakan soal seks, pacar, dan/atau jodoh'),
      AumModel(
          title:
              '158. Khawatir tidak mendapatkan pacar atau jodoh yang baik/cocok'),
      AumModel(
          title:
              '159. Terlalu memikirkan tentang seks, percintaan, pacaran, atau perkawinan'),
      AumModel(
          title:
              '160. Mengalami masalah karena dilarang atau merasa tidak patut berpacaran'),
      AumModel(
          title:
              '171. Kurang mendapat perhatian dari jenis kelamin lain atau pacar'),
      AumModel(title: '172. Mengalami masalah karena ingin mempunyai pacar'),
      AumModel(
          title:
              '173. Canggung dalam menghadapi jenis kelamin lain atau pacar'),
      AumModel(title: '174. Sukar mengendalikan dorongan seksual'),
      AumModel(
          title:
              '175. Mengalami masalah dalam memilih teman akrab dari jenis kelamin lain atau pacar'),
      AumModel(
          title:
              '186. Mengalami masalah karena takut atau sudah terlalu jauh berhubungan dengan jenis kelamin lain atau pacar'),
      AumModel(
          title: '187. Bertepuk sebelah tangan dengan kawan akrab atau pacar'),
      AumModel(
          title:
              '188. Takut ditinggalkan pacar atau patah hati, cemburu atau cinta segitiga'),
      AumModel(title: '189. Khawatir akan dipaksa kawin'),
      AumModel(
          title:
              '190. Mengalami masalah karena sering dan mudah jatuh cinta dan/atau rindu kepada pacar'),
    ];
    _resultD = <AumModel>[];
    _hsos = <AumModel>[
      AumModel(title: '136. Tidak menyukai atau tidak disukai seseorang'),
      AumModel(
          title:
              '137. Merasa diperhatikan, dibicarakan atau diperolokkan orang lain'),
      AumModel(
          title:
              '138. Mengalami masalah karena ingin lebih terkenal atau lebih menarik atau lebih menyenangkan bagi orang lain'),
      AumModel(title: '139. Mempunyai kawan yang kurang disukai orang lain'),
      AumModel(
          title:
              '140. Tidak menyukai kawan akrab, hubungan sosial terbatas atau terisolir'),
      AumModel(title: '151. Kurang peduli terhadap orang lain'),
      AumModel(title: '152. Rapuh dalam berteman'),
      AumModel(
          title:
              '153. Merasa tidak dianggap penting, diremehkan atau dikecam oleh orang lain'),
      AumModel(
          title:
              '154. Mengalami masalah dengan orang lain karena kurang peduli terhadap diri sendiri'),
      AumModel(
          title:
              '155. Canggung dan/atau tidak lancar berkomunikasi dengan orang lain'),
      AumModel(
          title:
              '166. Tidak lincah dan kurang mengetahui tentang tata krama pergaulan'),
      AumModel(
          title:
              '167. Kurang pandai memimpin dan/atau mudah dipengaruhi orang lain'),
      AumModel(
          title:
              '168. Sering membantah atau tidak menyukai sesuatu yang dikatakan/dirasakan orang lain atau dikatakan sombong'),
      AumModel(
          title:
              '169. Mudah tersinggung atau sakit hati dalam berhubungan dengan orang lain'),
      AumModel(title: '170. Lambat menjalin persahabatan'),
    ];
    _resultE = <AumModel>[];
    _jdks = <AumModel>[
      AumModel(title: '001. Badan terlalu kurus, atau kurang gemuk'),
      AumModel(title: '002. Warna kulit kurang memuaskan'),
      AumModel(title: '003. Berat badan terus berkurang atau bertambah'),
      AumModel(title: '004. Badan terlalu pendek, atau terlalu gemuk'),
      AumModel(title: '005. Secara jasmaniah kurang menarik'),
      AumModel(
          title: '016. Fungsi dan/atau kondisi kesehatan mata kurang baik'),
      AumModel(title: '017. Mengalami gangguan tertentu karena cacat jasmani'),
      AumModel(
          title: '018. Fungsi dan/atau kondisi kesehatan hidung kurang baik'),
      AumModel(title: '019. Kondisi kesehatan kulit sering terganggu'),
      AumModel(title: '020. Gangguan pada gigi'),
      AumModel(
          title:
              '031. Fungsi dan/atau kondisi kerongkongan kurang baik atau sering terganggu, misalnya serak'),
      AumModel(title: '032. Gagap dalam berbicara'),
      AumModel(
          title: '033. Fungsi dan/atau kondisi kesehatan teling kurang baik'),
      AumModel(
          title:
              '034. Kurang mampu berolahraga karena kondisi jasmani kurang baik'),
      AumModel(title: '035. Gangguan pada pencernaan makanan'),
      AumModel(title: '046. Sering pusing dan/atau mudah sakit'),
      AumModel(title: '047. Mengalami gangguan setiap datang bulan'),
      AumModel(title: '048. Secara umum merasa tidak sehat'),
      AumModel(title: '049. Khawatir mengidap penyakit turunan'),
      AumModel(title: '050. Selera makan sering terganggu'),
      AumModel(title: '061. Mengidap penyakit kambuhan'),
      AumModel(title: '062. Alergi terhadap makanan atau keadaan tertentu'),
      AumModel(title: '063. Kurang atau susah tidur'),
      AumModel(
          title:
              '064. Mengalami gangguan akibat merokok atau minuman atau obat-obatan'),
      AumModel(
          title: '065. Khawatir tertular penyakit yang diderita orang lain'),
    ];
    _resultF = <AumModel>[];
    _kdps = <AumModel>[
      AumModel(
          title:
              '006. Belum mampu memikirkan dan memilih pekerjaan yang akan dijabat nantinya'),
      AumModel(
          title:
              '007. Belum mengetahui bakat diri sendiri untuk jabatan/pekerjaan apa'),
      AumModel(
          title:
              '008. Kurang memiliki pengetahuan yang luas tentang lapangan pekerjaan dan seluk beluk jenis-jenis pekerjaan'),
      AumModel(
          title:
              '009. Ingin memperoleh bantuan dalam mendapatkan pekerjaan sambilan untuk melatih diri bekerja sambil sekolah'),
      AumModel(
          title:
              '010. Khawatir akan pekerjaan yang dijabat nanti; jangan-jangan memberikan penghasilan yang tidak mencukupi'),
      AumModel(
          title: '021. Ragu akan kemampuan saya untuk sukses dalam bekerja'),
      AumModel(title: '022. Belum mampu merencanakan masa depan'),
      AumModel(title: '023. Takut akan bayangan masa depan'),
      AumModel(
          title:
              '024. Mengalami masalah karena membanding-bandingkan pekerjaan yang layak atau tidak layak untuk dijabat'),
      AumModel(
          title:
              '025. Khawatir diperlakukan secara tidak wajar atau tidak adil dalam mencari dan/atau melamar pekerjaan'),
      AumModel(
          title:
              '036. Kurang yakin terhadap kemampuan pendidikan sekarang ini dalam menyiapkan jebatan tertentu nantinya'),
      AumModel(
          title:
              '037. Ragu tentang kesempatan memperoleh pekerjaan sesuai dengan pendidikan yang diikuti sekarang ini'),
      AumModel(
          title:
              '038. Ingin mengikuti kegiatan pelajaran dan/atau latihan khusus tertentu yang benar-benar menunjang proses mencari dan melamar pekerjaan setamat pendidikan ini'),
      AumModel(
          title: '039. Cemas kalau menjadi penganggur setamat pendidikan ini'),
      AumModel(
          title:
              '040. Ragu apakah setamat pendidikan ini dapat bekerja secara mandiri'),
      AumModel(title: '051. Hasil belajar atau nilai-nilai kurang memuaskan'),
      AumModel(title: '052. Mengalami masalah dalam belajar kelompok'),
      AumModel(
          title:
              '053. Kurang berminat dan/atau kurang mampu mempelajari buku pelajaran'),
      AumModel(
          title:
              '054. Takut dan/atau kurang mampu berbicara di dalam kelas dan/atau di luar kelas'),
      AumModel(
          title:
              '055. Mengalami kesulitan dalam ejaan, tata bahasa dan/atau perbendaharaan katadalam Bahasa Indonesia'),
      AumModel(
          title:
              '066. Mengalami kesulitan dalam pemahaman dan penggunaan istilah dan/atau Bahasa Inggris dan/atau bahasa asing lainnya'),
      AumModel(
          title:
              '067. Kesulitan dalam membaca cepat dan/atau memahami isi buku pelajaran'),
      AumModel(title: '068. Takut menghadapi ulangan/ujian'),
      AumModel(
          title:
              '069. Khawatir memperoleh nilai rendah dalam ulangan/ujian ataupun tugas-tugas'),
      AumModel(title: '070. Kesulitan dalam mengingat materi pelajaran'),
      AumModel(
          title:
              '081. Cara guru menyajikan pelajaran terlalu kaku dan/atau membosankan'),
      AumModel(title: '082. Guru kurang bersahabat dan/atau membimbing siswa'),
      AumModel(
          title:
              '083. Mengalami masalah karena disiplin yang ditetapkan oleh guru'),
      AumModel(
          title:
              '084. Dirugikan karena dalam menilai kemajuan atau keberhasilan siswa guru kurang objektif'),
      AumModel(
          title: '085. Guru kurang memberikan tanggung jawab kepada siswa'),
      AumModel(
          title:
              '096. Khawatir akan dipaksa melanjutkan pelajaran setamat sekolah ini'),
      AumModel(
          title:
              '097. Kekurangan informasi tentang pendidikan lanjutan yang dapat dimasuki setamat sekolah ini'),
      AumModel(
          title:
              '098. Ragu tentang kemanfaatan pendidikan lanjutan setamat sekolah ini'),
      AumModel(
          title:
              '099. Khawatir tidak mampu melanjutkan pelajaran setamat dari sekolah ini dan/atau terlalu memikirkan pendidikan lanjutan setamat sekolah ini'),
      AumModel(
          title:
              '100. Ragu apakah sekolah sekarang ini mampu memberikan modal yang kuat bagi para siswanya untuk menempuh pendidikan yang lebih lanjut'),
    ];
    _resultG = <AumModel>[];
    _khks = <AumModel>[
      AumModel(
          title:
              '161. Bermasalah karena kedua orang tua hidup berpisah atau bercerai'),
      AumModel(
          title:
              '162. Mengalami masalah karena ayah dan/atau ibu kandung telah meninggal'),
      AumModel(
          title: '163. Mengkhawatirkan kondisi kesehatan anggota keluarga'),
      AumModel(
          title:
              '164. Mengalami masalah karena keadaan dan perlengkapan tempat tinggal dan/atau rumah orang tua kurang memadai'),
      AumModel(
          title:
              '165. Mengkhawatirkan kondisi orang tua yang bekerja terlalu berat'),
      AumModel(title: '176. Keluarga mengeluh tentang keadaan keuangan'),
      AumModel(
          title:
              '177. Mengkhawatirkan keadaan orang tua yang bertempat tinggal jauh'),
      AumModel(title: '178. Bermasalah karena ibu atau bapak akan kawin lagi'),
      AumModel(
          title:
              '179. Khawatir tidak mampu memenuhi tuntutan atau harapan orang tua atau anggota keluarga lain'),
      AumModel(
          title:
              '180. Membayangkan dan berpikir-pikir seandainya menjadi anak dari keluarga lain'),
      AumModel(
          title:
              '191. Kurang mendapat perhatian dan pengertian dari orang tua dan/atau anggota keluarga'),
      AumModel(title: '192. Mengalami kesulitan dengan bapak atau ibu tiri'),
      AumModel(
          title:
              '193. Diperlakukan tidak adil oleh orang tua atau oleh anggota keluarga lainnya'),
      AumModel(
          title:
              '194. Khawatir akan terjadinya pertentangan atau percekcokan dalam keluarga'),
      AumModel(
          title:
              '195. Hubungan dengan orang tua dan anggota keluarga kurang hangat, kurang harmonis dan/atau kurang menggembirakan'),
      AumModel(
          title:
              '206. Mengalami masalah karena menjadi anak tunggal, anak sulung, anak bungsu, satu-satunya anak laki-laki atau satu-satunya anak perempuan'),
      AumModel(
          title:
              '207. Hubungan kurang harmonis dengan kakak atau adik atau dengan anggota keluarga lainnya'),
      AumModel(
          title:
              '208. Orang tua atau anggota keluarga lainnya terlalu berkuasa atau kurang memberi kebebasan'),
      AumModel(
          title: '209. Dicurigai oleh orang tua atau anggota keluarga lain'),
      AumModel(
          title:
              '210. Bermasalah karena dirumah orang tua tinggal orang atau anggota keluarga lain'),
      AumModel(
          title:
              '221. Tinggal di lingkungan keluarga atau tetangga yang kurang menyenangkan'),
      AumModel(
          title:
              '222. Tidak sependapat dengan orang tua atau anggota keluarga tentang sesuatu yang direncanakan'),
      AumModel(
          title: '223. Orang tua kurang senang kawan-kawan datang ke rumah'),
      AumModel(
          title:
              '224. Mengalami masalah karena rindu dan ingin bertemu dengan orang tua dan/atau anggota keluarga lainnya'),
      AumModel(
          title:
              '225. Tidak betah dan ingin meninggalkan rumah karena keadaannya sangat tidak menyenangkan'),
    ];
    _resultH = <AumModel>[];
    _pdps = <AumModel>[
      AumModel(title: '011. Terpaksa atau ragu-ragu memasuki sekolah ini'),
      AumModel(title: '012. Meragukan kemanfaatan memasuki sekolah ini'),
      AumModel(title: '013. Sukar menyesuaikan diri dengan keadaan sekolah'),
      AumModel(
          title:
              '014. Kurang meminati pelajaran atau jurusan atau program yang diikuti'),
      AumModel(
          title:
              '015. Khawatir tidak dapat menamatkan sekolah pada waktu yang direncanakan'),
      AumModel(title: '026. Sering tidak masuk sekolah'),
      AumModel(title: '027. Tugas-tugas pelajaran tidak selesai pada waktunya'),
      AumModel(
          title:
              '028. Sukar memahami penjelasan guru sewaktu pelajaran berlangsung'),
      AumModel(
          title: '029. Mengalami kesulitan dalam membuat catatan pelajaran'),
      AumModel(
          title: '030. Terpaksa mengikuti mata pelajaran yang tidak disukai'),
      AumModel(
          title:
              '041. Gelisah dan/atau melakukan kegiatan tidak menentu sewaktu pelajaran berlangsung, misalnya membuat coret-coretan dalam buku, cenderung mengganggu teman'),
      AumModel(title: '042. Sering malas belajar'),
      AumModel(title: '043. Kurang konsentrasi dalam mengikuti pelajaran'),
      AumModel(
          title:
              '044. Khawatir tugas-tugas pelajaran hasilnya kurang memuaskan atau rendah'),
      AumModel(
          title:
              '045. Mengalami masalah karena kemajuan atau hasil belajar hanya diberitahukan pada akhir catur wulan'),
      AumModel(title: '056. Mengalami masalah dalam menjawab pertanyaan ujian'),
      AumModel(
          title:
              '057. Tidak mengetahui dan/atau tidak mampu menerapkan cara-cara belajar yang baik'),
      AumModel(title: '058. Kekurangan waktu belajar'),
      AumModel(
          title:
              '059. Mengalami masalah dalam menyusun makalah, laporan, atau karya tulis lainnya'),
      AumModel(title: '060. Sukar mendapatkan buku pelajaran yang diperlukan'),
      AumModel(title: '071. Seringkali tidak siap menghadapi ujian'),
      AumModel(title: '072. Sarana belajar di sekolah kurang memadai'),
      AumModel(
          title:
              '073. Orang tua kurang peduli dan/atau kurang membantu kegiatan belajar di sekolah dan/atau di rumah'),
      AumModel(
          title:
              '074. Anggota keluarga kurang peduli dan/atau kurang membantu kegiatan belajar di sekolah dan/atau di rumah'),
      AumModel(title: '075. Sarana belajar di rumah kurang memadai'),
      AumModel(title: '086. Guru kurang adil atau pilih kasih'),
      AumModel(title: '087. Ingin dekat dengan guru'),
      AumModel(
          title:
              '088. Guru kurang memperhatikan kebutuhan dan/atau keadaan siswa'),
      AumModel(title: '089. Mendapatkan perhatian khusus dari guru tertentu'),
      AumModel(
          title:
              '090. Dalam memberikan pelajaran dan/atau berhubungan dengan siswa, sikap dan/atau tindakan guru sering berubah-ubah, sehingga membingungkan siswa'),
      AumModel(
          title:
              '101. Khawatir tidak tersedia biaya untuk melanjutkan pekerjaan setamat sekolah ini'),
      AumModel(
          title:
              '102. Tidak dapat mengambil keputusan tentang apakah akan mencari pekerjaan atau melanjutkan pelajaran setamat sekolah ini'),
      AumModel(
          title:
              '103. Khawatir tuntutan dan proses pendidikan lanjutansetamat sekolah ini sangat berat'),
      AumModel(
          title:
              '104. Terdapat pertentangan pendapat dengan orang tua dan/atau anggota keluarga lain tentang rencana melanjutkan pelajaran setamat sekolah ini'),
      AumModel(
          title:
              '105. Khawatir tidak mampu bersaing dalam upaya memasuki pendidikan lanjutan setamat sekolah ini'),
    ];
    _resultI = <AumModel>[];
    _wsgs = <AumModel>[
      AumModel(
          title:
              '201. Kekurangan waktu senggang, seperti waktu istirahat, waktu luang di sekolah ataupun di rumah, waktu libur untuk bersikap santai dan/atau melakukan kegiatan yang menyenangkan atau rekreasi'),
      AumModel(
          title:
              '202. Tidak diperkenankan atau kurang bebas dalam menggunakan waktu senggang yang tersedia untuk kegiatan yang disukai/diingini'),
      AumModel(
          title:
              '203. Mengalami masalah untuk mengikuti kegiatan acara-acara gembira dan santai bersama kawan-kawan'),
      AumModel(
          title:
              '204. Tidak mempunyai kawan akrab untuk bersama-sama mengisi waktu senggang'),
      AumModel(
          title:
              '205. Mengalami masalah karena memikirkan atau membayangkan kesempatan waktu berlibur ditempat yang jauh, indah, tenang, dan menyenangkan'),
      AumModel(
          title:
              '216. Tidak mengetahui cara menggunakan waktu senggang yang ada'),
      AumModel(
          title:
              '217. Kekurangan sarana, seperti biaya, kendaraan, televisi, buku-buku bacaan, dan lain-lain untuk memanfaatkan waktu senggang'),
      AumModel(
          title:
              '218. Mengalami masalah karena cara melaksanakan kegiatan atau acara yang kurang tepat dalam menggunakan waktu senggang'),
      AumModel(
          title:
              '219. Mengalami masalah dalam menggunakan waktu senggang karena tidak memiliki keterampilan tertentu, seperti bermain musik, olahraga, menari, dan sebagainya'),
      AumModel(
          title:
              '220. Kurang berminat atau tidak ada hal yang menarik dalam memanfaatkan waktu senggang yang tersedia'),
    ];
    _resultJ = <AumModel>[];
  }

  NetworkService _net;

  List<AumModel> _anms;
  List<AumModel> get anms => _anms;
  List<AumModel> _dpis;
  List<AumModel> get dpis => _dpis;
  List<AumModel> _edks;
  List<AumModel> get edks => _edks;
  List<AumModel> _hmms;
  List<AumModel> get hmms => _hmms;
  List<AumModel> _hsos;
  List<AumModel> get hsos => _hsos;
  List<AumModel> _jdks;
  List<AumModel> get jdks => _jdks;
  List<AumModel> _kdps;
  List<AumModel> get kdps => _kdps;
  List<AumModel> _khks;
  List<AumModel> get khks => _khks;
  List<AumModel> _pdps;
  List<AumModel> get pdps => _pdps;
  List<AumModel> _wsgs;
  List<AumModel> get wsgs => _wsgs;

  List<AumModel> _resultA;
  List<AumModel> get resultA => _resultA;
  List<AumModel> _resultB;
  List<AumModel> get resultB => _resultB;
  List<AumModel> _resultC;
  List<AumModel> get resultC => _resultC;
  List<AumModel> _resultD;
  List<AumModel> get resultD => _resultD;
  List<AumModel> _resultE;
  List<AumModel> get resultE => _resultE;
  List<AumModel> _resultF;
  List<AumModel> get resultF => _resultF;
  List<AumModel> _resultG;
  List<AumModel> get resultG => _resultG;
  List<AumModel> _resultH;
  List<AumModel> get resultH => _resultH;
  List<AumModel> _resultI;
  List<AumModel> get resultI => _resultI;
  List<AumModel> _resultJ;
  List<AumModel> get resultJ => _resultJ;

  List<GetAumModel> _aums = [];
  List<GetAumModel> get aums => _aums;

  AumModel am;

  String title;
  bool selected;
  int intVal;

  void setNetworkService(NetworkService net) {
    _net = net;
  }

  void onTitleChange(String val) {
    title = val;
  }

  void onSeledtedChange(bool val) {
    selected = val;
  }

  //anm
  void addToResultA(AumModel anm) {
    _resultA.add(anm);
    _anms.remove(anm);
    notifyListeners();
  }

  void removeFromResultA(AumModel anm) {
    _anms.add(anm);
    _resultA.remove(anm);
    notifyListeners();
  }

  int anmIndex;
  void switchSelectedtStatusA(AumModel anm, bool newVal) {
    anmIndex = _resultA.indexOf(anm);
    _resultA[anmIndex].selected = newVal;
    if (newVal == true) {
      intVal = 1;
    } else {
      intVal = 0;
    }
    print(intVal);
    title = anm.title;
    print(title);
    print(_resultA[anmIndex].selected);
    notifyListeners();
  }

  Future<void> addNewAumA() async {
    var formData = FormData.fromMap({
      'title': title,
      'selected': intVal,
    });
    var response =
        await _net.request('/storeaum', requestMethod: 'post', data: formData);

    print(response);

    Get.toNamed<void>('/resulthmm');
    notifyListeners();
  }

  //dpi
  void addToResultB(AumModel dpi) {
    _resultB.add(dpi);
    _dpis.remove(dpi);
    notifyListeners();
  }

  void removeFromResultB(AumModel dpi) {
    _dpis.add(dpi);
    _resultB.remove(dpi);
    notifyListeners();
  }

  int dpiIndex;
  void switchSelectedtStatusB(AumModel dpi, bool newVal) {
    dpiIndex = _resultB.indexOf(dpi);
    _resultB[dpiIndex].selected = newVal;
    if (newVal == true) {
      intVal = 1;
    } else {
      intVal = 0;
    }
    print(intVal);
    title = dpi.title;
    print(title);
    print(_resultB[dpiIndex].selected);
    notifyListeners();
  }

  Future<void> addNewAumB() async {
    var formData = FormData.fromMap({
      'title': title,
      'selected': intVal,
    });
    var response =
        await _net.request('/storeaum', requestMethod: 'post', data: formData);

    print(response);

    Get.toNamed<void>('/resulthso');
    notifyListeners();
  }

  //edk
  void addToResultC(AumModel edk) {
    _resultC.add(edk);
    _edks.remove(edk);
    notifyListeners();
  }

  void removeFromResultC(AumModel edk) {
    _edks.add(edk);
    _resultC.remove(edk);
    notifyListeners();
  }

  int edkIndex;
  void switchSelectedtStatusC(AumModel edk, bool newVal) {
    edkIndex = _resultC.indexOf(edk);
    _resultC[edkIndex].selected = newVal;
    if (newVal == true) {
      intVal = 1;
    } else {
      intVal = 0;
    }
    print(intVal);
    title = edk.title;
    print(title);
    print(_resultC[edkIndex].selected);
    notifyListeners();
  }

  Future<void> addNewAumC() async {
    var formData = FormData.fromMap({
      'title': title,
      'selected': intVal,
    });
    var response =
        await _net.request('/storeaum', requestMethod: 'post', data: formData);

    print(response);

    Get.toNamed<void>('/resultkdp');
    notifyListeners();
  }

  //hmm
  void addToResultD(AumModel hmm) {
    _resultD.add(hmm);
    _hmms.remove(hmm);
    notifyListeners();
  }

  void removeFromResultD(AumModel hmm) {
    _hmms.add(hmm);
    _resultD.remove(hmm);
    notifyListeners();
  }

  int hmmIndex;
  void switchSelectedtStatusD(AumModel hmm, bool newVal) {
    hmmIndex = _resultD.indexOf(hmm);
    _resultD[hmmIndex].selected = newVal;
    if (newVal == true) {
      intVal = 1;
    } else {
      intVal = 0;
    }
    print(intVal);
    title = hmm.title;
    print(title);
    print(_resultD[hmmIndex].selected);
    notifyListeners();
  }

  Future<void> addNewAumD() async {
    var formData = FormData.fromMap({
      'title': title,
      'selected': intVal,
    });
    var response =
        await _net.request('/storeaum', requestMethod: 'post', data: formData);

    print(response);

    Get.toNamed<void>('/resultkhk');
    notifyListeners();
  }

  //hso
  void addToResultE(AumModel hso) {
    _resultE.add(hso);
    _hsos.remove(hso);
    notifyListeners();
  }

  void removeFromResultE(AumModel hso) {
    _hsos.add(hso);
    _resultE.remove(hso);
    notifyListeners();
  }

  int hsoIndex;
  void switchSelectedtStatusE(AumModel hso, bool newVal) {
    hsoIndex = _resultE.indexOf(hso);
    _resultE[hsoIndex].selected = newVal;
    if (newVal == true) {
      intVal = 1;
    } else {
      intVal = 0;
    }
    print(intVal);
    title = hso.title;
    print(title);
    print(_resultE[hsoIndex].selected);
    notifyListeners();
  }

  Future<void> addNewAumE() async {
    var formData = FormData.fromMap({
      'title': title,
      'selected': intVal,
    });
    var response =
        await _net.request('/storeaum', requestMethod: 'post', data: formData);

    print(response);

    Get.toNamed<void>('/resultedk');
    notifyListeners();
  }

  //jdk
  void addToResultF(AumModel jdk) {
    _resultF.add(jdk);
    _jdks.remove(jdk);
    notifyListeners();
  }

  void removeFromResultF(AumModel jdk) {
    _jdks.add(jdk);
    _resultF.remove(jdk);
    notifyListeners();
  }

  int jdkIndex;
  void switchSelectedtStatusF(AumModel jdk, bool newVal) {
    jdkIndex = _resultF.indexOf(jdk);
    _resultF[jdkIndex].selected = newVal;
    if (newVal == true) {
      intVal = 1;
    } else {
      intVal = 0;
    }
    print(intVal);
    title = jdk.title;
    print(title);
    print(_resultF[jdkIndex].selected);
    notifyListeners();
  }

  Future<void> addNewAumF() async {
    var formData = FormData.fromMap({
      'title': title,
      'selected': intVal,
    });
    var response =
        await _net.request('/storeaum', requestMethod: 'post', data: formData);

    print(response);

    Get.toNamed<void>('/resultdpi');
    notifyListeners();
  }

  //kdp
  void addToResultG(AumModel kdp) {
    _resultG.add(kdp);
    _kdps.remove(kdp);
    notifyListeners();
  }

  void removeFromResultG(AumModel kdp) {
    _kdps.add(kdp);
    _resultG.remove(kdp);
    notifyListeners();
  }

  int kdpIndex;
  void switchSelectedtStatusG(AumModel kdp, bool newVal) {
    kdpIndex = _resultG.indexOf(kdp);
    _resultG[kdpIndex].selected = newVal;
    if (newVal == true) {
      intVal = 1;
    } else {
      intVal = 0;
    }
    print(intVal);
    title = kdp.title;
    print(title);
    print(_resultG[kdpIndex].selected);
    notifyListeners();
  }

  Future<void> addNewAumG() async {
    var formData = FormData.fromMap({
      'title': title,
      'selected': intVal,
    });
    var response =
        await _net.request('/storeaum', requestMethod: 'post', data: formData);

    print(response);

    Get.toNamed<void>('/resultpdp');
    notifyListeners();
  }

  //khk
  void addToResultH(AumModel khk) {
    _resultH.add(khk);
    _khks.remove(khk);
    notifyListeners();
  }

  void removeFromResultH(AumModel khk) {
    _khks.add(khk);
    _resultH.remove(khk);
    notifyListeners();
  }

  int khkIndex;
  void switchSelectedtStatusH(AumModel khk, bool newVal) {
    khkIndex = _resultH.indexOf(khk);
    _resultH[khkIndex].selected = newVal;
    if (newVal == true) {
      intVal = 1;
    } else {
      intVal = 0;
    }
    print(intVal);
    title = khk.title;
    print(title);
    print(_resultH[khkIndex].selected);
    notifyListeners();
  }

  Future<void> addNewAumH() async {
    var formData = FormData.fromMap({
      'title': title,
      'selected': intVal,
    });
    var response =
        await _net.request('/storeaum', requestMethod: 'post', data: formData);

    print(response);

    Get.toNamed<void>('/resultwsg');
    notifyListeners();
  }

  //pdp
  void addToResultI(AumModel pdp) {
    _resultI.add(pdp);
    _pdps.remove(pdp);
    notifyListeners();
  }

  void removeFromResultI(AumModel pdp) {
    _pdps.add(pdp);
    _resultI.remove(pdp);
    notifyListeners();
  }

  int pdpIndex;
  void switchSelectedtStatusI(AumModel pdp, bool newVal) {
    pdpIndex = _resultI.indexOf(pdp);
    _resultI[pdpIndex].selected = newVal;
    if (newVal == true) {
      intVal = 1;
    } else {
      intVal = 0;
    }
    print(intVal);
    title = pdp.title;
    print(title);
    print(_resultI[pdpIndex].selected);
    notifyListeners();
  }

  Future<void> addNewAumI() async {
    var formData = FormData.fromMap({
      'title': title,
      'selected': intVal,
    });
    var response =
        await _net.request('/storeaum', requestMethod: 'post', data: formData);

    print(response);

    Get.toNamed<void>('/resultanm');
    notifyListeners();
  }

  //wsg
  void addToResultJ(AumModel wsg) {
    _resultJ.add(wsg);
    _wsgs.remove(wsg);
    notifyListeners();
  }

  void removeFromResultJ(AumModel wsg) {
    _wsgs.add(wsg);
    _resultJ.remove(wsg);
    notifyListeners();
  }

  int wsgIndex;
  void switchSelectedtStatusJ(AumModel wsg, bool newVal) {
    wsgIndex = _resultJ.indexOf(wsg);
    _resultJ[wsgIndex].selected = newVal;
    if (newVal == true) {
      intVal = 1;
    } else {
      intVal = 0;
    }
    print(intVal);
    title = wsg.title;
    print(title);
    print('am.title');
    print(_resultJ[wsgIndex].selected);
    notifyListeners();
  }

  Future<void> addNewAumJ() async {
    var formData = FormData.fromMap({
      'title': title,
      'selected': intVal,
    });
    var response =
        await _net.request('/storeaum', requestMethod: 'post', data: formData);

    print(response);

    Get.toNamed<void>('/formpage');
    notifyListeners();
  }

  Future<void> fetchAum(int id) async {
    final Response<dynamic> resp = await _net.request('/getaum/$id');
    List<dynamic> listData = resp.data['data'];
    print(listData);
    for (dynamic data in listData)
      _aums.add(GetAumModel.fromJson(data as Map<String, dynamic>));
    print(id);
    notifyListeners();
  }

  Future<void> removeEarlierData(int id) async {
    if (_aums != null) _aums.clear();
    print(_aums);
    fetchDataList(id);
    notifyListeners();
  }

  Future<void> fetchDataList(int id) async {
    if (_aums != null) {
      fetchAum(id);
    } else {
      print('datas have been fetched.');
    }
    notifyListeners();
  }
}
