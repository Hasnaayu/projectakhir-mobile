import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/aum_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:get/route_manager.dart';

class KhkService extends ChangeNotifier {
  KhkService() {
    _khks = <AumModel>[
      AumModel(
          title:
              '161. Bermasalah karena kedua orang tua hidup berpisah atau bercerai'),
      AumModel(
          title:
              '162. Mengalami masalah karena ayah dan/atau ibu kandung telah meninggal'),
      AumModel(
          title: '163. Mengkhawatirkan kondisi kesehatan anggota keluarga'),
      AumModel(
          title:
              '164. Mengalami masalah karena keadaan dan perlengkapan tempat tinggal dan/atau rumah orang tua kurang memadai'),
      AumModel(
          title:
              '165. Mengkhawatirkan kondisi orang tua yang bekerja terlalu berat'),
      AumModel(title: '176. Keluarga mengeluh tentang keadaan keuangan'),
      AumModel(
          title:
              '177. Mengkhawatirkan keadaan orang tua yang bertempat tinggal jauh'),
      AumModel(title: '178. Bermasalah karena ibu atau bapak akan kawin lagi'),
      AumModel(
          title:
              '179. Khawatir tidak mampu memenuhi tuntutan atau harapan orang tua atau anggota keluarga lain'),
      AumModel(
          title:
              '180. Membayangkan dan berpikir-pikir seandainya menjadi anak dari keluarga lain'),
      AumModel(
          title:
              '191. Kurang mendapat perhatian dan pengertian dari orang tua dan/atau anggota keluarga'),
      AumModel(title: '192. Mengalami kesulitan dengan bapak atau ibu tiri'),
      AumModel(
          title:
              '193. Diperlakukan tidak adil oleh orang tua atau oleh anggota keluarga lainnya'),
      AumModel(
          title:
              '194. Khawatir akan terjadinya pertentangan atau percekcokan dalam keluarga'),
      AumModel(
          title:
              '195. Hubungan dengan orang tua dan anggota keluarga kurang hangat, kurang harmonis dan/atau kurang menggembirakan'),
      AumModel(
          title:
              '206. Mengalami masalah karena menjadi anak tunggal, anak sulung, anak bungsu, satu-satunya anak laki-laki atau satu-satunya anak perempuan'),
      AumModel(
          title:
              '207. Hubungan kurang harmonis dengan kakak atau adik atau dengan anggota keluarga lainnya'),
      AumModel(
          title:
              '208. Orang tua atau anggota keluarga lainnya terlalu berkuasa atau kurang memberi kebebasan'),
      AumModel(
          title: '209. Dicurigai oleh orang tua atau anggota keluarga lain'),
      AumModel(
          title:
              '210. Bermasalah karena dirumah orang tua tinggal orang atau anggota keluarga lain'),
      AumModel(
          title:
              '221. Tinggal di lingkungan keluarga atau tetangga yang kurang menyenangkan'),
      AumModel(
          title:
              '222. Tidak sependapat dengan orang tua atau anggota keluarga tentang sesuatu yang direncanakan'),
      AumModel(
          title: '223. Orang tua kurang senang kawan-kawan datang ke rumah'),
      AumModel(
          title:
              '224. Mengalami masalah karena rindu dan ingin bertemu dengan orang tua dan/atau anggota keluarga lainnya'),
      AumModel(
          title:
              '225. Tidak betah dan ingin meninggalkan rumah karena keadaannya sangat tidak menyenangkan'),
    ];
    _result = <AumModel>[];
  }

  NetworkService _net;

  List<AumModel> _khks;
  List<AumModel> get khks => _khks;

  List<AumModel> _result;
  List<AumModel> get result => _result;

  AumModel am;

  String title;
  bool selected;

  void setNetworkService(NetworkService net) {
    _net = net;
  }

  void onTitleChange(String val) {
    title = val;
  }

  void onSeledtedChange(bool val) {
    selected = val;
  }

  void addToResult(AumModel khk) {
    _result.add(khk);
    _khks.remove(khk);
    notifyListeners();
  }

  void removeFromResult(AumModel khk) {
    _khks.add(khk);
    _result.remove(khk);
    notifyListeners();
  }

  void switchSelectedtStatus(AumModel khk, bool newVal) {
    final int khkIndex = _result.indexOf(khk);
    _result[khkIndex].selected = newVal;
    notifyListeners();
  }

  Future<void> addNewAum() async {
    final Response<dynamic> resp = await _net.request(
      '/aum/store',
      requestMethod: 'post',
      data: <String, dynamic>{
        'title': title,
        'selected': selected,
      },
    );
    final Map<String, dynamic> respData = resp.data as Map<String, dynamic>;

    if (respData != null) {
      Get.back<void>();
      print(respData);
    }
    notifyListeners();
  }

  Future<void> fetchAum() async {
    final Response<dynamic> resp = await _net.request('/aum/indexaum');

    final List<dynamic> respData = resp.data['data'] as List<dynamic>;

    for (final dynamic respData in respData) {
      _khks.add(AumModel.fromJson(respData as Map<String, dynamic>));
    }
    print(respData);
    notifyListeners();
  }
}
