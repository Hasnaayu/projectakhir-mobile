import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/aum_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:dio/dio.dart';

class JdkService extends ChangeNotifier {
  JdkService() {
    _jdks = <AumModel>[
      AumModel(title: '001. Badan terlalu kurus, atau kurang gemuk'),
      AumModel(title: '002. Warna kulit kurang memuaskan'),
      AumModel(title: '003. Berat badan terus berkurang atau bertambah'),
      AumModel(title: '004. Badan terlalu pendek, atau terlalu gemuk'),
      AumModel(title: '005. Secara jasmaniah kurang menarik'),
      AumModel(
          title: '016. Fungsi dan/atau kondisi kesehatan mata kurang baik'),
      AumModel(title: '017. Mengalami gangguan tertentu karena cacat jasmani'),
      AumModel(
          title: '018. Fungsi dan/atau kondisi kesehatan hidung kurang baik'),
      AumModel(title: '019. Kondisi kesehatan kulit sering terganggu'),
      AumModel(title: '020. Gangguan pada gigi'),
      AumModel(
          title:
              '031. Fungsi dan/atau kondisi kerongkongan kurang baik atau sering terganggu, misalnya serak'),
      AumModel(title: '032. Gagap dalam berbicara'),
      AumModel(
          title: '033. Fungsi dan/atau kondisi kesehatan teling kurang baik'),
      AumModel(
          title:
              '034. Kurang mampu berolahraga karena kondisi jasmani kurang baik'),
      AumModel(title: '035. Gangguan pada pencernaan makanan'),
      AumModel(title: '046. Sering pusing dan/atau mudah sakit'),
      AumModel(title: '047. Mengalami gangguan setiap datang bulan'),
      AumModel(title: '048. Secara umum merasa tidak sehat'),
      AumModel(title: '049. Khawatir mengidap penyakit turunan'),
      AumModel(title: '050. Selera makan sering terganggu'),
      AumModel(title: '061. Mengidap penyakit kambuhan'),
      AumModel(title: '062. Alergi terhadap makanan atau keadaan tertentu'),
      AumModel(title: '063. Kurang atau susah tidur'),
      AumModel(
          title:
              '064. Mengalami gangguan akibat merokok atau minuman atau obat-obatan'),
      AumModel(
          title: '065. Khawatir tertular penyakit yang diderita orang lain'),
    ];
    _result = <AumModel>[];
  }

  NetworkService _net;

  List<AumModel> _jdks;
  List<AumModel> get jdks => _jdks;

  List<AumModel> _result;
  List<AumModel> get result => _result;

  AumModel am;

  String title;
  bool selected;

  void setNetworkService(NetworkService net) {
    _net = net;
  }

  void onTitleChange(String val) {
    title = val;
  }

  void onSeledtedChange(bool val) {
    selected = val;
  }

  void addToResult(AumModel jdk) {
    _result.add(jdk);
    _jdks.remove(jdk);
    notifyListeners();
  }

  void removeFromResult(AumModel jdk) {
    _jdks.add(jdk);
    _result.remove(jdk);
    notifyListeners();
  }

  void switchSelectedtStatus(AumModel jdk, bool newVal) {
    final int jdkIndex = _result.indexOf(jdk);
    _result[jdkIndex].selected = newVal;
    notifyListeners();
  }

  Future<void> addNewAum() async {
    final Response<dynamic> resp = await _net.request(
      '/aum/store',
      requestMethod: 'post',
      data: <String, dynamic>{
        'title': title,
        'selected': selected,
      },
    );
    final Map<String, dynamic> respData = resp.data as Map<String, dynamic>;

    if (respData != null) {
      Get.back<void>();
      print(respData);
    }
    notifyListeners();
  }

  Future<void> fetchAum() async {
    final Response<dynamic> resp = await _net.request('/aum/indexaum');

    final List<dynamic> respData = resp.data['data'] as List<dynamic>;

    for (final dynamic respData in respData) {
      _jdks.add(AumModel.fromJson(respData as Map<String, dynamic>));
    }
    print(respData);
    notifyListeners();
  }
}
