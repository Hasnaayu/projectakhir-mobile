import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/aum_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:dio/dio.dart';

class AnmService extends ChangeNotifier {
  AnmService() {
    _anms = <AumModel>[
      AumModel(
          title: '111. Mengalami masalah untuk pergi ke tempat peribadatan'),
      AumModel(
          title:
              '112. Mempunyai pandangan dan/atau kebiasaan yang tidak sesuai dengan kaidah-kaidah agama'),
      AumModel(
          title:
              '113. Tidak mampu melaksanakan tuntutan keagamaan dan/atau khawatir tidak mampu menghindari larangan yang ditentukan oleh agama'),
      AumModel(title: '114. Kurang menyukai pembicaraan tentang agama'),
      AumModel(
          title:
              '115. Ragu dan ingin memperoleh penjelasan lebih banyak tentang kaidah-kaidah agama'),
      AumModel(title: '116. Mengalami kesulitan dalam mendalami agama'),
      AumModel(
          title:
              '117. Tidak memiliki kecakapan dan/atau sarana untuk melaksanakan ibadah agama'),
      AumModel(
          title:
              '118. Mengalami masalah karena membandingkan agama yang satu dengan yang lainnya'),
      AumModel(title: '119. Bermasalah karena anggota keluarga tidak seagama'),
      AumModel(
          title: '120. Belum menjalankan ibadah agama sebagaimana diharapkan'),
      AumModel(
          title:
              '126. Berkata dusta dan/atau berbuat tidak jujur untuk tujuan-tujuan tertentu, seperti membohongi teman, berlaku curang dalam ujian'),
      AumModel(
          title:
              '127. Kurang mengetahui hal-hal yang menurut orang lain dianggap baik atau buruk, benar atau salah'),
      AumModel(
          title:
              '128. Tidak dapat mengambil keputusan tentang sesuatu karena kurang memahami baik-buruknya atau benar-salahnya sesuatu itu'),
      AumModel(
          title:
              '129. Merasa terganggu oleh kesalahan atau keburukan orang lain'),
      AumModel(
          title:
              '130. Tidak mengetahui cara-cara yang tepat untuk mengatakan kepada orang lain tentang sesuatu yang baik atau buruk, benar atau salah'),
      AumModel(
          title:
              '131. Khawatir atau merasa ketakutan akan akibat perbuatan melanggar kaidah-kaidah agama'),
      AumModel(
          title:
              '132. Kurang menyukai pembicaraan yang dilontarkan di tempat peribadatan'),
      AumModel(
          title:
              '133. Kurang taat dan/atau kurang khusyuk dalam menjalankan ibadah agama'),
      AumModel(
          title:
              '134. Mengalami masalah karena memiliki pandangan dan/atau sikap keagamaan yang cenderung fanatik atau berprasangka'),
      AumModel(
          title: '135. Meragukan manfaat ibadah dan/atau upacara keagamaan'),
      AumModel(
          title:
              '141. Merasa terganggu karena melakukan sesuatu yang menjadikan orang lain tidak senang'),
      AumModel(
          title:
              '142. Terlanjur berbicara, bertindak atau bersikap yang tidak layak kepada orang tua dan/atau orang lain'),
      AumModel(
          title:
              '143. Sering ditegur karena dianggap melakukan kesalahan, pelanggaran atau sesuatu yang tidak layak'),
      AumModel(
          title:
              '144. Mengalami masalah karena berbohong atau berkata tidak layak meskipun sebenarnya dengan maksud sekedar berolok-olok atau menimbulkan suasana gembira'),
      AumModel(
          title:
              '145. Tidak melakukan sesuatu yang sesungguhnya perlu dilakukan'),
      AumModel(title: '146. Takut dipersalahkan karena melanggar adat'),
      AumModel(
          title:
              '147. Mengalami masalah karena memiliki kebiasaan yang berbeda dari orang lain'),
      AumModel(
          title:
              '148. Terlanjur melakukan sesuatu perbuatan yang salah, atau melanggar nilai-nilai moral atau adat'),
      AumModel(title: '149. Merasa bersalah karena terpaksa mengingkari janji'),
      AumModel(
          title:
              '150. Mengalami persoalan karena berbeda pendapat tentang suatu aturan dalam adat'),
    ];
    _result = <AumModel>[];
  }

  NetworkService _net;

  List<AumModel> _anms;
  List<AumModel> get anms => _anms;

  List<AumModel> _result;
  List<AumModel> get result => _result;

  AumModel am;

  String title;
  bool selected;

  void setNetworkService(NetworkService net) {
    _net = net;
  }

  void onTitleChange(String val) {
    title = val;
  }

  void onSeledtedChange(bool val) {
    selected = val;
  }

  void addToResult(AumModel anm) {
    _result.add(anm);
    _anms.remove(anm);
    notifyListeners();
  }

  void removeFromResult(AumModel anm) {
    _anms.add(anm);
    _result.remove(anm);
    notifyListeners();
  }

  void switchSelectedtStatus(AumModel anm, bool newVal) {
    final int anmIndex = _result.indexOf(anm);
    _result[anmIndex].selected = newVal;
    notifyListeners();
  }

  Future<void> addNewAum() async {
    final Response<dynamic> resp = await _net.request(
      '/aum/store',
      requestMethod: 'post',
      data: <String, dynamic>{
        'title': title,
        'selected': selected,
      },
    );
    final Map<String, dynamic> respData = resp.data as Map<String, dynamic>;

    if (respData != null) {
      Get.back<void>();
      print(respData);
    }
    notifyListeners();
  }

  Future<void> fetchAum() async {
    final Response<dynamic> resp = await _net.request('/aum/indexaum');

    final List<dynamic> respData = resp.data['data'] as List<dynamic>;

    for (final dynamic respData in respData) {
      _anms.add(AumModel.fromJson(respData as Map<String, dynamic>));
    }
    print(respData);
    notifyListeners();
  }
}
