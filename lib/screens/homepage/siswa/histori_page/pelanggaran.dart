import 'dart:convert';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/add_pelanggaran.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/kelas_vm.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/pelanggaran_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:flutter/scheduler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ListPelanggaran extends StatefulWidget {
  @override
  _ListPelanggaranState createState() => _ListPelanggaranState();
}

class _ListPelanggaranState extends State<ListPelanggaran> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context.read<PelanggaranViewModel>().setNetworkService(
          context.read<NetworkService>()); //for fetchPelanggaran()
      context.read<PelanggaranViewModel>().fetchDataPelanggaran();
    });
  }

  void _onRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    context.read<PelanggaranViewModel>().removeData();
    _refreshController.refreshCompleted();
  }

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  void dispose() {
    super.dispose();
    _refreshController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'History Pelanggaran',
          style: CustomFont.appBar,
        ),
        centerTitle: false,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: false,
        controller: _refreshController,
        onRefresh: _onRefresh,
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Consumer<PelanggaranViewModel>(
            builder: (_, PelanggaranViewModel vm, __) => Column(
              children: [
                Padding(padding: EdgeInsets.only(top: 20.0)),
                Center(
                  child: Text(
                    'Muat ulang untuk menampilkan data terbaru',
                    style: CustomFont.noticeText,
                  ),
                ),
                for (var i = 0; i < vm.pelanggarans.length; i++)
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        width: 480,
                        decoration: BoxDecoration(
                          color: CustomColor.whitebg,
                          border: Border.all(
                              width: 2.0, color: CustomColor.themedarker),
                          borderRadius: BorderRadius.all(
                            Radius.circular(16),
                          ),
                        ),
                        child: Column(
                          children: [
                            Container(
                              padding:
                                  const EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
                              child: ListTile(
                                title: Text('Tanggal Pelanggaran'),
                                subtitle: Text(
                                    '${vm?.pelanggarans[i]?.tanggal_pelanggaran}'),
                              ),
                            ),
                            Container(
                              padding:
                                  const EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
                              child: ListTile(
                                title: Text('Pelanggaran'),
                                subtitle:
                                    Text('${vm?.pelanggarans[i]?.pelanggaran}'),
                              ),
                            ),
                            Container(
                              padding:
                                  const EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
                              child: ListTile(
                                title: Text('Tindak Lanjut'),
                                subtitle: Text(
                                    '${vm?.pelanggarans[i]?.tindak_lanjut}'),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
