import 'dart:convert';

import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/list_kelas.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/pilih_kelas.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/histori_page/histori_bimbingan.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/histori_page/pelanggaran.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';

class HistoriPage extends StatefulWidget {
  @override
  _HistoriPageState createState() => _HistoriPageState();
}

class _HistoriPageState extends State<HistoriPage> {
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'History',
          style: CustomFont.appBar,
        ),
        centerTitle: true,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(16),
            child: Card(
              color: CustomColor.whitebg,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Center(
                    child: Padding(
                      padding: EdgeInsets.all(10),
                      child: Image(
                        image: CustomImage.pelanggaran,
                        width: 280,
                        height: 140,
                      ),
                    ),
                  ),
                  Center(
                    child: MaterialButton(
                      minWidth: 120.0,
                      height: 40.0,
                      elevation: 10,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ListPelanggaran()),
                        );
                      },
                      color: CustomColor.themedarker,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      child: Text(
                        'History Pelanggaran',
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(16),
            child: Card(
              color: CustomColor.whitebg,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Center(
                    child: Padding(
                      padding: EdgeInsets.all(10),
                      child: Image(
                        image: CustomImage.bimbingan,
                        width: 360,
                        height: 180,
                      ),
                    ),
                  ),
                  Center(
                    child: MaterialButton(
                      minWidth: 120.0,
                      height: 40.0,
                      elevation: 10,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ListBimbingan()),
                        );
                      },
                      color: CustomColor.themedarker,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      child: Text(
                        'History Bimbingan',
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
