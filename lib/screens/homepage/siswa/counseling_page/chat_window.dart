import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/chatbot_model.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/counseling_page/chatbot_bubble.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/chatbot_vm.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';

class ChatWindow extends StatefulWidget {
  const ChatWindow({Key key}) : super(key: key);

  @override
  _ChatWindowState createState() => _ChatWindowState();
}

class _ChatWindowState extends State<ChatWindow> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      context
          .read<ChatbotViewModel>()
          .setNetworkViewModel(context.read<NetworkService>());
      context.read<ChatbotViewModel>().getTopics();
    });
    scrollController = ScrollController();
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  ScrollController scrollController;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Bot'),
      ),
      body: Consumer<ChatbotViewModel>(
        builder: (
          BuildContext context,
          ChatbotViewModel vm,
          Widget child,
        ) =>
            Column(
          children: <Widget>[
            Expanded(
              child: ListView.builder(
                controller: scrollController,
                padding: EdgeInsets.symmetric(horizontal: 16),
                itemCount: vm.chats.length,
                itemBuilder: (BuildContext context, int index) {
                  final ChatbotModel chat = vm.chats[index];
                  if (chat.topic != null) {
                    return ChatbotBubble(
                      isBot: chat.isBot,
                      dateTime: chat.timeStamp,
                      message: chat.message,
                      options: vm.topicString,
                      onPressed: (int val) async {
                        await vm.onTopicTapped(val);

                        scrollController.animateTo(
                          scrollController.position.maxScrollExtent + 400,
                          duration: Duration(milliseconds: 250),
                          curve: Curves.linear,
                        );
                      },
                    );
                  } else if (chat.response != null) {
                    return ChatbotBubble(
                      isBot: chat.isBot,
                      dateTime: chat.timeStamp,
                      message: chat.message,
                      options: vm.pertanyaanString,
                      onPressed: (int val) async {
                        await vm.onQuestionTapped(val);

                        scrollController.animateTo(
                          scrollController.position.maxScrollExtent + 100,
                          duration: Duration(milliseconds: 250),
                          curve: Curves.linear,
                        );
                      },
                    );
                  } else if (chat.message != null) {
                    return ChatbotBubble(
                      isBot: chat.isBot,
                      dateTime: chat.timeStamp,
                      message: chat.message,
                    );
                  }
                  return SizedBox();
                },
              ),
            ),
            //
            FractionallySizedBox(
              widthFactor: 1,
              child: ElevatedButton(
                onPressed: vm.topics.isNotEmpty
                    ? () {
                        vm.sendTopic();

                        scrollController.animateTo(
                          scrollController.position.maxScrollExtent + 400,
                          duration: Duration(milliseconds: 250),
                          curve: Curves.linear,
                        );
                      }
                    : null,
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('Tampilkan Topik'),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
