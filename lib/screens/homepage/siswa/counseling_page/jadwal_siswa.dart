import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/kelas_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/jadwal_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/will_pop_vm.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:get/route_manager.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class JadwalPage extends StatefulWidget {
  @override
  _JadwalPageState createState() => _JadwalPageState();
}

class _JadwalPageState extends State<JadwalPage> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context
          .read<JadwalViewModel>()
          .setNetworkViewModel(context.read<NetworkService>());
      context.read<JadwalViewModel>().removeEarlierJadwal();
    });
  }

  void _onRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    context.read<JadwalViewModel>().removeEarlierJadwal();
    _refreshController.refreshCompleted();
  }

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onAddJadwal() {
    Get.toNamed<void>('/addjadwal');
  }

  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Consumer<WillPopViewModel>(
      builder: (_, WillPopViewModel _vm, __) => WillPopScope(
        onWillPop: () => _vm.onBackPressed('/homepagesiswa'),
        child: Scaffold(
          backgroundColor: CustomColor.graybg,
          appBar: AppBar(
            title: const Text(
              'Jadwal Bimbingan',
              style: CustomFont.appBar,
            ),
            centerTitle: true,
            backgroundColor: CustomColor.whitebg,
            automaticallyImplyLeading: false,
          ),
          body: SmartRefresher(
            enablePullDown: true,
            enablePullUp: false,
            controller: _refreshController,
            onRefresh: _onRefresh,
            child: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: Consumer<JadwalViewModel>(
                builder: (_, JadwalViewModel vm, __) => Column(
                  children: [
                    MaterialButton(
                      child: RichText(
                        text: const TextSpan(
                          text: 'Klik ',
                          style: CustomFont.basicText,
                          children: <TextSpan>[
                            TextSpan(
                                text: 'di sini', style: CustomFont.noticeText),
                            TextSpan(
                                text: ' untuk membuat permintaan jadwal.',
                                style: CustomFont.basicText),
                          ],
                        ),
                      ),
                      onPressed: _onAddJadwal,
                    ),
                    if (vm.jadwals.isEmpty)
                      Center(
                        child: Container(
                          margin: EdgeInsets.only(top: 300.0),
                          child: Text(
                            'Muat ulang untuk menampilkan data terbaru.',
                            style: CustomFont.noticeText,
                          ),
                        ),
                      )
                    else if (vm.jadwals.isNotEmpty)
                      for (var i = 0; i < vm.jadwals.length; i++)
                        if (vm.jadwals[i].isConfirmed == null)
                          Column(
                            children: [
                              if ((vm.jadwals[i].tanggal
                                              .difference(vm.today)
                                              .inDays ==
                                          0 &&
                                      vm.jadwals[i].tanggal
                                              .difference(vm.today)
                                              .inHours ==
                                          0) ||
                                  (vm.jadwals[i].tanggal
                                              .difference(vm.today)
                                              .inDays ==
                                          0 &&
                                      vm.jadwals[i].tanggal
                                              .difference(vm.today)
                                              .inHours <
                                          0) ||
                                  (vm.jadwals[i].tanggal
                                          .difference(vm.today)
                                          .inDays <
                                      0))
                                Stack(
                                  children: [
                                    Card(
                                      child: Container(
                                        decoration: BoxDecoration(
                                          color: Colors.grey.withOpacity(0.2),
                                        ),
                                        child: Slidable(
                                          actionPane:
                                              SlidableDrawerActionPane(),
                                          actionExtentRatio: 0.25,
                                          child: Column(
                                            children: [
                                              Container(
                                                width: size.width,
                                                padding: EdgeInsets.only(
                                                    top: 5.0, bottom: 5.0),
                                                child: Text(
                                                  'Expired.',
                                                  style:
                                                      CustomFont.basicTextgrey,
                                                ),
                                              ),
                                              Container(
                                                width: size.width,
                                                padding: EdgeInsets.only(
                                                    top: 5.0, bottom: 5.0),
                                                child: Text(
                                                  vm?.jadwals[i]?.topik ?? '',
                                                  style: CustomFont.basicTitle,
                                                ),
                                              ),
                                              Container(
                                                width: size.width,
                                                padding: EdgeInsets.only(
                                                    top: 5.0, bottom: 5.0),
                                                child: Text(
                                                  vm?.jadwals[i]
                                                          ?.toStringDue() ??
                                                      '',
                                                  style: CustomFont.basicTitle,
                                                ),
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.end,
                                                children: [
                                                  Icon(Icons.timer),
                                                  Text(vm.jadwals[i].tanggal
                                                      .difference(vm.today)
                                                      .inDays
                                                      .toString()),
                                                ],
                                              ),
                                              Container(
                                                width: size.width,
                                                alignment:
                                                    Alignment.bottomRight,
                                                padding: EdgeInsets.only(
                                                    top: 5.0, bottom: 5.0),
                                                child: Text(
                                                  vm?.jadwals[i]
                                                          ?.toStringMade() ??
                                                      '',
                                                  style: CustomFont.smallMuted,
                                                ),
                                              ),
                                            ],
                                          ),
                                          secondaryActions: <Widget>[
                                            IconSlideAction(
                                              color: CustomColor.theme,
                                              icon: Icons.edit,
                                              foregroundColor:
                                                  CustomColor.whitebg,
                                              onTap: () => vm
                                                  .onEditJadwal(vm.jadwals[i]),
                                            ),
                                            IconSlideAction(
                                              color: Colors.red,
                                              icon: Icons.delete,
                                              foregroundColor:
                                                  CustomColor.whitebg,
                                              onTap: () => vm.deleteJadwal(
                                                  vm.jadwals[i].id),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              else
                                Column(
                                  children: [
                                    Card(
                                      child: Container(
                                        decoration: BoxDecoration(
                                          color: Colors.lightBlueAccent
                                              .withOpacity(0.2),
                                        ),
                                        child: Slidable(
                                          actionPane:
                                              SlidableDrawerActionPane(),
                                          actionExtentRatio: 0.25,
                                          child: Column(
                                            children: [
                                              Container(
                                                width: size.width,
                                                padding: EdgeInsets.only(
                                                    top: 5.0, bottom: 5.0),
                                                child: Text(
                                                  'Menunggu konfirmasi.',
                                                  style: CustomFont.waiting,
                                                ),
                                              ),
                                              Container(
                                                width: size.width,
                                                padding: EdgeInsets.only(
                                                    top: 5.0, bottom: 5.0),
                                                child: Text(
                                                  vm?.jadwals[i]?.topik ?? '',
                                                  style: CustomFont.basicTitle,
                                                ),
                                              ),
                                              Container(
                                                width: size.width,
                                                padding: EdgeInsets.only(
                                                    top: 5.0, bottom: 5.0),
                                                child: Text(
                                                  vm?.jadwals[i]
                                                          ?.toStringDue() ??
                                                      '',
                                                  style: CustomFont.basicTitle,
                                                ),
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.end,
                                                children: [
                                                  Icon(Icons.timer),
                                                  Text(
                                                      '${vm.jadwals[i].tanggal.difference(vm.today).inDays}'),
                                                ],
                                              ),
                                              Container(
                                                width: size.width,
                                                alignment:
                                                    Alignment.bottomRight,
                                                padding: EdgeInsets.only(
                                                    top: 5.0, bottom: 5.0),
                                                child: Text(
                                                  vm?.jadwals[i]
                                                          ?.toStringMade() ??
                                                      '',
                                                  style: CustomFont.smallMuted,
                                                ),
                                              ),
                                            ],
                                          ),
                                          // secondaryActions: <Widget>[
                                          //   IconSlideAction(
                                          //     color: CustomColor.theme,
                                          //     icon: Icons.edit,
                                          //     foregroundColor: CustomColor.whitebg,
                                          //     onTap: () =>
                                          //         vm.onEditJadwal(vm.jadwals[i]),
                                          //   ),
                                          //   IconSlideAction(
                                          //     color: Colors.red,
                                          //     icon: Icons.delete,
                                          //     foregroundColor: CustomColor.whitebg,
                                          //     onTap: () =>
                                          //         vm.deleteJadwal(vm.jadwals[i].id),
                                          //   ),
                                          // ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                            ],
                          )
                        else if (vm.jadwals[i].isConfirmed == 1)
                          Column(
                            children: [
                              Card(
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: CustomColor.themelighter
                                        .withOpacity(0.2),
                                  ),
                                  child: Slidable(
                                    actionPane: SlidableDrawerActionPane(),
                                    actionExtentRatio: 0.25,
                                    child: Column(
                                      children: [
                                        Container(
                                          width: size.width,
                                          padding: EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          child: Text(
                                            'Diterima.',
                                            style: CustomFont.accept,
                                          ),
                                        ),
                                        Container(
                                          width: size.width,
                                          padding: EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          child: Text(
                                            vm?.jadwals[i]?.topik ?? '',
                                            style: CustomFont.basicTitle,
                                          ),
                                        ),
                                        Align(
                                            alignment: Alignment.bottomRight,
                                            child: Icon(Icons.check)),
                                        Container(
                                          width: size.width,
                                          alignment: Alignment.bottomRight,
                                          padding: EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          child: Text(
                                            vm?.jadwals[i]?.toStringDue() ?? '',
                                            style: CustomFont.smallMuted,
                                          ),
                                        ),
                                      ],
                                    ),
                                    secondaryActions: <Widget>[
                                      IconSlideAction(
                                        color: CustomColor.theme,
                                        icon: Icons.chat,
                                        foregroundColor: CustomColor.whitebg,
                                        onTap: () =>
                                            Get.toNamed<void>('/bimbingan'),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          )
                        else if (vm.jadwals[i].isConfirmed == 0)
                          Column(
                            children: [
                              Card(
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: CustomColor.orangeaccent
                                        .withOpacity(0.2),
                                  ),
                                  child: Slidable(
                                    actionPane: SlidableDrawerActionPane(),
                                    actionExtentRatio: 0.25,
                                    child: Column(
                                      children: [
                                        Container(
                                          width: size.width,
                                          padding: EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          child: Text(
                                            'Ditolak.',
                                            style: CustomFont.reject,
                                          ),
                                        ),
                                        Container(
                                          width: size.width,
                                          padding: EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          child: Text(
                                            vm?.jadwals[i]?.topik ?? '',
                                            style: CustomFont.basicTitle,
                                          ),
                                        ),
                                        Align(
                                            alignment: Alignment.bottomRight,
                                            child: Icon(Icons.cancel)),
                                        Container(
                                          width: size.width,
                                          alignment: Alignment.bottomRight,
                                          padding: EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          child: Text(
                                            vm?.jadwals[i]?.toStringMade() ??
                                                '',
                                            style: CustomFont.smallMuted,
                                          ),
                                        ),
                                      ],
                                    ),
                                    secondaryActions: <Widget>[
                                      IconSlideAction(
                                        color: CustomColor.theme,
                                        icon: Icons.edit,
                                        foregroundColor: CustomColor.whitebg,
                                        onTap: () =>
                                            vm.onEditJadwal(vm.jadwals[i]),
                                      ),
                                      IconSlideAction(
                                        color: Colors.red,
                                        icon: Icons.delete,
                                        foregroundColor: CustomColor.whitebg,
                                        onTap: () =>
                                            vm.deleteJadwal(vm.jadwals[i].id),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
