import 'dart:convert';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/chatting_screen.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/view_models/chat_vm.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/widget.dart/conversation_card.dart';
import 'package:bk_mobile_app/styles/size_config.dart';
import 'package:bubble_bottom_bar/bubble_bottom_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:flutter/scheduler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:get/route_manager.dart';

class ChatSiswa extends StatefulWidget {
  @override
  _ChatSiswaState createState() => _ChatSiswaState();
}

class _ChatSiswaState extends State<ChatSiswa> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context
          .read<ChatViewModel>()
          .setNetworkService(context.read<NetworkService>());
      context.read<ChatViewModel>().getChats();
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Bimbingan',
          style: CustomFont.appBar,
        ),
        centerTitle: true,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: Consumer<ChatViewModel>(
        builder: (_, ChatViewModel vm, __) => Center(
          child: vm.busy
              ? CircularProgressIndicator()
              : ListView.builder(
                  padding:
                      EdgeInsets.only(top: SizeConfig.blockSizeVertical * 2),
                  itemCount: vm.chats.length,
                  itemBuilder: (context, index) => ConversationCard(
                    chatting: vm.chats[index],
                    // onTap: () {
                    //   Navigator.of(context).push(MaterialPageRoute(
                    //       builder: (_) => ChattingScreen(
                    //             conversation: vm.chats[index],
                    //           )));
                    // }),
                  ),
                ),
        ),
      ),
    );
  }
}
