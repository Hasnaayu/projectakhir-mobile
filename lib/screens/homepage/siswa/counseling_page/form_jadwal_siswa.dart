import 'package:bk_mobile_app/auth/mixins/dialog_mixin.dart';
import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/jadwal_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/will_pop_vm.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/route_manager.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class JadwalFormPage extends StatefulWidget {
  @override
  _JadwalFormPageState createState() => _JadwalFormPageState();
}

class _JadwalFormPageState extends State<JadwalFormPage> with DialogMixin {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context
          .read<JadwalViewModel>()
          .setNetworkViewModel(context.read<NetworkService>());
      //context.read<JadwalViewModel>().removeEarlierData();
    });
  }

  @override
  void dispose() {
    super.dispose();
    _refreshController.dispose();
  }

  void _onRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    context.read<JadwalViewModel>().removeEarlierData();
    _refreshController.refreshCompleted();
  }

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Consumer<WillPopViewModel>(
      builder: (_, WillPopViewModel _vm, __) => WillPopScope(
        onWillPop: () => _vm.onGoBack(),
        child: Scaffold(
          backgroundColor: CustomColor.graybg,
          appBar: AppBar(
            title: const Text(
              'Form Jadwal',
              style: CustomFont.appBar,
            ),
            centerTitle: true,
            backgroundColor: CustomColor.whitebg,
            automaticallyImplyLeading: false,
          ),
          body: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Consumer<JadwalViewModel>(
              builder: (_, JadwalViewModel vm, __) => ConstrainedBox(
                constraints: BoxConstraints(maxHeight: size.height / 1.2),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    const Padding(
                      padding: EdgeInsets.only(top: 30.0),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            ListTile(
                              tileColor: CustomColor.whitebg,
                              leading: Icon(
                                Icons.calendar_today,
                                size: 35,
                              ),
                              onTap: () => vm.selectDate(context),
                              title: Row(
                                children: [
                                  if (vm.tanggal == null &&
                                      vm?.selectedJadwal?.tanggal == null)
                                    Text(
                                      'Tentukan tanggal.',
                                      style: CustomFont.noticeText,
                                    )
                                  else if (vm.tanggal != null ||
                                      vm?.selectedJadwal?.tanggal != null)
                                    Text(
                                      vm.toStringDue(),
                                      style: CustomFont.noticeText,
                                    ),
                                ],
                              ),
                            ),
                            Divider(
                              color: CustomColor.mutedButton.withOpacity(0.5),
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 20.0),
                                  child: Text(
                                    'Topik',
                                    style: CustomFont.basicText,
                                  ),
                                ),
                                Container(
                                  child: TextFormField(
                                    keyboardType: TextInputType.text,
                                    autofocus: false,
                                    maxLines: 1,
                                    onChanged: vm.onTopikChange,
                                    decoration: const InputDecoration(
                                      hintText: 'Tentukan topik.',
                                      focusedBorder: InputBorder.none,
                                      border: InputBorder.none,
                                      hintStyle: CustomFont.noticeText,
                                      contentPadding: EdgeInsets.fromLTRB(
                                          20.0, 10.0, 20.0, 10.0),
                                    ),
                                  ),
                                ),
                                const Padding(
                                  padding: EdgeInsets.only(top: 70.0),
                                ),
                                Center(
                                  child: MaterialButton(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0)),
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 100.0),
                                        child: Text(
                                          'Kirim'.toUpperCase(),
                                          style: CustomFont.signIn,
                                        ),
                                      ),
                                      padding: const EdgeInsets.only(
                                          top: 10.0, bottom: 10.0),
                                      color: CustomColor.themedarker,
                                      onPressed: vm.addNewJadwal),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
