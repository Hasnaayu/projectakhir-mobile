import 'dart:convert';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/bimbingan_page.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/aum_first_page.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/counseling_page/chatbot.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/counseling_page/jadwal_siswa.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/counseling_page/livechat.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/main_page/main_page.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/chatbot_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:bk_mobile_app/screens/welcome/login.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:get/route_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sticky_headers/sticky_headers.dart';
import 'package:bk_mobile_app/screens/homepage/home_page.dart';
import 'package:provider/provider.dart';

class BimbinganPage extends StatefulWidget {
  @override
  _BimbinganPageState createState() => _BimbinganPageState();
}

class _BimbinganPageState extends State<BimbinganPage> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context.read<ChatbotViewModel>().setNetworkViewModel(
            context.read<NetworkService>(),
          );
      // for (var i = 0; i < context.read<ChatbotViewModel>().bots.length; i++) {
      //   int id = context.read<ChatbotViewModel>().bots[i].id;
      //context.read<ChatbotViewModel>().removeEarlierData();
    });
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Bimbingan',
          style: CustomFont.appBar,
        ),
        centerTitle: true,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: Consumer<ChatbotViewModel>(
        builder: (_, ChatbotViewModel vm, __) => Column(
          children: [
            Padding(
              padding: EdgeInsets.all(16),
              child: Card(
                color: CustomColor.whitebg,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Center(
                      child: Padding(
                        padding: EdgeInsets.all(10),
                        child: Image(
                          image: CustomImage.chatbot,
                          width: 280,
                          height: 140,
                        ),
                      ),
                    ),
                    Center(
                      child: MaterialButton(
                        minWidth: 120.0,
                        height: 40.0,
                        elevation: 10,
                        onPressed: () => Get.toNamed('/bot'),
                        color: CustomColor.themedarker,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        child: Text(
                          'Chatbot',
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(16),
              child: Card(
                color: CustomColor.whitebg,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Center(
                      child: Padding(
                        padding: EdgeInsets.all(10),
                        child: Image(
                          image: CustomImage.chat,
                          width: 300,
                          height: 140,
                        ),
                      ),
                    ),
                    Center(
                      child: MaterialButton(
                        minWidth: 120.0,
                        height: 40.0,
                        elevation: 10,
                        onPressed: () => Get.toNamed('/jadwalsiswa'),
                        color: CustomColor.themedarker,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        child: Text(
                          'Chat Guru BK',
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
