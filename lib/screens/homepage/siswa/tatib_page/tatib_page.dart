import 'dart:convert';

import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/aum_first_page.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/main_page/main_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:bk_mobile_app/screens/welcome/login.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sticky_headers/sticky_headers.dart';
import 'package:bk_mobile_app/screens/homepage/home_page.dart';

class TatibPage extends StatefulWidget {
  @override
  _TatibPageState createState() => _TatibPageState();
}

class _TatibPageState extends State<TatibPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Tata Tertib',
          style: CustomFont.appBar,
        ),
        centerTitle: false,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: false,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
      ),
    );
  }
}
