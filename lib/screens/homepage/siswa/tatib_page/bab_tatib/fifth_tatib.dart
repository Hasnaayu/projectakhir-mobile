import 'dart:convert';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/tatib_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:flutter/scheduler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class TatibLima extends StatefulWidget {
  @override
  _TatibLimaState createState() => _TatibLimaState();
}

class _TatibLimaState extends State<TatibLima> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context.read<TatibViewModel>().setNetworkService(
          context.read<NetworkService>()); //for fetchPelanggaran()
      context.read<TatibViewModel>().fetchTatibLima();
    });
  }

  void _onRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    context.read<TatibViewModel>().removeTatibLima();
    _refreshController.refreshCompleted();
  }

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  void dispose() {
    super.dispose();
    _refreshController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'V. Penghargaan',
          style: CustomFont.appBar,
        ),
        centerTitle: true,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: false,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: false,
        controller: _refreshController,
        onRefresh: _onRefresh,
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Consumer<TatibViewModel>(
            builder: (_, TatibViewModel vm, __) => Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(padding: EdgeInsets.only(top: 20.0)),
                Text(
                  'Penghargaan (reward) akan diberikan apabila:',
                  style: CustomFont.option,
                ),
                for (var i = 0; i < vm.tatibs.length; i++)
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(
                      width: 480,
                      decoration: BoxDecoration(
                        color: Colors.grey[300],
                        border: Border.all(
                            width: 2.0, color: CustomColor.themedarker),
                        borderRadius: BorderRadius.all(
                          Radius.circular(4),
                        ),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              vm?.tatibs[i]?.isi ?? 'null',
                              style: CustomFont.option,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
