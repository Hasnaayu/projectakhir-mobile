import 'package:bk_mobile_app/screens/homepage/siswa/main_page/main_page.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/tatib_page/bab_tatib/first_tatib.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';

class TatibIntroPage extends StatefulWidget {
  @override
  _TatibIntroPageState createState() => _TatibIntroPageState();
}

class _TatibIntroPageState extends State<TatibIntroPage> {
  void _firstTatib() {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => TatibSatu()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Tata Tertib',
          style: CustomFont.appBar,
        ),
        centerTitle: true,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: false,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(
              context,
              MaterialPageRoute(builder: (context) => MainPage()),
            );
          },
        ),
      ),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(16),
            child: Card(
              color: CustomColor.whitebg,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Center(
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
                      child: Image(
                        image: CustomImage.menu4,
                        width: 280,
                        height: 140,
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(bottom: 25.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        //child: Center(
                        Text(
                          'TATA TERTIB SEKOLAH',
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w500),
                        ),
                        Text(
                          'SMK PSM 1 KAWEDANAN',
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(18.0, 8.0, 18.0, 8.0),
            child: Container(
              color: Colors.deepOrange[900],
              padding: EdgeInsets.all(5.0),
              child: ListTile(
                tileColor: CustomColor.deeporange,
                leading:
                    Image(image: CustomImage.tatib1, height: 35, width: 35),
                title: Text(
                  'I.   Hal Masuk Sekolah',
                  style: CustomFont.smallMuted,
                ),
                trailing: Icon(
                  Icons.keyboard_arrow_right,
                  size: 35,
                ),
                onTap: () => Get.toNamed<void>('/tatibsatu'),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(18.0, 8.0, 18.0, 8.0),
            child: Container(
              color: Colors.lime[900],
              padding: EdgeInsets.all(5.0),
              child: ListTile(
                tileColor: CustomColor.lime,
                leading:
                    Image(image: CustomImage.tatib2, height: 35, width: 35),
                title: Text(
                  'II.   Kewajiban Peserta Didik',
                  style: CustomFont.smallMuted,
                ),
                trailing: Icon(
                  Icons.keyboard_arrow_right,
                  size: 35,
                ),
                onTap: () => Get.toNamed<void>('/tatibdua'),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(18.0, 8.0, 18.0, 8.0),
            child: Container(
              color: Colors.orange[900],
              padding: EdgeInsets.all(5.0),
              child: ListTile(
                tileColor: CustomColor.orangeaccent,
                leading:
                    Image(image: CustomImage.tatib3, height: 35, width: 35),
                title: Text(
                  'III.   Larangan Peserta Didik',
                  style: CustomFont.smallMuted,
                ),
                trailing: Icon(
                  Icons.keyboard_arrow_right,
                  size: 35,
                ),
                onTap: () => Get.toNamed<void>('/tatibtiga'),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(18.0, 8.0, 18.0, 8.0),
            child: Container(
              color: Colors.indigo[400],
              padding: EdgeInsets.all(5.0),
              child: ListTile(
                tileColor: CustomColor.indigo,
                leading:
                    Image(image: CustomImage.tatib4, height: 35, width: 35),
                title: Text(
                  'IV.   Sanksi',
                  style: CustomFont.smallMuted,
                ),
                trailing: Icon(
                  Icons.keyboard_arrow_right,
                  size: 35,
                ),
                onTap: () => Get.toNamed<void>('/tatibempat'),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(18.0, 8.0, 18.0, 8.0),
            child: Container(
              color: Colors.yellow[700],
              padding: EdgeInsets.all(5.0),
              child: ListTile(
                tileColor: CustomColor.yellow,
                leading:
                    Image(image: CustomImage.tatib5, height: 35, width: 35),
                title: Text(
                  'V.   Penghargaan',
                  style: CustomFont.smallMuted,
                ),
                trailing: Icon(
                  Icons.keyboard_arrow_right,
                  size: 35,
                ),
                onTap: () => Get.toNamed<void>('/tatiblima'),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(18.0, 8.0, 18.0, 8.0),
            child: Container(
              color: Colors.teal[600],
              padding: EdgeInsets.all(5.0),
              child: ListTile(
                tileColor: CustomColor.tealaccent,
                leading:
                    Image(image: CustomImage.tatib6, height: 35, width: 35),
                title: Text(
                  'VI.   Lain-lain',
                  style: CustomFont.smallMuted,
                ),
                trailing: Icon(
                  Icons.keyboard_arrow_right,
                  size: 35,
                ),
                onTap: () => Get.toNamed<void>('/tatibenam'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
