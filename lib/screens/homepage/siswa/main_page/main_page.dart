import 'dart:convert';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/aum_first_page.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/counseling_page/bimbingan_page.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/histori_page/histori_page.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/profile_page/profile_page.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/tatib_page/tatib_intro_page.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/tatib_page/tatib_page.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/siswa_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/user_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sticky_headers/sticky_headers.dart';
import 'package:provider/provider.dart';
import 'package:flutter/scheduler.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  String name;

  // @override
  // void initState() {
  //   _loadUserData();
  //   super.initState();
  // }

  // _loadUserData() async {
  //   SharedPreferences localStorage = await SharedPreferences.getInstance();
  //   var user = jsonDecode(localStorage.getString('user'));

  //   if (user != null) {
  //     setState(() {
  //       name = user['name'];
  //     });
  //   }
  // }

  void _profilePage() {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => ProfilePage()),
    );
  }

  void _aumFirstPage() {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => AumFirstPage()),
    );
  }

  void _bimbinganPage() {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => BimbinganPage()),
    );
  }

  void _historiPage() {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => HistoriPage()),
    );
  }

  void _tatibIntroPage() {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => TatibIntroPage()),
    );
  }

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context
          .read<SiswaViewModel>()
          .setNetworkService(context.read<NetworkService>()); //for fetchSiswa()
      context.read<SiswaViewModel>().fetchSiswa();
      context
          .read<SiswaViewModel>()
          .setNetworkService(context.read<NetworkService>()); //for addFoto()
      context.read<SiswaViewModel>().fetchData();
    });
  }

  @override
  Widget build(BuildContext context) {
    var mediaQueryData = MediaQuery.of(context);
    //List<String> entries = <String>['Student', 'Class', ''];
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Home',
          style: CustomFont.appBar,
        ),
        centerTitle: true,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: false,
      ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            SizedBox(
              width: double.infinity,
              child: Consumer<UserViewModel>(
                builder: (_, UserViewModel vm, __) => InkWell(
                  child: Card(
                    child: Consumer<SiswaViewModel>(
                      builder: (_, SiswaViewModel vm1, __) => Padding(
                        padding:
                            const EdgeInsets.fromLTRB(8.0, 26.0, 26.0, 26.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.fromLTRB(
                                  0, 10.0, 10.0, 10.0),
                            ),
                            CircleAvatar(
                              radius: 50,
                              backgroundColor: Colors.grey,
                              child: ClipOval(
                                child: SizedBox(
                                  width: 100,
                                  height: 100,
                                  child: Image(
                                    image: CustomImage.avatar,
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(
                                  20.0, 10.0, 10.0, 10.0),
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  vm?.user?.name ?? '-',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18.0),
                                ),
                                Text(
                                  'Siswa',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18.0,
                                      color: Colors.grey),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  onTap: _profilePage,
                  //print("Profile");
                ),
              ),
            ),
            Expanded(
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: InkWell(
                      child: Card(
                        color: CustomColor.whitebg,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 20.0),
                                child: Image(
                                  image: CustomImage.menu1,
                                  height: 170,
                                ),
                              ),
                            ),
                            Center(
                              child: Container(
                                //padding: const EdgeInsets.only(top: 2.0),
                                child: Text(
                                  'Alat Ungkap Masalah',
                                  style: TextStyle(fontWeight: FontWeight.w500),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: _aumFirstPage,
                      //print("AUM");
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      child: Card(
                        color: CustomColor.whitebg,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 10.0),
                                child: Image(
                                  image: CustomImage.menu2,
                                  width: 180,
                                  height: 180,
                                ),
                              ),
                            ),
                            Center(
                              child: Container(
                                //padding: const EdgeInsets.only(top: 5.0),
                                child: Text(
                                  'Bimbingan',
                                  style: TextStyle(fontWeight: FontWeight.w500),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: _bimbinganPage,
                      //print("Bimbingan");
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Row(
                children: <Widget>[
                  // bisa di rapiin dijadiin widget terpisah
                  Expanded(
                    child: InkWell(
                      child: Card(
                        color: CustomColor.whitebg,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 20.0),
                                child: Image(
                                  image: CustomImage.menu3,
                                  width: 160,
                                  height: 160,
                                ),
                              ),
                            ),
                            Center(
                              child: Container(
                                padding: const EdgeInsets.only(top: 10.0),
                                child: Text(
                                  'Histori',
                                  style: TextStyle(fontWeight: FontWeight.w500),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: _historiPage,
                      //print("Informasi");
                    ),
                  ),
                  // bisa di rapiin dijadiin widget terpisah
                  Expanded(
                    child: InkWell(
                      child: Card(
                        color: CustomColor.whitebg,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 10.0),
                                child: Image(
                                  image: CustomImage.menu4,
                                  width: 180.0,
                                  height: 180.0,
                                ),
                              ),
                            ),
                            Center(
                              child: Container(
                                //padding: const EdgeInsets.only(top: 5.0),
                                child: Text(
                                  'Tata Tertib',
                                  style: TextStyle(fontWeight: FontWeight.w500),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: _tatibIntroPage,
                      //print("Tata Tertib");
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
            ),
          ],
        ),
      ),
    );
  }

  Future<bool> _onBackPressed() {
    return SystemNavigator.pop();
  }
}
