import 'package:bk_mobile_app/auth/mixins/dialog_mixin.dart';
import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/jadwal_model.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/user_model.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/user_vm.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'dart:io';

class JadwalViewModel extends ChangeNotifier with DialogMixin {
  JadwalViewModel();

  NetworkService _net;

  UserViewModel _user;
  UserModel usr;

  List<JadwalModel> _jadwals = [];
  List<JadwalModel> get jadwals => _jadwals;

  List<int> spanHours = [];
  List<int> spanDays = [];
  List<String> jadwalTopik = [];
  List<int> jadwalSpanHours = [];
  List<int> jadwalSpanDays = [];

  JadwalModel _selectedJadwal;
  JadwalModel get selectedJadwal => _selectedJadwal;

  UserModel _users;
  UserModel get users => _users;

  DateTime tanggal;
  bool isConfirmed;
  DateTime today = DateTime.now();
  String topik;
  String nama_siswa;

  String toStringDue() {
    return DateFormat('EEEEEE, d - M - y').format(today);
  }

  void setUserViewModel(UserViewModel user) {
    _user = user;
    notifyListeners();
  }

  void onTopikChange(String val) {
    topik = val;
    print(topik);
    notifyListeners();
  }

  void onNamaChange(String val) {
    nama_siswa = val;
    notifyListeners();
  }

  void setNetworkViewModel(NetworkService net) {
    _net = net;
    notifyListeners();
  }

  void onEditJadwal(
    dynamic val,
  ) async {
    _selectedJadwal = val;
    Get.toNamed<void>('/editjadwal');
    notifyListeners();
  }

  void onJadwalTapped(UserModel userr) async {
    usr = userr;
    print('findMe: ${userr.id} - ${userr.name}');
    print(usr.id);
    notifyListeners();
    Get.toNamed<void>('/addjadwal');
  }

  Future<void> selectDate(BuildContext context) async {
    tanggal = await showDatePicker(
      context: context,
      initialDate: today,
      firstDate: DateTime.now(),
      lastDate: DateTime(3000),
    );
    if (tanggal != null && tanggal != today) today = tanggal;
    print(tanggal);
    notifyListeners();
  }

  Future<void> selectEditedDate(BuildContext context) async {
    tanggal = null;
    tanggal = await showDatePicker(
      context: context,
      initialDate: today,
      firstDate: DateTime.now(),
      lastDate: DateTime(3000),
    );
    if (tanggal != null && tanggal != today) today = tanggal;
    print(tanggal);
    notifyListeners();
  }

  Future<void> addNewJadwal() async {
    var formData = FormData.fromMap({
      'tanggal': tanggal ?? null,
      'topik': topik,
      'nama_siswa': nama_siswa,
      'isConfirmed': isConfirmed,
    });
    var response = await _net.request('/storejadwalsiswa',
        requestMethod: 'post', data: formData);
    print(response);
    if (response.data['success'] == true) {
      tanggal = null;
      topik = null;
      nama_siswa = null;
      isConfirmed = null;
      Get.back<void>();
      showSuccessSnackbar('Sukses mengirim permintaan jadwal.');
    } else
      showErrorSnackbar('Isi form permintaan jadwal.');

    print(response);

    notifyListeners();
  }

  Future<void> removeEarlierData() async {
    _jadwals.clear();
    print(_jadwals);
    fetchDataList();
    notifyListeners();
  }

  Future<void> fetchDataList() async {
    if (_jadwals.isEmpty) {
      addNewJadwal();
    } else {
      print('datas have been fetched.');
    }
    notifyListeners();
  }

  Future<void> fetchAllJadwal() async {
    final Response<dynamic> resp = await _net.request('/jadwal/index');

    List<dynamic> listData = resp.data['data'];

    for (dynamic data in listData) {
      _jadwals.add(JadwalModel.fromJson(data as Map<String, dynamic>));
    }

    if (jadwals.isNotEmpty) {
      if (jadwalTopik.isEmpty ||
          jadwalSpanDays.isEmpty ||
          jadwalSpanHours.isEmpty) {
        for (var i = 0; i < _jadwals.length; i++) {
          if (jadwals[i].isConfirmed == null)
            jadwalTopik.add(_jadwals[i].topik);
          spanDays = [_jadwals[i].tanggal.difference(today).inDays];
          spanHours = [_jadwals[i].tanggal.difference(today).inHours];
          for (var i = 0; i < spanDays.length; i++) {
            jadwalSpanDays.add(spanDays[i]);
            jadwalSpanHours.add(spanHours[i]);
          }
        }
        showTargetReminderPopUp(jadwalTopik, jadwalSpanDays, jadwalSpanHours);
      }
    }
    print(resp);
    notifyListeners();
  }

  Future<void> removeEarlierJadwal() async {
    _jadwals.clear();
    print(_jadwals);
    fetchListjadwal();
    notifyListeners();
  }

  Future<void> fetchListjadwal() async {
    if (_jadwals.isEmpty) {
      fetchAllJadwal();
    } else {
      print('datas have been fetched.');
    }
    notifyListeners();
  }

  Future<void> addToSucceeds(int id) async {
    var form = FormData.fromMap({
      'isConfirmed': 1,
    });
    var response = await _net.request('/add_to_confirm/$id',
        requestMethod: 'post', data: form, queryParameter: {'_method': 'put'});
    print(response.data);

    // Get.back<void>();
    // removeEarlierData();
    notifyListeners();
  }

  Future<void> addToFails(int id) async {
    var form = FormData.fromMap({
      'isConfirmed': 0,
    });
    var response = await _net.request('/add_to_fail/$id',
        requestMethod: 'post', data: form, queryParameter: {'_method': 'put'});
    print(response.data);
    // Get.back<void>();
    //removeEarlierData();
    notifyListeners();
  }

  Future<void> editJadwal(int id) async {
    var form = FormData.fromMap({
      'tanggal': tanggal ?? selectedJadwal.tanggal,
      'topik': selectedJadwal?.topik,
      'isConfirmed': isConfirmed,
    });
    var response = await _net.request('/updatejadwal/$id',
        requestMethod: 'post', data: form, queryParameter: {'_method': 'put'});
    print(response.data);
    if (selectedJadwal?.topik == null) {
      showErrorSnackbar('Isi topik untuk edit.');
    } else if (topik == selectedJadwal.topik &&
        tanggal == selectedJadwal.tanggal &&
        isConfirmed == null) {
      topik = null;
      tanggal = null;
      isConfirmed = null;

      Get.back<void>();
      showErrorSnackbar('Tidak terdapat perubahan jadwal.');
    } else if (topik != selectedJadwal.topik ||
        tanggal != selectedJadwal.tanggal) {
      topik = null;
      tanggal = null;
      isConfirmed = null;
      Get.back<void>();
      showSuccessSnackbar(
          'Berhasil membuat perubahan jadwal. Muat ulang untuk memperbarui data.');
    }

    //removeEarlierData();
    notifyListeners();
  }

  Future<void> removeEarlierEdit(int id) async {
    _jadwals.clear();
    print(_jadwals);
    fetchListEdit(id);
    notifyListeners();
  }

  Future<void> fetchListEdit(int id) async {
    if (_jadwals.isEmpty) {
      editJadwal(id);
    } else {
      print('datas have been fetched.');
    }
    notifyListeners();
  }

  Future<void> deleteJadwal(int id) async {
    final Response<dynamic> resp =
        await _net.request('/deletejadwal/$id', requestMethod: 'delete');

    _jadwals.remove(id);

    showSuccessSnackbar(
        'Jadwal berhasil dihapus. Muat ulang untuk memperbarui data.');

    print(resp);
    //removeEarlierData();
    notifyListeners();
  }

  Future<void> fetchJadwalGuru() async {
    final Response<dynamic> resp = await _net.request('/jadwalguru');
    List<dynamic> listData = resp.data['data'];
    for (dynamic data in listData)
      _jadwals.add(JadwalModel.fromJson(data as Map<String, dynamic>));
    print(resp);
    notifyListeners();
  }

  Future<void> removeEarlierJadwalGuru() async {
    _jadwals.clear();
    print(_jadwals);
    fetchListJadwalGuru();
    notifyListeners();
  }

  Future<void> fetchListJadwalGuru() async {
    if (_jadwals.isEmpty) {
      fetchJadwalGuru();
    } else {
      print('datas have been fetched.');
    }
    notifyListeners();
  }
}
