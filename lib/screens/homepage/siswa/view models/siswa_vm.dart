import 'dart:io';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/models/kelas_model.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/models/pelanggaran_model.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/kelas_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/siswa_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:get/route_manager.dart';
import 'package:image_picker/image_picker.dart';

class SiswaViewModel extends ChangeNotifier {
  SiswaViewModel();
  NetworkService _net;
  SiswaModel siswa;
  KelasViewModel _kvm;
  PelanggaranModel pel;

  List<SiswaModel> _fotos = [];
  List<SiswaModel> get fotos => _fotos;
  // List<SiswaModel> _siswas = [];
  // List<SiswaModel> get siswas => _siswas;

  List<PelanggaranModel> _pelanggarans = [];
  List<PelanggaranModel> get pelanggarans => _pelanggarans;

  final List<PelanggaranModel> pelanggaranId = <PelanggaranModel>[];

  File foto;
  final picker = ImagePicker();
  int id;
  int id_siswa;

  void setNetworkService(NetworkService net) {
    _net = net;
  }

//#1
  void onAddPelanggaranChange(
    SiswaModel val,
  ) async {
    siswa = val;
    print(siswa);
    Get.toNamed('/addpelanggaran');
    notifyListeners();
  }

//#2
  void onAddBimbinganChange(
    SiswaModel val,
  ) async {
    siswa = val;
    print(siswa);
    Get.toNamed('/addbimbingan');
    notifyListeners();
  }

  //#3
  void onAddPanggilanChange(
    SiswaModel val,
  ) async {
    siswa = val;
    print(siswa);
    Get.toNamed('/addpanggilan');
    notifyListeners();
  }

  void onPelanggaranTapped(PelanggaranModel val) async {
    pel = val;
    //print('findMe: ${siswa.id} - ${siswa.name}');
    //print(selectedSiswaModel.id);
    notifyListeners();
    Get.toNamed('/createpelanggaran');
  }

  // void onButtonTapped(SiswaModel siswa) async {
  //   selectedSiswaModel = siswa;
  //   print('findMe: ${siswa.id} - ${siswa.name}');
  //   print(selectedSiswaModel.id);
  //   notifyListeners();
  //   Get.toNamed('/addpelanggaran');
  // }

  Future<void> getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      foto = File(pickedFile.path);
    } else {
      print('No photo selected.');
    }
    print('Image Path $foto');
    notifyListeners();
  }

  Future<void> addFoto() async {
    var formData = FormData.fromMap({
      if (foto != null) 'foto': await MultipartFile.fromFile(foto.path),
    });

    var response = await _net.request('/siswa/storesiswa',
        requestMethod: 'post', data: formData);

    print(response);

    Get.back<void>();
    notifyListeners();
  }

  Future<void> fetchSiswa() async {
    final Response<dynamic> resp = await _net.request('/siswa');
    final Map<String, dynamic> respData =
        resp.data['data'] as Map<String, dynamic>;
    siswa = SiswaModel.fromJson(respData);
    print(respData);
    notifyListeners();
  }

  Future<void> fetchPelanggaran(int id) async {
    final Response<dynamic> resp = await _net.request('/pelanggaran/$id');
    _pelanggarans.clear();
    List<dynamic> listData = resp.data['data'];
    for (dynamic data in listData)
      _pelanggarans
          .add(PelanggaranModel.fromJson(data as Map<String, dynamic>));
    print(resp);
    Get.toNamed<void>('/addpelanggaran');
    notifyListeners();
  }

//#3
  // Future<void> createPelanggaran(int id) async {
  //   final Response<dynamic> resp = await _net.request('/kelas/$id');
  //   List<dynamic> listData = resp.data['data'];
  //   namaSiswaDikelas.clear();
  //   for (dynamic data in listData)
  //     namaSiswaDikelas.add(SiswaModel.fromJson(data));
  //   // print(resp);
  //   print(id);
  //   Get.toNamed<void>('/createpelanggaran');
  //   notifyListeners();
  // }

// //#2
//   Future<void> fetchNamaSiswa() async {
//     final Response<dynamic> resp = await _net.request('/siswa/indexnama');
//     List<dynamic> listData = resp.data['data'];
//     namaSiswaDikelas.clear();
//     for (dynamic data in listData)
//       namaSiswaDikelas.add(SiswaModel.fromJson(data as Map<String, dynamic>));
//     print(resp);
//   }

  // Future<void> removeEarlierNama() async {
  //   namaSiswaDikelas.clear();
  //   // print();
  //   fetchDataNama();
  //   notifyListeners();
  // }

  // Future<void> fetchDataNama() async {
  //   if (namaSiswaDikelas.isEmpty) {
  //     fetchNamaSiswa();
  //   } else {
  //     print('datas have been fetched.');
  //   }
  //   notifyListeners();
  // }

  // Future<void> fetchListSiswa() async {
  //   final Response<dynamic> resp = await _net.request('/kelas');
  //   List<Map<String, Object>> listData = resp.data['data'];
  //   List<dynamic> listData = resp.data['data'];
  //   for (dynamic data in listData)
  //     _siswas.add(SiswaModel.fromJson(data as Map<String, dynamic>));
  //   print(resp.data['data']);
  //   notifyListeners();
  // }

  Future<void> fetchData() async {
    if (_fotos.isEmpty) {
      fetchSiswa();
    } else {
      print('Data telah ditambahkan');
    }
    notifyListeners();
  }
}
