import 'package:bk_mobile_app/screens/homepage/siswa/counseling_page/bimbingan_page.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/histori_page/histori_page.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/main_page/main_page.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/profile_page/profile_page.dart';
import 'package:flutter/material.dart';

class HomePageViewModel with ChangeNotifier {
  HomePageViewModel() {
    currentIndex = 0;
    _screens = <Widget>[
      MainPage(),
      HistoriPage(),
      BimbinganPage(),
      ProfilePage(),
    ];
  }

  int currentIndex;
  List<Widget> _screens;
  List<Widget> get screens => _screens;
  PageController _pageController;

  Future<void> moveTo(int index) async {
    final int temp = (index - currentIndex).abs();

    await _pageController.animateToPage(
      index,
      duration: Duration(milliseconds: temp * 100),
      curve: Curves.easeIn,
    );
    notifyListeners();
  }

  void setIndex(int index) {
    currentIndex = index;
    notifyListeners();
  }

  void setPageController(PageController pageCon) {
    _pageController = pageCon;
  }
}
