import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:get/route_manager.dart';

class WillPopViewModel extends ChangeNotifier {
  WillPopViewModel();

  Future<void> onBackPressed(String index) async {
    Get.offNamed(index);
    notifyListeners();
  }

  Future<void> onGoBack() async {
    Get.back<void>();
    notifyListeners();
  }

  Future<void> exit() async {
    SystemNavigator.pop();
    notifyListeners();
  }
}
