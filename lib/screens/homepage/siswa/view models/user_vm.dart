import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/user_model.dart';
import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:get/route_manager.dart';

class UserViewModel extends ChangeNotifier {
  UserViewModel();
  NetworkService _net;
  UserModel user;

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  var token;

  void setNetworkService(NetworkService net) {
    _net = net;
  }

  Future<void> fetchUser() async {
    final Response<dynamic> resp = await _net.request('/user');
    final Map<String, dynamic> respData = resp.data as Map<String, dynamic>;
    user = UserModel.fromJson(respData);
    print(respData);
    notifyListeners();
  }

  Future<void> setFcmToken() async {
    token = await _firebaseMessaging.getToken();
    // var form = FormData.fromMap({
    //   'fcm_token': token.toString(),
    // });
    // var response =
    //     await _net.request('/fcm', requestMethod: 'post', data: form);
    print(token);
    notifyListeners();
  }
}
