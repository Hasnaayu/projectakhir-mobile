import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/chatbot_model.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/response_model.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/topik_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';

// algoritma:
//   if response || topic then it's bot
//   else if message then it's human

class ChatbotViewModel extends ChangeNotifier {
  List<ChatbotModel> chats = [];
  List<TopicModel> topics = [];
  List<ResponseModel> responses = [];
  List<String> topicString = [];
  List<String> pertanyaanString = [];

  NetworkService _net;

  DateTime dateTime = DateTime.now();
  String content;

  String formatDate() {
    return DateFormat('HH:mm').format(dateTime);
  }

  void setNetworkViewModel(NetworkService net) {
    _net = net;
    notifyListeners();
  }

  Future<void> getTopics() async {
    final Response<dynamic> resp = await _net.request('/topikbot');
    List<dynamic> listData = resp.data['data'];

    for (final dynamic data in listData) {
      topics.add(TopicModel.fromJson(data as Map<String, dynamic>));
    }

    for (var i = 0; i < topics.length; i++) {
      topicString.add(topics[i].topic);
    }
    print(listData);
    //print(topicString);
    await sendTopic();
    notifyListeners();
  }

  Future<void> getResponse(int id) async {
    final Response<dynamic> resp = await _net.request('/topikbot/$id');
    List<dynamic> listData = resp.data['data'];

    for (final dynamic data in listData) {
      responses.add(ResponseModel.fromJson(data as Map<String, dynamic>));
    }

    for (var i = 0; i < responses.length; i++) {
      pertanyaanString.add(responses[i].pertanyaan);
    }
    pertanyaanString.remove(pertanyaanString.first);

    print(listData);
    await sendResponse();
    notifyListeners();
  }

  Future<void> sendChat(String message, {bool isBot = false}) async {
    chats.add(
      ChatbotModel(
        timeStamp: DateTime.now(),
        message: message,
        isBot: isBot,
      ),
    );

    notifyListeners();
  }

  Future<void> sendTopic() async {
    chats.add(
      ChatbotModel(
        topicString: topicString,
        timeStamp: DateTime.now(),
        topic: topics,
        message: 'Topik apa yang anda ingin tanyakan?',
        isBot: true,
      ),
    );
    notifyListeners();
  }

  Future<void> sendResponse() async {
    chats.add(
      ChatbotModel(
        pertanyaanString: pertanyaanString,
        timeStamp: DateTime.now(),
        response: responses,
        message: 'Pertanyaan apa yang ingin anda ketahui?',
        isBot: true,
      ),
    );

    notifyListeners();
  }

  Future<void> onQuestionTapped(int responseIndex) async {
    chats.add(
      ChatbotModel(
        timeStamp: DateTime.now(),
        message: responses[responseIndex].pertanyaan,
        isBot: false,
      ),
    );
    sendChat(responses[responseIndex].jawaban, isBot: true);

    notifyListeners();
  }

  Future<void> onTopicTapped(int topicIndex) async {
    print(topicIndex);
    chats.add(
      ChatbotModel(
        timeStamp: DateTime.now(),
        message: topics[topicIndex].topic,
        isBot: false,
      ),
    );
    await getResponse(topicIndex + 1); //ini kurang idnya gak pas

    notifyListeners();
  }

  // Future<void> removeEarlierData() async {
  //   topics.clear();
  //   topicString.clear();
  //   getTopics();
  //   notifyListeners();
  // }

  // Future<void> fetchData() async {
  //   if (topics.isEmpty || topicString.isEmpty) {
  //     getTopics();
  //   } else {
  //     print('datas have been fetched.');
  //   }
  //   notifyListeners();
  // }
}
