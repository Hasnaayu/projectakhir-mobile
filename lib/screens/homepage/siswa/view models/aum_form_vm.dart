import 'dart:io';

import 'package:bk_mobile_app/auth/mixins/dialog_mixin.dart';
import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/form_aum_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:get/route_manager.dart';

class AumFormViewModel extends ChangeNotifier with DialogMixin {
  AumFormViewModel();
  NetworkService _net;
  FormAumModel form;

  List<FormAumModel> _forms = [];
  List<FormAumModel> get forms => _forms;

  String problem;
  String desc;

  void setNetworkService(NetworkService net) {
    _net = net;
    notifyListeners();
  }

  void onProblem(String val) {
    problem = val;
    print(problem);
    notifyListeners();
  }

  void onDesc(String val) {
    desc = val;
    print(desc);
    notifyListeners();
  }

  Future<void> addNewForm() async {
    var formData = FormData.fromMap({
      'problem': problem,
      'desc': desc,
    });
    var response = await _net.request('/aumform/storeform',
        requestMethod: 'post', data: formData);

    print(response);
    if (response.data['success'] == true) {
      problem = null;
      desc = null;
      Get.toNamed<void>('/homepagesiswa');
      showSuccessSnackbar('Lembar jawaban sudah diterima. Terima kasih.');
    } else {
      showErrorSnackbar('Isi form alat ungkap masalah');
    }

    notifyListeners();
  }

  Future<void> removeEarlierData() async {
    _forms.clear();
    print(_forms);
    fetchDataList();
    notifyListeners();
  }

  Future<void> fetchDataList() async {
    if (_forms.isEmpty) {
      addNewForm();
    } else {
      print('datas have been fetched.');
    }
    notifyListeners();
  }

  Future<void> fetchAumForm(int id) async {
    final Response<dynamic> resp = await _net.request('/getaumform/$id');
    List<dynamic> listData = resp.data['data'];
    print(listData);
    for (dynamic data in listData)
      _forms.add(FormAumModel.fromJson(data as Map<String, dynamic>));
    print(id);
    notifyListeners();
  }

  Future<void> removeEarlierDataForm(int id) async {
    if (_forms != null) _forms.clear();
    print(_forms);
    fetchDataListForm(id);
    notifyListeners();
  }

  Future<void> fetchDataListForm(int id) async {
    if (_forms != null) {
      fetchAumForm(id);
    } else {
      print('datas have been fetched.');
    }
    notifyListeners();
  }
}
