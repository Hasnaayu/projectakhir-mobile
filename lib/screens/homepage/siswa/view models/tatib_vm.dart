import 'dart:io';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/tatib_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:get/route_manager.dart';
import 'package:image_picker/image_picker.dart';

class TatibViewModel extends ChangeNotifier {
  TatibViewModel();
  NetworkService _net;
  TatibModel tatib;
  TatibViewModel _tatib;

  List<TatibModel> _tatibs = [];
  List<TatibModel> get tatibs => _tatibs;

  String isi;

  void setNetworkService(NetworkService net) {
    _net = net;
  }

  Future<void> fetchTatibSatu() async {
    final Response<dynamic> resp = await _net.request('/tatibsatu');
    _tatibs.clear();
    List<dynamic> listData = resp.data['data'];
    for (dynamic data in listData)
      _tatibs.add(TatibModel.fromJson(data as Map<String, dynamic>));
    print(resp);
    notifyListeners();
  }

  Future<void> fetchTatibDua() async {
    final Response<dynamic> resp = await _net.request('/tatibdua');
    _tatibs.clear();
    List<dynamic> listData = resp.data['data'];
    for (dynamic data in listData)
      _tatibs.add(TatibModel.fromJson(data as Map<String, dynamic>));
    print(resp);
    notifyListeners();
  }

  Future<void> fetchTatibTiga() async {
    final Response<dynamic> resp = await _net.request('/tatibtiga');
    _tatibs.clear();
    List<dynamic> listData = resp.data['data'];
    for (dynamic data in listData)
      _tatibs.add(TatibModel.fromJson(data as Map<String, dynamic>));
    print(resp);
    notifyListeners();
  }

  Future<void> fetchTatibEmpat() async {
    final Response<dynamic> resp = await _net.request('/tatibempat');
    _tatibs.clear();
    List<dynamic> listData = resp.data['data'];
    for (dynamic data in listData)
      _tatibs.add(TatibModel.fromJson(data as Map<String, dynamic>));
    print(resp);
    notifyListeners();
  }

  Future<void> fetchTatibLima() async {
    final Response<dynamic> resp = await _net.request('/tatiblima');
    _tatibs.clear();
    List<dynamic> listData = resp.data['data'];
    for (dynamic data in listData)
      _tatibs.add(TatibModel.fromJson(data as Map<String, dynamic>));
    print(resp);
    notifyListeners();
  }

  Future<void> fetchTatibEnam() async {
    final Response<dynamic> resp = await _net.request('/tatibenam');
    _tatibs.clear();
    List<dynamic> listData = resp.data['data'];
    for (dynamic data in listData)
      _tatibs.add(TatibModel.fromJson(data as Map<String, dynamic>));
    print(resp);
    notifyListeners();
  }

  Future<void> removeTatibSatu() async {
    _tatibs.clear();
    // print();
    fetchDataTatibSatu();
    notifyListeners();
  }

  Future<void> fetchDataTatibSatu() async {
    if (_tatibs.isEmpty) {
      fetchTatibSatu();
    } else {
      print('datas have been fetched.');
    }
    notifyListeners();
  }

  Future<void> removeTatibDua() async {
    _tatibs.clear();
    // print();
    fetchDataTatibDua();
    notifyListeners();
  }

  Future<void> fetchDataTatibDua() async {
    if (_tatibs.isEmpty) {
      fetchTatibDua();
    } else {
      print('datas have been fetched.');
    }
    notifyListeners();
  }

  Future<void> removeTatibTiga() async {
    _tatibs.clear();
    // print();
    fetchDataTatibTiga();
    notifyListeners();
  }

  Future<void> fetchDataTatibTiga() async {
    if (_tatibs.isEmpty) {
      fetchTatibTiga();
    } else {
      print('datas have been fetched.');
    }
    notifyListeners();
  }

  Future<void> removeTatibEmpat() async {
    _tatibs.clear();
    // print();
    fetchDataTatibEmpat();
    notifyListeners();
  }

  Future<void> fetchDataTatibEmpat() async {
    if (_tatibs.isEmpty) {
      fetchTatibEmpat();
    } else {
      print('datas have been fetched.');
    }
    notifyListeners();
  }

  Future<void> removeTatibLima() async {
    _tatibs.clear();
    // print();
    fetchDataTatibLima();
    notifyListeners();
  }

  Future<void> fetchDataTatibLima() async {
    if (_tatibs.isEmpty) {
      fetchTatibLima();
    } else {
      print('datas have been fetched.');
    }
    notifyListeners();
  }

  Future<void> removeTatibEnam() async {
    _tatibs.clear();
    // print();
    fetchDataTatibEnam();
    notifyListeners();
  }

  Future<void> fetchDataTatibEnam() async {
    if (_tatibs.isEmpty) {
      fetchTatibEnam();
    } else {
      print('datas have been fetched.');
    }
    notifyListeners();
  }
}
