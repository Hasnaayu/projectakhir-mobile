import 'dart:io';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/siswa_model.dart';
import 'package:flutter/foundation.dart';
import 'package:get/route_manager.dart';
import 'package:image_picker/image_picker.dart';
import 'package:dio/dio.dart';

class PhotoViewModel extends ChangeNotifier {
  PhotoViewModel();

  NetworkService _net;

  File photo;
  final picker = ImagePicker();

  void setNetworkService(NetworkService net) {
    _net = net;
  }

  Future<void> getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      photo = File(pickedFile.path);
    } else {
      print('No photo selected.');
    }
  }

  Future<void> removeImage() async {
    photo = null;
  }

  Future<void> addPhoto() async {
    final Response<dynamic> resp = await _net.request(
      '/siswa/storesiswa',
      requestMethod: 'post',
      data: <String, dynamic>{
        'photo': MultipartFile.fromFile(photo.toString()),
      },
    );
    final Map<String, dynamic> respData = resp.data as Map<String, dynamic>;

    if (respData != null) {
      Get.back<void>();
      print(respData);
    }
    notifyListeners();
  }
}
