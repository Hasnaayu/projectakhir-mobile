import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ChatBubble extends StatefulWidget {
  const ChatBubble({
    @required this.isBot,
    @required this.dateTime,
    this.message = '',
    this.options = const <String>[],
    this.onPressed,
    this.showDateTimeOnResponse = false,
    Key key,
  }) : super(key: key);

  /// isBot mean the resonse of the bot or other people.
  /// by default when isResopnse is true, there will be no date time shown
  /// on the left of the widget.
  ///
  /// to change that, use showDateTimeOnResponse = true.
  final bool isBot;

  /// the messsage that will be shown on the screen
  final String message;

  /// the options that users will be clicking at
  final List<dynamic> options;

  /// when the option is pressed, these callback will be called
  final Future<void> Function(int) onPressed;

  /// the time that message have been sent
  final DateTime dateTime;

  /// show date time on resonse see [isBot] for more info.
  final bool showDateTimeOnResponse;

  @override
  _ChatBubbleState createState() => _ChatBubbleState();
}

class _ChatBubbleState extends State<ChatBubble> {
  // String selectedValue;
  int chatPage = 0;
  int maxPage;
  int selectedIndex = -1;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    maxPage = (widget.options.length / 5).floor();
  }

  /// constant varible
  final Radius roundedRadius = const Radius.circular(16);

  // void select(String value) {
  //   selectedValue = value;
  //   setState(() {});
  // }

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: widget.isBot ? Alignment.topLeft : Alignment.topRight,
      child: FractionallySizedBox(
        widthFactor: 0.9,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            if (!widget.isBot)
              Padding(
                padding: const EdgeInsets.only(right: 4),
                child: Text(
                  formatDate(widget.dateTime),
                  style: TextStyle(color: Colors.black),
                ),
              ),
            Expanded(
              child: Container(
                padding: const EdgeInsets.all(8),
                margin: const EdgeInsets.only(top: 16),
                decoration: BoxDecoration(
                  borderRadius: widget.isBot
                      ? BorderRadius.only(
                          topRight: roundedRadius,
                          bottomRight: roundedRadius,
                          topLeft: roundedRadius,
                        )
                      : BorderRadius.only(
                          topLeft: roundedRadius,
                          bottomLeft: roundedRadius,
                          topRight: roundedRadius,
                        ),
                  color: widget.isBot ? Colors.blue[200] : Colors.red[200],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(widget.message),
                    if (widget.options.isNotEmpty) const SizedBox(height: 8),
                    ...List<Widget>.generate(
                      widget.options.length >= 5 ? 5 : widget.options.length,
                      (int index) {
                        if (index + 5 * chatPage < widget.options.length)
                          return TextButton(
                            onPressed: () async {
                              if (!isLoading) {
                                isLoading = true;
                                selectedIndex = index;
                                setState(() {});
                                await widget.onPressed(index + 5 * chatPage);
                                isLoading = false;
                                selectedIndex = -1;

                                // select(widget.options[(index + 5 * chatPage)]
                                //     .toString());
                              }
                            },
                            child: Row(
                              children: <Widget>[
                                if (isLoading && index == selectedIndex)
                                  const SizedBox(
                                    height: 20,
                                    width: 20,
                                    child: CircularProgressIndicator(),
                                  ),
                                // if (widget.options[(index + 5 * chatPage)]
                                //             .toString() ==
                                //         selectedValue &&
                                //     index != selectedIndex)
                                //   const Icon(Icons.circle_rounded),
                                // if (widget.options[(index + 5 * chatPage)]
                                //             .toString() !=
                                //         selectedValue &&
                                //     index != selectedIndex)
                                //   const Icon(Icons.circle_outlined),
                                const SizedBox(width: 8),
                                Expanded(
                                  child: Text(
                                    widget.options[(index + 5 * chatPage)]
                                        .toString(),
                                  ),
                                ),
                              ],
                            ),
                          );

                        return const SizedBox(
                          height: 48,
                        );
                      },
                    ),
                    if (widget.options.length >= 5)
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          if (chatPage > 0)
                            OutlinedButton(
                              onPressed: () {
                                if (!isLoading) {
                                  setState(() {
                                    chatPage--;
                                  });
                                }
                              },
                              child: Text('<'),
                            ),
                          if (chatPage == 0) const SizedBox(),
                          if (chatPage < maxPage)
                            OutlinedButton(
                              onPressed: () {
                                if (!isLoading) {
                                  setState(() {
                                    chatPage++;
                                  });
                                }
                              },
                              child: Text('>'),
                            ),
                          if (chatPage == maxPage) const SizedBox(),
                        ],
                      )
                  ],
                ),
              ),
            ),
            //widget below will be shown only when
            //isResopnse and showDateTimeOnResopnse are true
            if (widget.isBot && widget.showDateTimeOnResponse)
              Padding(
                padding: const EdgeInsets.only(left: 4),
                child: Text(
                  formatDate(widget.dateTime),
                  style: TextStyle(color: Colors.black),
                ),
              ),
          ],
        ),
      ),
    );
  }

  String formatDate(DateTime dateTime) {
    return DateFormat('HH:mm').format(dateTime);
  }
}
