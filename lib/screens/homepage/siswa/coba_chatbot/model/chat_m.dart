import 'package:bk_mobile_app/screens/homepage/siswa/coba_chatbot/model/response_m.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/coba_chatbot/model/topic_m.dart';

class ChatModel {
  ChatModel({
    this.message,
    this.response,
    this.topic,
    this.timeStamp,
    this.isBot,
  });

  String message;
  List<ResponseModel> response;
  List<TopicModel> topic;
  DateTime timeStamp;
  bool isBot;
}
