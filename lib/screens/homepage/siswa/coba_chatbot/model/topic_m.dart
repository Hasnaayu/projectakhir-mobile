class TopicModel {
  TopicModel({
    this.id,
    this.topic,
  });

  factory TopicModel.fromJson(Map<String, dynamic> json) => TopicModel(
        id: json['id_topik'],
        topic: json['topik'],
      );

  int id;
  String topic;

  @override
  String toString() => topic;
}
