class ResponseModel {
  ResponseModel({
    this.question,
    this.answer,
  });

  ResponseModel.fromJson(Map<String, dynamic> json) {
    question = json['pertanyaan'] as String;
    answer = json['jawaban'] as String;
  }

  String question;
  String answer;

  @override
  String toString() => question;
}
