import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

import 'model/chat_m.dart';
import 'model/response_m.dart';
import 'model/topic_m.dart';

// algoritma:
//   if response || topic then it's bot
//   else if message then it's human

class ChatService extends ChangeNotifier {
  List<ChatModel> chats = [];
  List<TopicModel> topics = [];
  List<ResponseModel> responses = [];

  NetworkService _net;

  void setNetworkViewModel(NetworkService net) {
    _net = net;
    notifyListeners();
  }

  // Future<void> getTopics() async {
  //   Response resp = await _net.request('/topikbot');
  //   List<dynamic> listData = resp.data['data'];
  //   // if success:
  //   // if(resp.statusCode == 200){
  //   //   //do something
  //   // }
  //   for (final dynamic data in listData) {
  //     topics.add(TopicModel.fromJson(data as Map<String, dynamic>));
  //   }
  //   print(resp);
  //   await sendTopic();
  //   notifyListeners();
  // }

  Future<void> getResponse() async {
    Response<dynamic> resp = await _net.request('/tanyajawab');
    List<dynamic> listData = resp.data['data'];

    for (final dynamic data in listData) {
      responses.add(ResponseModel.fromJson(data as Map<String, dynamic>));
    }
    print(resp);
    await sendResponse();
    notifyListeners();
  }

  Future<void> sendChat(String message, {bool isBot = false}) async {
    chats.add(
      ChatModel(
        timeStamp: DateTime.now(),
        message: message,
        isBot: isBot,
      ),
    );
    notifyListeners();
  }

  // Future<void> sendTopic() async {
  //   chats.add(
  //     ChatModel(
  //       timeStamp: DateTime.now(),
  //       topic: topics,
  //       message: 'Topik apa yang anda ingin tanyakan?',
  //       isBot: true,
  //     ),
  //   );
  //   notifyListeners();
  // }

  Future<void> sendResponse() async {
    chats.add(
      ChatModel(
        timeStamp: DateTime.now(),
        response: responses,
        message: 'Pertanyaan apa yang ingin anda ketahui?',
        isBot: true,
      ),
    );

    notifyListeners();
  }

  Future<void> onQuestionTapped(int responseIndex) async {
    chats.add(
      ChatModel(
        timeStamp: DateTime.now(),
        message: responses[responseIndex].question,
        isBot: false,
      ),
    );
    await sendChat(responses[responseIndex].answer, isBot: true);

    notifyListeners();
  }

  // Future<void> onTopicTapped(int topicIndex) async {
  //   chats.add(
  //     ChatModel(
  //       timeStamp: DateTime.now(),
  //       message: topics[topicIndex].topic,
  //       isBot: false,
  //     ),
  //   );
  //   await getResponse();

  //   notifyListeners();
  // }
}

// List<Map<String, dynamic>> topic = <Map<String, dynamic>>[
//   <String, dynamic>{
//     'id_topik': 1,
//     'topik': 'apakah topik 1?',
//   },
//   <String, dynamic>{
//     'id_topik': 2,
//     'topik': 'apakah topik 2?',
//   },
//   <String, dynamic>{
//     'id_topik': 3,
//     'topik': 'apakah topik 3?',
//   },
//   <String, dynamic>{
//     'id_topik': 4,
//     'topik': 'apakah topik 4?',
//   },
//   <String, dynamic>{
//     'id_topik': 5,
//     'topik': 'apakah topik 5?',
//   },
//   <String, dynamic>{
//     'id_topik': 6,
//     'topik': 'apakah topik 6?',
//   },
//   <String, dynamic>{
//     'id_topik': 7,
//     'topik': 'apakah topik 7?',
//   },
// ];

// List<Map<String, dynamic>> response = <Map<String, dynamic>>[
//   <String, dynamic>{
//     'pertanyaan': 'kenapa begitu 1?',
//     'jawaban': 'karena begitulah 1',
//   },
//   <String, dynamic>{
//     'pertanyaan': 'kenapa begitu 2?',
//     'jawaban': 'karena begitulah 2',
//   },
//   <String, dynamic>{
//     'pertanyaan': 'kenapa begitu 3?',
//     'jawaban': 'karena begitulah 3',
//   },
//   <String, dynamic>{
//     'pertanyaan': 'kenapa begitu 4?',
//     'jawaban': 'karena begitulah 4',
//   },
//   <String, dynamic>{
//     'pertanyaan': 'kenapa begitu 5?',
//     'jawaban': 'karena begitulah 5',
//   },
//   <String, dynamic>{
//     'pertanyaan': 'kenapa begitu 6?',
//     'jawaban': 'karena begitulah 6',
//   },
//   <String, dynamic>{
//     'pertanyaan': 'kenapa begitu 7?',
//     'jawaban': 'karena begitulah 7',
//   },
//];
