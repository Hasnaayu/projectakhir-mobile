import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/coba_chatbot/bot_vm.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';

import 'bot_bubble.dart';
import 'model/chat_m.dart';

class BotWindow extends StatefulWidget {
  const BotWindow({Key key}) : super(key: key);

  @override
  _BotWindowState createState() => _BotWindowState();
}

class _BotWindowState extends State<BotWindow> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      context
          .read<ChatService>()
          .setNetworkViewModel(context.read<NetworkService>());
      context.read<ChatService>().getResponse();
    });
    scrollController = ScrollController();
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  ScrollController scrollController;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'BiKos',
          style: CustomFont.appBar,
        ),
        centerTitle: true,
        backgroundColor: CustomColor.themedarker,
        automaticallyImplyLeading: false,
      ),
      body: Consumer<ChatService>(
        builder: (
          BuildContext context,
          ChatService vm,
          Widget child,
        ) =>
            Column(
          children: <Widget>[
            Expanded(
              child: ListView.builder(
                controller: scrollController,
                padding: EdgeInsets.symmetric(horizontal: 16),
                itemCount: vm.chats.length,
                itemBuilder: (BuildContext context, int index) {
                  final ChatModel chat = vm.chats[index];
                  // if (chat.topic != null) {
                  //   return ChatBubble(
                  //     isBot: chat.isBot,
                  //     dateTime: chat.timeStamp,
                  //     message: chat.message,
                  //     options: vm.topics,
                  //     onPressed: (int val) async {
                  //       await vm.onTopicTapped(val);

                  //       scrollController.animateTo(
                  //         scrollController.position.maxScrollExtent + 400,
                  //         duration: Duration(milliseconds: 250),
                  //         curve: Curves.linear,
                  //       );
                  //     },
                  //   );
                  if (chat.response != null) {
                    return ChatBubble(
                      isBot: chat.isBot,
                      dateTime: chat.timeStamp,
                      message: chat.message,
                      options: vm.responses,
                      onPressed: (int val) async {
                        await vm.onQuestionTapped(val);

                        scrollController.animateTo(
                          scrollController.position.maxScrollExtent + 100,
                          duration: Duration(milliseconds: 250),
                          curve: Curves.linear,
                        );
                      },
                    );
                  } else if (chat.message != null) {
                    return ChatBubble(
                      isBot: chat.isBot,
                      dateTime: chat.timeStamp,
                      message: chat.message,
                    );
                  }
                  return SizedBox();
                },
              ),
            ),
            //
            FractionallySizedBox(
              widthFactor: 1,
              child: ElevatedButton(
                onPressed: vm.responses.isNotEmpty
                    ? () {
                        vm.sendResponse();

                        scrollController.animateTo(
                          scrollController.position.maxScrollExtent + 400,
                          duration: Duration(milliseconds: 250),
                          curve: Curves.linear,
                        );
                      }
                    : null,
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text('Tampilkan Pertanyaan'),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
