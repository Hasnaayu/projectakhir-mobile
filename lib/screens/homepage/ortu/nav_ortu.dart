import 'dart:convert';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/auth/view_models/LoginViewModel.dart';
import 'package:bk_mobile_app/screens/homepage/ortu/view_model/nav_ortu_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/home_page_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/user_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/will_pop_vm.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:provider/provider.dart';

class HomePageOrtu extends StatefulWidget {
  @override
  _HomePageOrtuState createState() => _HomePageOrtuState();
}

class _HomePageOrtuState extends State<HomePageOrtu> {
  PageController pageController;

  @override
  void initState() {
    super.initState();
    pageController = PageController();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context
          .read<LoginViewModel>()
          .setNetworkService(context.read<NetworkService>()); //for checkToken()
      context
          .read<UserViewModel>()
          .setNetworkService(context.read<NetworkService>()); //for fetchUser()
      //context.read<LoginViewModel>().checkToken();
      context.read<UserViewModel>().fetchUser();
      context.read<NavOrtuViewModel>().setPageController(pageController);
    });
  }

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<WillPopViewModel>(
      builder: (_, WillPopViewModel vm, __) => WillPopScope(
        onWillPop: () => vm.exit(),
        child: Scaffold(
          body: GestureDetector(
            onTap: FocusScope.of(context).unfocus,
            child: SafeArea(
              child: PageView(
                physics: const BouncingScrollPhysics(),
                controller: pageController,
                onPageChanged: context.read<NavOrtuViewModel>().setIndex,
                children: context.read<NavOrtuViewModel>().screens,
              ),
            ),
          ),
          bottomNavigationBar: Consumer<NavOrtuViewModel>(
            builder: (_, NavOrtuViewModel vm, __) => CurvedNavigationBar(
              onTap: vm.moveTo,
              backgroundColor: CustomColor.graybg,
              color: CustomColor.mutedButton,
              buttonBackgroundColor: CustomColor.graybg,
              height: 70,
              animationDuration: Duration(
                milliseconds: 200,
              ),
              index: vm.currentIndex,
              animationCurve: Curves.linearToEaseOut,
              items: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    ImageIcon(
                      CustomImage.homeNav,
                      size: 30,
                      color: CustomColor.themedarker,
                    ),
                    Text('Home')
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    ImageIcon(
                      CustomImage.chart,
                      size: 30,
                      color: CustomColor.themedarker,
                    ),
                    Text('Chart')
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    ImageIcon(
                      CustomImage.report,
                      size: 30,
                      color: CustomColor.themedarker,
                    ),
                    Text('Panggilan')
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    ImageIcon(
                      CustomImage.profileNav,
                      size: 30,
                      color: CustomColor.themedarker,
                    ),
                    Text('Profile')
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
