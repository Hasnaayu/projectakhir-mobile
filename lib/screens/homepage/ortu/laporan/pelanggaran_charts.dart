import 'package:bk_mobile_app/screens/homepage/gurubk/models/pelanggaran_chart_model.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class PelanggaranChart extends StatelessWidget {
  final List<PelanggaranChartModel> data;

  PelanggaranChart({@required this.data});

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    List<charts.Series<PelanggaranChartModel, String>> series = [
      charts.Series(
          id: "Pelanggaran",
          data: data,
          domainFn: (PelanggaranChartModel series, _) => series.month,
          measureFn: (PelanggaranChartModel series, _) => series.pelanggaran,
          colorFn: (PelanggaranChartModel series, _) => series.barPelanggaran)
    ];
    return Container(
      height: MediaQuery.of(context).size.height / 1.42,
      padding: EdgeInsets.all(10),
      child: Card(
        elevation: 5.0,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Statistik Pelanggaran",
                style: CustomFont.basicTitle,
              ),
              Expanded(
                child: charts.BarChart(
                  series,
                  animate: true,
                  barGroupingType: charts.BarGroupingType.grouped,
                  animationDuration: Duration(seconds: 1),
                ),
              ),
              Padding(padding: EdgeInsets.all(20.0)),
              Column(
                children: [
                  Row(
                    children: [
                      Container(
                        width: 10,
                        height: 10,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: CustomColor.theme),
                      ),
                      Padding(padding: EdgeInsets.all(3.0)),
                      Text(
                        'Pelanggaran',
                        style: CustomFont.smallerMuted,
                      ),
                    ],
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
