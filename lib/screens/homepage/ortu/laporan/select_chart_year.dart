import 'package:bk_mobile_app/styles/font.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SelectChartYear extends StatefulWidget {
  const SelectChartYear(
    this.onTap,
    this.data,
  );

  final void Function(dynamic value) onTap;
  final List<dynamic> data;

  @override
  _SelectChartYearState createState() => _SelectChartYearState();
}

class _SelectChartYearState extends State<SelectChartYear> {
  String search = '';
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return DecoratedBox(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16),
          topRight: Radius.circular(16),
        ),
      ),
      child: Padding(
        padding: EdgeInsets.all(16),
        child: Stack(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 64),
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: ConstrainedBox(
                  constraints: BoxConstraints(maxHeight: size.height),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      for (var index = 0; index < widget.data.length; index++)
                        if (widget.data[index].toString().contains(search))
                          TextButton(
                            onPressed: () =>
                                widget.onTap(widget?.data[index] ?? null),
                            style: ButtonStyle(
                              alignment: Alignment.centerLeft,
                              overlayColor: MaterialStateProperty.all<Color>(
                                Colors.green,
                              ),
                            ),
                            child: Text(
                              widget.data[index].toString(),
                              style: CustomFont.smallMutedDarker,
                            ),
                          ),
                    ],
                  ),
                ),
              ),
            ),
            TextField(
              decoration: InputDecoration(
                  hintText: 'Pilih tahun',
                  enabled: false,
                  hintStyle: CustomFont.basicTitle),
              readOnly: true,
            )
          ],
        ),
      ),
    );
  }
}
