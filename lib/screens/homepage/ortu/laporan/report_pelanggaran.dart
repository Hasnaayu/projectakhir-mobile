import 'package:bk_mobile_app/auth/mixins/dialog_mixin.dart';
import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/pelanggaran_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/siswa_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/user_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/will_pop_vm.dart';
import 'package:bk_mobile_app/screens/homepage/ortu/laporan/select_chart_year.dart';
import 'package:bk_mobile_app/screens/homepage/ortu/laporan/pelanggaran_charts.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class PelanggaranReport extends StatefulWidget {
  @override
  _PelanggaranReportState createState() => _PelanggaranReportState();
}

class _PelanggaranReportState extends State<PelanggaranReport>
    with DialogMixin {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context
          .read<PelanggaranViewModel>()
          .setNetworkService(context.read<NetworkService>());
      context.read<PelanggaranViewModel>().removeData();
      context
          .read<PelanggaranViewModel>()
          .setUserViewModel(context.read<SiswaViewModel>());
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _selectYear(PelanggaranViewModel tVM) {
    showMyBottomSheet(
      context,
      SelectChartYear(
        tVM.onYearChange,
        tVM.years,
      ),
    );
    print(tVM.years);
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Consumer<WillPopViewModel>(
      builder: (_, WillPopViewModel _vm, __) => WillPopScope(
        onWillPop: () => _vm.onGoBack(),
        child: Scaffold(
          appBar: AppBar(
            title: const Text(
              'Chart Pelanggaran',
              style: CustomFont.appBar,
            ),
            centerTitle: true,
            backgroundColor: CustomColor.themedarker,
            automaticallyImplyLeading: false,
          ),
          body: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Consumer<PelanggaranViewModel>(
              builder: (_, PelanggaranViewModel vm, __) => ConstrainedBox(
                constraints: BoxConstraints(maxHeight: size.height / 1.12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 20.0, left: 5.0, bottom: 10.0),
                      child: SizedBox(
                        width: size.width / 2,
                        child: MaterialButton(
                          elevation: 5.0,
                          color: CustomColor.whitebg,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0)),
                          onPressed: () => _selectYear(vm),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image(
                                image: CustomImage.year,
                                height: 30,
                                width: 30,
                              ),
                              vm?.year == null
                                  ? Padding(
                                      padding:
                                          const EdgeInsets.only(left: 20.0),
                                      child: Text(
                                        'Pilih tahun',
                                        style: CustomFont.smallMuted,
                                      ),
                                    )
                                  : Padding(
                                      padding:
                                          const EdgeInsets.only(left: 20.0),
                                      child: Text(
                                        '${vm.year}',
                                        style: CustomFont.smallMutedDarker,
                                      ),
                                    ),
                              Padding(
                                padding: const EdgeInsets.only(left: 20.0),
                                child: Icon(
                                  Icons.keyboard_arrow_down,
                                  color: CustomColor.mutedButton,
                                  size: 30,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    PelanggaranChart(
                      data: vm.pelanggaran_charts,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
