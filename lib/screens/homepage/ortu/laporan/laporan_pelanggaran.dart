import 'dart:convert';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/add_pelanggaran.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/catatan_siswa_intro.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/pelanggaran_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/user_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/route_manager.dart';
import 'package:provider/provider.dart';
import 'package:flutter/scheduler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class LaporanPelanggaran extends StatefulWidget {
  @override
  _LaporanPelanggaranState createState() => _LaporanPelanggaranState();
}

class _LaporanPelanggaranState extends State<LaporanPelanggaran> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context.read<PelanggaranViewModel>().setNetworkService(
          context.read<NetworkService>()); //for fetchPelanggaran()
      context.read<PelanggaranViewModel>().fetchLaporanOrtu();
    });
  }

  void _onRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    context.read<PelanggaranViewModel>().removeLaporan();
    _refreshController.refreshCompleted();
  }

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  void dispose() {
    super.dispose();
    _refreshController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Laporan Pelanggaran Siswa',
          style: CustomFont.appBar,
        ),
        centerTitle: true,
        backgroundColor: CustomColor.themedarker,
      ),
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: false,
        controller: _refreshController,
        onRefresh: _onRefresh,
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Consumer<UserViewModel>(
            builder: (_, UserViewModel vm, __) => InkWell(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8.0, 26.0, 26.0, 26.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(0, 10.0, 10.0, 10.0),
                        ),
                        CircleAvatar(
                          radius: 50,
                          backgroundColor: Colors.grey,
                          child: ClipOval(
                            child: SizedBox(
                              width: 100,
                              height: 100,
                              child: Image(
                                image: CustomImage.avatar,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(20.0, 10.0, 10.0, 10.0),
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              vm?.user?.name ?? '-',
                              style: TextStyle(
                                  fontWeight: FontWeight.w500, fontSize: 18.0),
                            ),
                            Text(
                              'Orang Tua/Wali',
                              style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 18.0,
                                  color: Colors.grey),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Consumer<PelanggaranViewModel>(
                    builder: (_, PelanggaranViewModel vm1, __) => Column(
                      children: [
                        for (var i = 0; i < vm1.pelanggarans.length; i++)
                          Card(
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Container(
                                width: 480,
                                decoration: BoxDecoration(
                                  color: CustomColor.whitebg,
                                  border: Border.all(
                                      width: 2.0,
                                      color: CustomColor.themedarker),
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(16),
                                  ),
                                ),
                                child: Column(
                                  children: [
                                    Container(
                                      padding: const EdgeInsets.fromLTRB(
                                          8.0, 0, 8.0, 0),
                                      child: ListTile(
                                        title: Text('Tanggal Pelanggaran'),
                                        subtitle: Text(
                                            '${vm1?.pelanggarans[i]?.tanggal_pelanggaran}'),
                                      ),
                                    ),
                                    Container(
                                      padding: const EdgeInsets.fromLTRB(
                                          8.0, 0, 8.0, 0),
                                      child: ListTile(
                                        title: Text('Pelanggaran'),
                                        subtitle: Text(
                                            '${vm1?.pelanggarans[i]?.pelanggaran}'),
                                      ),
                                    ),
                                    Container(
                                      padding: const EdgeInsets.fromLTRB(
                                          8.0, 0, 8.0, 0),
                                      child: ListTile(
                                        title: Text('Tindak Lanjut'),
                                        subtitle: Text(
                                            '${vm1?.pelanggarans[i]?.tindak_lanjut}'),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        Padding(
                          padding: const EdgeInsets.all(20.0),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
