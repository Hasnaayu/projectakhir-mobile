import 'dart:convert';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/profile_guru/logout_page.dart';
import 'package:bk_mobile_app/screens/homepage/ortu/view_model/ortu_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/user_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/will_pop_vm.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bk_mobile_app/screens/welcome/login.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:get/route_manager.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:flutter/scheduler.dart';

class ProfileOrtuPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ProfileOrtuPageState();
  }
}

class _ProfileOrtuPageState extends State<ProfileOrtuPage> {
  ScrollController _controller;

  void _profilePage() {
    Get.toNamed<void>('/profilepageortu');
  }

  void _logoutPage() {
    Get.toNamed('/logoutpageortu');
  }

  void _securityPin() {
    Get.toNamed<void>('/my_security_pin');
  }

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context
          .read<UserViewModel>()
          .setNetworkService(context.read<NetworkService>()); //for fetchUser()
      context.read<UserViewModel>().fetchUser();
      context
          .read<OrtuViewModel>()
          .setNetworkService(context.read<NetworkService>());
      context.read<OrtuViewModel>().fetchOrtu();
    });
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Consumer<WillPopViewModel>(
      builder: (_, WillPopViewModel _vm, __) => WillPopScope(
        onWillPop: () => _vm.exit(),
        child: Scaffold(
          appBar: AppBar(
            title: Text(
              'Profile',
              style: CustomFont.appBar,
            ),
            centerTitle: true,
            backgroundColor: CustomColor.themedarker,
            automaticallyImplyLeading: false,
          ),
          body: ListView(
            shrinkWrap: true,
            controller: _controller,
            children: ListTile.divideTiles(
              context: context,
              tiles: [
                Consumer<UserViewModel>(
                  builder: (_, UserViewModel vm, __) => Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      // Padding(
                      //   padding: const EdgeInsets.all(15.0),
                      SizedBox(
                        height: 10.0,
                      ),
                      Consumer<OrtuViewModel>(
                        builder: (_, OrtuViewModel vm1, __) => Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Align(
                              alignment: Alignment.center,
                              child: CircleAvatar(
                                radius: 50,
                                backgroundColor: Colors.grey,
                                child: ClipOval(
                                  child: SizedBox(
                                    width: 100,
                                    height: 100,
                                    child: Image(
                                      image: CustomImage.avatar,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 20, 0, 15),
                          child: Text(
                            vm?.user?.name ?? '-',
                            style: CustomFont.smallMutedDarker,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Consumer<OrtuViewModel>(
                  builder: (_, OrtuViewModel vm1, __) => Container(
                    padding: EdgeInsets.all(5.0),
                    decoration: BoxDecoration(
                        color: CustomColor.whitebg,
                        border: Border(
                            top: BorderSide(
                                color:
                                    CustomColor.mutedButton.withOpacity(0.3)),
                            bottom: BorderSide(
                                color:
                                    CustomColor.mutedButton.withOpacity(0.3)))),
                    child: ListTile(
                      tileColor: CustomColor.whitebg,
                      leading: Image(
                          image: CustomImage.kelas,
                          height: 35,
                          width: 35,
                          color: CustomColor.mutedButton),
                      title: Text(
                        'Nama Ayah',
                        style: CustomFont.smallMuted,
                      ),
                      subtitle: Text(
                        vm1?.ortu?.namaAyah ?? '-',
                      ),
                    ),
                  ),
                ),
                Consumer<OrtuViewModel>(
                  builder: (_, OrtuViewModel vm1, __) => Container(
                    padding: EdgeInsets.all(5.0),
                    decoration: BoxDecoration(
                        color: CustomColor.whitebg,
                        border: Border(
                            top: BorderSide(
                                color:
                                    CustomColor.mutedButton.withOpacity(0.3)),
                            bottom: BorderSide(
                                color:
                                    CustomColor.mutedButton.withOpacity(0.3)))),
                    child: ListTile(
                      tileColor: CustomColor.whitebg,
                      leading: Image(
                          image: CustomImage.kelas,
                          height: 35,
                          width: 35,
                          color: CustomColor.mutedButton),
                      title: Text(
                        'Nama Ibu',
                        style: CustomFont.smallMuted,
                      ),
                      subtitle: Text(
                        vm1?.ortu?.namaIbu ?? '-',
                      ),
                    ),
                  ),
                ),
                Consumer<OrtuViewModel>(
                  builder: (_, OrtuViewModel vm1, __) => Container(
                    padding: EdgeInsets.all(5.0),
                    decoration: BoxDecoration(
                        color: CustomColor.whitebg,
                        border: Border(
                            top: BorderSide(
                                color:
                                    CustomColor.mutedButton.withOpacity(0.3)),
                            bottom: BorderSide(
                                color:
                                    CustomColor.mutedButton.withOpacity(0.3)))),
                    child: ListTile(
                      tileColor: CustomColor.whitebg,
                      leading: Image(
                          image: CustomImage.kelas,
                          height: 35,
                          width: 35,
                          color: CustomColor.mutedButton),
                      title: Text(
                        'Nama Wali',
                        style: CustomFont.smallMuted,
                      ),
                      subtitle: Text(
                        vm1?.ortu?.namaWali ?? '-',
                      ),
                    ),
                  ),
                ),
                // Container(
                //   padding: EdgeInsets.all(5.0),
                //   decoration: BoxDecoration(
                //     color: CustomColor.whitebg,
                //     border: Border(
                //       top: BorderSide(
                //           color: CustomColor.mutedButton.withOpacity(0.3)),
                //       bottom: BorderSide(
                //         color: CustomColor.mutedButton.withOpacity(0.3),
                //       ),
                //     ),
                //   ),
                //   child: ListTile(
                //     tileColor: CustomColor.whitebg,
                //     leading: Image(
                //         image: CustomImage.pin,
                //         height: 35,
                //         width: 35,
                //         color: CustomColor.mutedButton),
                //     title: Text(
                //       'PIN Keamanan',
                //       style: CustomFont.smallMuted,
                //     ),
                //     trailing: Icon(
                //       Icons.keyboard_arrow_right,
                //       size: 35,
                //     ),
                //     onTap: _securityPin,
                //   ),
                // ),
                Container(
                  padding: EdgeInsets.all(5.0),
                  decoration: BoxDecoration(
                      color: CustomColor.whitebg,
                      border: Border(
                          top: BorderSide(
                              color: CustomColor.mutedButton.withOpacity(0.3)),
                          bottom: BorderSide(
                              color:
                                  CustomColor.mutedButton.withOpacity(0.3)))),
                  child: ListTile(
                    tileColor: CustomColor.whitebg,
                    leading: Image(
                        image: CustomImage.logout,
                        height: 35,
                        width: 35,
                        color: CustomColor.mutedButton),
                    title: Text(
                      'Logout',
                      style: CustomFont.smallMuted,
                    ),
                    trailing: Icon(
                      Icons.keyboard_arrow_right,
                      size: 35,
                    ),
                    onTap: _logoutPage,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                ),
              ],
            ).toList(),
          ),
        ),
      ),
    );
  }
}
