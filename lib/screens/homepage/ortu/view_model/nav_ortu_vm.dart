import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/bimbingan_page.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/main_page/main_page_guru.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/profile_guru/profile_guru_page.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/schedule_guru/schedule_page.dart';
import 'package:bk_mobile_app/screens/homepage/ortu/laporan/laporan_pelanggaran.dart';
import 'package:bk_mobile_app/screens/homepage/ortu/laporan/report_pelanggaran.dart';
import 'package:bk_mobile_app/screens/homepage/ortu/panggilan_ortu/panggilan_ortu.dart';
import 'package:bk_mobile_app/screens/homepage/ortu/profile_page/profile_ortu.dart';
import 'package:flutter/material.dart';

class NavOrtuViewModel with ChangeNotifier {
  NavOrtuViewModel() {
    currentIndex = 0;
    _screens = <Widget>[
      LaporanPelanggaran(),
      PelanggaranReport(),
      PanggilanOrtuPage(),
      ProfileOrtuPage(),
    ];
  }

  int currentIndex;
  List<Widget> _screens;
  List<Widget> get screens => _screens;
  PageController _pageController;

  Future<void> moveTo(int index) async {
    final int temp = (index - currentIndex).abs();

    await _pageController.animateToPage(
      index,
      duration: Duration(milliseconds: temp * 100),
      curve: Curves.easeIn,
    );
    notifyListeners();
  }

  void setIndex(int index) {
    currentIndex = index;
    notifyListeners();
  }

  void setPageController(PageController pageCon) {
    _pageController = pageCon;
  }
}
