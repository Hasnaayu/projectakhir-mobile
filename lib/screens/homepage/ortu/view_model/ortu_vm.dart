import 'dart:io';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/ortu/model/ortu_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:get/route_manager.dart';
import 'package:image_picker/image_picker.dart';

class OrtuViewModel extends ChangeNotifier {
  OrtuViewModel();
  NetworkService _net;
  OrtuModel ortu;

  void setNetworkService(NetworkService net) {
    _net = net;
  }

  Future<void> fetchOrtu() async {
    final Response<dynamic> resp = await _net.request('/indexortu');
    final Map<String, dynamic> respData =
        resp.data['data'] as Map<String, dynamic>;
    ortu = OrtuModel.fromJson(respData);
    print(respData);
    notifyListeners();
  }
}
