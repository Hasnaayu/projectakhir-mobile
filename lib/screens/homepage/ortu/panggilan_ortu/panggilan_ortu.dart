import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/kelas_vm.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/panggilan_ortu_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/jadwal_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/will_pop_vm.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:get/route_manager.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class PanggilanOrtuPage extends StatefulWidget {
  @override
  _PanggilanOrtuPageState createState() => _PanggilanOrtuPageState();
}

class _PanggilanOrtuPageState extends State<PanggilanOrtuPage> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context
          .read<PanggilanOrtuViewModel>()
          .setNetworkViewModel(context.read<NetworkService>());
      context.read<PanggilanOrtuViewModel>().removeEarlierPanggilanOrtu();
    });
  }

  void _onRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    context.read<PanggilanOrtuViewModel>().removeEarlierPanggilanOrtu();
    _refreshController.refreshCompleted();
  }

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onAddJadwal() {
    Get.toNamed<void>('/addjadwal');
  }

  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Consumer<WillPopViewModel>(
      builder: (_, WillPopViewModel _vm, __) => WillPopScope(
        onWillPop: () => _vm.onBackPressed('/homepageguru'),
        child: Scaffold(
          backgroundColor: CustomColor.graybg,
          appBar: AppBar(
            title: const Text(
              'Panggilan Orang Tua',
              style: CustomFont.appBar,
            ),
            centerTitle: true,
            backgroundColor: CustomColor.themedarker,
            automaticallyImplyLeading: false,
          ),
          body: SmartRefresher(
            enablePullDown: true,
            enablePullUp: false,
            controller: _refreshController,
            onRefresh: _onRefresh,
            child: SingleChildScrollView(
              child: Consumer<PanggilanOrtuViewModel>(
                builder: (_, PanggilanOrtuViewModel vm, __) => Column(
                  children: [
                    Padding(padding: EdgeInsets.only(top: 20.0)),
                    Center(
                      child: Text(
                        'Muat ulang untuk menampilkan data terbaru.',
                        style: CustomFont.noticeText,
                      ),
                    ),
                    if (vm.panggilans.isEmpty)
                      Center(
                        child: Container(
                          margin: EdgeInsets.only(top: 300.0),
                          child: Text(
                            'Panggilan orang tua kosong.',
                            style: CustomFont.noticeText,
                          ),
                        ),
                      )
                    else if (vm.panggilans.isNotEmpty)
                      for (var i = 0; i < vm.panggilans.length; i++)
                        if (vm.panggilans[i].isDone == null)
                          Column(
                            children: [
                              Card(
                                child: Container(
                                  decoration: BoxDecoration(
                                    color:
                                        Colors.lightBlueAccent.withOpacity(0.2),
                                  ),
                                  child: Slidable(
                                    actionPane: SlidableDrawerActionPane(),
                                    actionExtentRatio: 0.25,
                                    child: Column(
                                      children: [
                                        Container(
                                          width: size.width,
                                          padding: EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          child: Text(
                                            'Akan berlangsung.',
                                            style: CustomFont.waiting,
                                          ),
                                        ),
                                        Container(
                                          width: size.width,
                                          padding: EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          child: ListTile(
                                            title: Text('Tanggal'),
                                            subtitle: Text(
                                              vm?.panggilans[i]
                                                      ?.toStringDue() ??
                                                  '',
                                              style: CustomFont.basicTitle,
                                            ),
                                          ),
                                        ),
                                        Container(
                                          width: size.width,
                                          padding: EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          child: ListTile(
                                            title: Text('Orang tua dari'),
                                            subtitle: Text(
                                              vm?.panggilans[i]?.nama_siswa ??
                                                  '',
                                              style: CustomFont.basicTitle,
                                            ),
                                          ),
                                        ),
                                        Container(
                                          width: size.width,
                                          padding: EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          child: ListTile(
                                            title: Text('Agenda'),
                                            subtitle: Text(
                                              vm?.panggilans[i]?.topik ?? '',
                                              style: CustomFont.basicTitle,
                                            ),
                                          ),
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            Icon(Icons.timer),
                                            Text(
                                                '${vm.panggilans[i].tanggal.difference(vm.today).inDays}'),
                                          ],
                                        ),
                                        Container(
                                          width: size.width,
                                          alignment: Alignment.bottomRight,
                                          padding: EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          child: Text(
                                            vm?.panggilans[i]?.toStringMade() ??
                                                '',
                                            style: CustomFont.smallMuted,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          )
                        else if (vm.panggilans[i].isDone == 1)
                          Column(
                            children: [
                              Card(
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: CustomColor.themelighter
                                        .withOpacity(0.2),
                                  ),
                                  child: Slidable(
                                    actionPane: SlidableDrawerActionPane(),
                                    actionExtentRatio: 0.25,
                                    child: Column(
                                      children: [
                                        Container(
                                          width: size.width,
                                          padding: EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          child: Text(
                                            'Selesai.',
                                            style: CustomFont.accept,
                                          ),
                                        ),
                                        Container(
                                          width: size.width,
                                          padding: EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          child: ListTile(
                                            title: Text('Tanggal'),
                                            subtitle: Text(
                                              vm?.panggilans[i]
                                                      ?.toStringDue() ??
                                                  '',
                                              style: CustomFont.basicTitle,
                                            ),
                                          ),
                                        ),
                                        Container(
                                          width: size.width,
                                          padding: EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          child: ListTile(
                                            title: Text('Orang tua dari'),
                                            subtitle: Text(
                                              vm?.panggilans[i]?.nama_siswa ??
                                                  '',
                                              style: CustomFont.basicTitle,
                                            ),
                                          ),
                                        ),
                                        Container(
                                          width: size.width,
                                          padding: EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          child: ListTile(
                                            title: Text('Agenda'),
                                            subtitle: Text(
                                              vm?.panggilans[i]?.topik ?? '',
                                              style: CustomFont.basicTitle,
                                            ),
                                          ),
                                        ),
                                        Align(
                                            alignment: Alignment.bottomRight,
                                            child: Icon(Icons.check)),
                                        Container(
                                          width: size.width,
                                          alignment: Alignment.bottomRight,
                                          padding: EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          child: Text(
                                            vm?.panggilans[i]?.toStringMade() ??
                                                '',
                                            style: CustomFont.smallMuted,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          )
                        else if (vm.panggilans[i].isDone == 0)
                          Column(
                            children: [
                              Card(
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: CustomColor.orangeaccent
                                        .withOpacity(0.2),
                                  ),
                                  child: Slidable(
                                    actionPane: SlidableDrawerActionPane(),
                                    actionExtentRatio: 0.25,
                                    child: Column(
                                      children: [
                                        Container(
                                          width: size.width,
                                          padding: EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          child: Text(
                                            'Tidak terpenuhi.',
                                            style: CustomFont.reject,
                                          ),
                                        ),
                                        Container(
                                          width: size.width,
                                          padding: EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          child: ListTile(
                                            title: Text('Tanggal'),
                                            subtitle: Text(
                                              vm?.panggilans[i]
                                                      ?.toStringDue() ??
                                                  '',
                                              style: CustomFont.basicTitle,
                                            ),
                                          ),
                                        ),
                                        Container(
                                          width: size.width,
                                          padding: EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          child: ListTile(
                                            title: Text('Orang tua dari'),
                                            subtitle: Text(
                                              vm?.panggilans[i]?.nama_siswa ??
                                                  '',
                                              style: CustomFont.basicTitle,
                                            ),
                                          ),
                                        ),
                                        Container(
                                          width: size.width,
                                          padding: EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          child: ListTile(
                                            title: Text('Agenda'),
                                            subtitle: Text(
                                              vm?.panggilans[i]?.topik ?? '',
                                              style: CustomFont.basicTitle,
                                            ),
                                          ),
                                        ),
                                        Align(
                                            alignment: Alignment.bottomRight,
                                            child: Icon(Icons.cancel)),
                                        Container(
                                          width: size.width,
                                          alignment: Alignment.bottomRight,
                                          padding: EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          child: Text(
                                            vm?.panggilans[i]?.toStringMade() ??
                                                '',
                                            style: CustomFont.smallMuted,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
