class OrtuModel {
  OrtuModel({
    this.id,
    this.namaAyah,
    this.namaIbu,
    this.namaWali,
  });

  OrtuModel.fromJson(Map<String, dynamic> json) {
    id = json['id_ortu'] as int;
    namaAyah = json['nama_ayah'] as String;
    namaIbu = json['nama_ibu'] as String;
    namaWali = json['nama_wali'] as String;
  }

  int id;
  String namaAyah;
  String namaIbu;
  String namaWali;
}
