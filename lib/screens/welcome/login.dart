import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/auth/view_models/LoginViewModel.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/will_pop_vm.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:provider/provider.dart';
import 'package:flutter/scheduler.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  bool _isHidePassword = true;
  String email = '';
  String password = '';

  void _forgot() {
    Get.toNamed<void>('/forgot');
  }

  void _togglePasswordVisibility() {
    setState(() {
      _isHidePassword = !_isHidePassword;
    });
  }

  @override
  void initState() {
    super.initState();
    emailController = TextEditingController();
    passwordController = TextEditingController();
    SchedulerBinding.instance.addPostFrameCallback((Duration _) {
      onFinishLoading();
      final LoginViewModel svm = context.read<LoginViewModel>();
      emailController.text = svm.email;
      passwordController.text = null;
      svm.password = null;
    });
  }

  @override
  void dispose() {
    super.dispose();
    emailController.dispose();
    passwordController.dispose();
  }

  Future<void> onFinishLoading() async {
    await Future<void>.delayed(const Duration(milliseconds: 1000));
    context
        .read<LoginViewModel>()
        .setNetworkService(context.read<NetworkService>());
    //context.read<LoginViewModel>().isUserAvailable();
  }

  TextEditingController emailController;
  TextEditingController passwordController;

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Consumer<WillPopViewModel>(
      builder: (_, WillPopViewModel _vm, __) => WillPopScope(
        onWillPop: () => _vm.exit(),
        child: Scaffold(
          body: Consumer<LoginViewModel>(
            builder: (_, LoginViewModel vm, __) => Container(
              width: size.width,
              height: size.height,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.all(60.0),
                      child: Image(
                        image: CustomImage.appLogo,
                        width: 260.0,
                        height: 260.0,
                      ),
                    ),
                  ),
                  Container(
                    width: size.width / 1.5,
                    decoration: BoxDecoration(
                      color: CustomColor.mutedButton.withOpacity(0.1),
                    ),
                    child: TextFormField(
                      keyboardType: TextInputType.text,
                      autofocus: false,
                      maxLines: 1,
                      onChanged: vm.onEmailChange,
                      controller: emailController,
                      decoration: InputDecoration(
                        hintText: 'Email',
                        hintStyle: TextStyle(color: CustomColor.themedarker),
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      ),
                      validator: (emailValue) {
                        if (emailValue.isEmpty) {
                          return 'Please enter your email';
                        }
                        email = emailValue;
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(5.0),
                  ),
                  Container(
                    width: size.width / 1.5,
                    decoration: BoxDecoration(
                      color: CustomColor.mutedButton.withOpacity(0.1),
                    ),
                    child: TextFormField(
                      obscureText: _isHidePassword,
                      autofocus: false,
                      maxLines: 1,
                      controller: passwordController,
                      onChanged: vm.onPasswordChange,
                      keyboardType: TextInputType.text,
                      validator: (passwordValue) {
                        if (passwordValue.isEmpty) {
                          return 'Please enter your password';
                        }
                        password = passwordValue;
                        return null;
                      },
                      decoration: InputDecoration(
                        hintText: 'Password',
                        hintStyle: TextStyle(color: CustomColor.themedarker),
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        suffixIcon: GestureDetector(
                          onTap: () {
                            _togglePasswordVisibility();
                          },
                          child: Icon(
                            _isHidePassword
                                ? Icons.visibility_off
                                : Icons.visibility,
                            color: _isHidePassword ? Colors.grey : Colors.blue,
                          ),
                        ),
                        isDense: true,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(20.0),
                  ),
                  MaterialButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0)),
                    child: Text(
                      'Login'.toUpperCase(),
                      style: CustomFont.signIn,
                    ),
                    minWidth: size.width / 1.5,
                    padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    color: CustomColor.themedarker,
                    onPressed: vm.validateToken,
                  ),
                  Padding(padding: EdgeInsets.all(5.0)),
                  TextButton(
                    child: Text(
                      'Forgot Password?',
                      style: CustomFont.smallerMuted,
                    ),
                    onPressed: _forgot,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
