import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/auth/view_models/LoginViewModel.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/will_pop_vm.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/route_manager.dart';
import 'package:provider/provider.dart';

class ForgotPage extends StatefulWidget {
  @override
  _ForgotPageState createState() => _ForgotPageState();
}

class _ForgotPageState extends State<ForgotPage> {
  @override
  void initState() {
    super.initState();
    emailController = TextEditingController();
    SchedulerBinding.instance.addPostFrameCallback((Duration _) {
      onFinishLoading();
      final LoginViewModel svm = context.read<LoginViewModel>();
      emailController.text = null;
    });
  }

  @override
  void dispose() {
    super.dispose();
    emailController.dispose();
  }

  Future<void> onFinishLoading() async {
    await Future<void>.delayed(const Duration(milliseconds: 1000));
    context
        .read<LoginViewModel>()
        .setNetworkService(context.read<NetworkService>());
    //context.read<LoginViewModel>().isUserAvailable();
  }

  TextEditingController emailController;

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Consumer<WillPopViewModel>(
      builder: (_, WillPopViewModel _vm, __) => WillPopScope(
        onWillPop: () => _vm.onBackPressed('/'),
        child: Scaffold(
          body: Consumer<LoginViewModel>(
            builder: (_, LoginViewModel vm, __) => Container(
              width: size.width,
              height: size.height,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Center(
                    child: Image(
                      image: CustomImage.appLogo,
                      width: 260.0,
                      height: 260.0,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10.0),
                  ),
                  Container(
                    width: size.width / 1.5,
                    decoration: BoxDecoration(
                      color: CustomColor.mutedButton.withOpacity(0.1),
                    ),
                    child: TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      autofocus: false,
                      maxLines: 1,
                      onChanged: vm.onForgotChange,
                      controller: emailController,
                      decoration: InputDecoration(
                        hintText: 'Email',
                        errorText: vm.emailError,
                        hintStyle: TextStyle(color: CustomColor.themedarker),
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(20.0),
                  ),
                  MaterialButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0)),
                    child: Text(
                      'send'.toUpperCase(),
                      style: CustomFont.signIn,
                    ),
                    minWidth: size.width / 1.5,
                    padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    color: CustomColor.themedarker,
                    onPressed: vm.forgot,
                  ),
                  Padding(padding: EdgeInsets.all(5.0)),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
