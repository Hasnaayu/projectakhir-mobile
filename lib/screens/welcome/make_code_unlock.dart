import 'dart:math';
import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/auth/view_models/user_security_pin_view_model.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/will_pop_vm.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'KeyPad.dart';

class MakeCodeUnlock extends StatefulWidget {
  @override
  _MakeCodeUnlockState createState() => _MakeCodeUnlockState();
}

class _MakeCodeUnlockState extends State {
  TextEditingController pinController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<WillPopViewModel>(
      builder: (_, WillPopViewModel _vm, __) => WillPopScope(
        onWillPop: () => _vm.onGoBack(),
        child: Scaffold(
          backgroundColor: CustomColor.whitebg,
          body: Builder(
            builder: (context) => Consumer<UserSecurityPinViewModel>(
              builder: (_, UserSecurityPinViewModel vm, __) => Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.all(60.0),
                      child: Image(
                        image: CustomImage.appLogo,
                        width: 260.0,
                        height: 260.0,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 50, right: 50, bottom: 60),
                    child: Center(
                      child: TextField(
                        controller: pinController,
                        readOnly: true,
                        obscureText: vm.isHidePassword,
                        textAlign: TextAlign.center,
                        style: CustomFont.bigTheme,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: '',
                            hintStyle: CustomFont.bigMuted),
                      ),
                    ),
                  ),
                  KeyPad(
                    pinController: pinController,
                    isPinLogin: false,
                    onChange: vm.onChangePin,
                    onSubmit: vm.turnOnPin,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
