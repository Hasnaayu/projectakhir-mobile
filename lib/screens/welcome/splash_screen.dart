import 'dart:async';

import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/auth/view_models/LoginViewModel.dart';
import 'package:bk_mobile_app/auth/view_models/user_security_pin_view_model.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/will_pop_vm.dart';
import 'package:bk_mobile_app/styles/color.dart';
import 'package:bk_mobile_app/styles/font.dart';
import 'package:bk_mobile_app/styles/image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:get/route_manager.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:provider/provider.dart';

class SplashScreenPage extends StatefulWidget {
  @override
  _SplashScreenPageState createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((Duration _) {
      onFinishLoading();
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> onFinishLoading() async {
    await Future<void>.delayed(const Duration(milliseconds: 2600));
    Get.toNamed('/login');
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Consumer<WillPopViewModel>(
      builder: (_, WillPopViewModel _vm, __) => WillPopScope(
        onWillPop: () => _vm.exit(),
        child: Scaffold(
          backgroundColor: CustomColor.whitebg,
          body: Container(
            width: size.width,
            height: size.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(40.0),
                    child: Image(
                      image: CustomImage.appLogo,
                      width: 260.0,
                      height: 260.0,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Center(
                    child: Text('BiKos', style: CustomFont.basicTitleSplash),
                  ),
                ),
                Loading(
                    indicator: BallPulseIndicator(),
                    size: 50.0,
                    color: CustomColor.themedarker),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
