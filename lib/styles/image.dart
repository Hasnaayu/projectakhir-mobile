import 'package:flutter/cupertino.dart';

class CustomImage {
  //LOGO
  static const AssetImage appLogo = AssetImage('lib/assets/images/logosmk.png');

  //NAVBAR
  static const AssetImage homeNav = AssetImage('lib/assets/images/home.png');
  static const AssetImage infoNav = AssetImage('lib/assets/images/info.png');
  static const AssetImage chatNav = AssetImage('lib/assets/images/chat.png');
  static const AssetImage profileNav =
      AssetImage('lib/assets/images/profile-user.png');
  static const AssetImage historyNav = AssetImage('lib/assets/images/file.png');
  static const AssetImage scheduleNav =
      AssetImage('lib/assets/images/calendar.png');
  static const AssetImage chart = AssetImage('lib/assets/images/statistic.png');
  static const AssetImage report = AssetImage('lib/assets/images/mail.png');

  //MAIN PAGE
  static const AssetImage menu1 = AssetImage('lib/assets/images/menu1.png');
  static const AssetImage menu2 = AssetImage('lib/assets/images/Chatting.png');
  static const AssetImage menu3 =
      AssetImage('lib/assets/images/Information.png');
  static const AssetImage menu4 = AssetImage('lib/assets/images/Terms.png');
  static const AssetImage guru1 = AssetImage('lib/assets/images/catatan.png');
  static const AssetImage guru2 = AssetImage('lib/assets/images/report.png');
  static const AssetImage userSiswa =
      AssetImage('lib/assets/images/studysmk.png');
  static const AssetImage userGuru =
      AssetImage('lib/assets/images/businessman.png');
  static const AssetImage schedule =
      AssetImage('lib/assets/images/schedule.png');

  //Profile
  static const AssetImage nisn = AssetImage('lib/assets/images/id-card.png');
  static const AssetImage kelas = AssetImage('lib/assets/images/person.png');
  static const AssetImage email = AssetImage('lib/assets/images/email.png');
  static const AssetImage address = AssetImage('lib/assets/images/address.png');
  static const AssetImage pin = AssetImage('lib/assets/images/lock.png');
  static const AssetImage logout = AssetImage('lib/assets/images/arrow.png');
  static const AssetImage avatar = AssetImage('lib/assets/images/user.png');

  //HISTORY
  static const AssetImage pelanggaran =
      AssetImage('lib/assets/images/pelanggaran.png');
  static const AssetImage bimbingan =
      AssetImage('lib/assets/images/bimbingan.png');

  //AUM
  static const AssetImage problem = AssetImage('lib/assets/images/problem.png');
  static const AssetImage students =
      AssetImage('lib/assets/images/students.png');

  //Bimbingan
  static const AssetImage chatbot = AssetImage('lib/assets/images/Chatbot.png');
  static const AssetImage chat = AssetImage('lib/assets/images/Chatting.png');

  //Tatib
  static const AssetImage tatib1 = AssetImage('lib/assets/images/school.png');
  static const AssetImage tatib2 =
      AssetImage('lib/assets/images/kewajiban.png');
  static const AssetImage tatib3 = AssetImage('lib/assets/images/banned.png');
  static const AssetImage tatib4 = AssetImage('lib/assets/images/penalty.png');
  static const AssetImage tatib5 =
      AssetImage('lib/assets/images/appreciation.png');
  static const AssetImage tatib6 = AssetImage('lib/assets/images/others.png');

  //Chart
  static const AssetImage year = AssetImage('lib/assets/images/verified.png');
}
