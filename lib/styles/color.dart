import 'package:flutter/cupertino.dart';

class CustomColor {
  static const Color theme = Color(0xFF00C853);
  static const Color themelighter = Color(0xFF43A047);
  static const Color themelightest = Color(0xFF4CAF50);
  static const Color themedarker = Color(0xFF2E7D32);
  static const List<Color> gradientColor = [themelighter, themelightest];

  static const Color whitebg = Color(0xFFFFFFFF);
  static const Color graybg = Color(0xFFFAFAFA);
  static const Color white1 = Color(0xFFF3FFEE);
  static const Color text = Color(0x0FF000000);
  static const Color mutedText = Color(0xFF000000);
  static const Color mutedButton = Color(0xFF858585);
  static const Color mutedTextDarker = Color(0xFF404040);
  static const Color lightgrey = Color(0xFFD6D6D6);
  static const Color blue = Color(0xFF03A9F4);

  static const Color deeporange = Color(0xFFFF5722);
  static const Color lime = Color(0xFFAFB42B);
  static const Color orangeaccent = Color(0xFFFF9E80);
  static const Color indigo = Color(0xFFC5CAE9);
  static const Color yellow = Color(0xFFFFEE58);
  static const Color tealaccent = Color(0xFF80CBC4);
}
