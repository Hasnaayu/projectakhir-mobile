import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'color.dart';

class CustomFont {
  static const TextStyle introTitle = TextStyle(
      fontSize: 30, color: CustomColor.text, fontWeight: FontWeight.w600);
  static const TextStyle appBar = TextStyle(
      fontSize: 24, color: CustomColor.text, fontWeight: FontWeight.w600);
  static const TextStyle basicText =
      TextStyle(fontSize: 15, color: CustomColor.text);
  static const TextStyle basicTextgrey =
      TextStyle(fontSize: 16, color: CustomColor.mutedButton);
  static const TextStyle noticeText =
      TextStyle(fontSize: 15, color: CustomColor.themedarker);
  static const TextStyle basicText2 =
      TextStyle(fontSize: 17, color: CustomColor.text);
  static const TextStyle option =
      TextStyle(fontSize: 18, color: CustomColor.text);
  static const TextStyle basicTitle = TextStyle(
      fontSize: 23, color: CustomColor.text, fontWeight: FontWeight.w400);
  static const TextStyle basicTitleSplash = TextStyle(
      fontSize: 30, color: CustomColor.text, fontWeight: FontWeight.w600);
  static const TextStyle smallMuted = TextStyle(
      fontSize: 16, color: CustomColor.mutedText, fontWeight: FontWeight.w500);
  static const TextStyle smallMutedDarker =
      TextStyle(fontSize: 17, color: CustomColor.mutedTextDarker);
  static const TextStyle smallTheme =
      TextStyle(fontSize: 15, color: CustomColor.theme);
  static const TextStyle signIn = TextStyle(
      fontSize: 18, color: CustomColor.white1, fontWeight: FontWeight.w700);
  static const TextStyle regist =
      TextStyle(fontSize: 18, color: CustomColor.mutedText);
  static const TextStyle accept =
      TextStyle(fontSize: 16, color: CustomColor.themedarker);
  static const TextStyle reject = TextStyle(fontSize: 16, color: Colors.red);
  static const TextStyle waiting = TextStyle(fontSize: 16, color: Colors.blue);
  static const TextStyle smallerMuted = TextStyle(
      fontSize: 13,
      color: CustomColor.mutedText,
      fontWeight: FontWeight.w500,
      fontStyle: FontStyle.italic);
  static const TextStyle bigTheme = TextStyle(
      fontSize: 50, color: CustomColor.theme, fontWeight: FontWeight.w600);
  static const TextStyle bigMuted = TextStyle(
      fontSize: 50,
      color: CustomColor.mutedButton,
      fontWeight: FontWeight.w600);
}
