import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/auth/services/routes.dart';
import 'package:bk_mobile_app/auth/view_models/LoginViewModel.dart';
import 'package:bk_mobile_app/auth/view_models/LogoutViewModel.dart';
import 'package:bk_mobile_app/auth/view_models/user_security_pin_view_model.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/view_models/chat_vm.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/bimbingan_vm.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/gurubk_vm.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/kelas_vm.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/panggilan_ortu_vm.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/pelanggaran_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/anm_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/dpi_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/edk_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/form_aum_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/hmm_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/hso_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/jdk_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/kdp_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/khk_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/pdp_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/wsg_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/coba_chatbot/bot_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/aum_form_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/chatbot_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/jadwal_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/siswa_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/tatib_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/user_vm.dart';
import 'package:bk_mobile_app/screens/homepage/ortu/view_model/ortu_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/will_pop_vm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/services/aum_vm.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:get/get.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: <SingleChildWidget>[
        ChangeNotifierProvider<NetworkService>(
          create: (_) => NetworkService(),
        ),
        ChangeNotifierProvider<LoginViewModel>(
          create: (_) => LoginViewModel(),
        ),
        ChangeNotifierProvider<WillPopViewModel>(
          create: (_) => WillPopViewModel(),
        ),
        ChangeNotifierProvider<UserViewModel>(
          create: (_) => UserViewModel(),
        ),
        ChangeNotifierProvider<OrtuViewModel>(
          create: (_) => OrtuViewModel(),
        ),
        ChangeNotifierProvider<SiswaViewModel>(
          create: (_) => SiswaViewModel(),
        ),
        ChangeNotifierProvider<GurubkViewModel>(
          create: (_) => GurubkViewModel(),
        ),
        ChangeNotifierProvider<PelanggaranViewModel>(
          create: (_) => PelanggaranViewModel(),
        ),
        ChangeNotifierProvider<BimbinganViewModel>(
          create: (_) => BimbinganViewModel(),
        ),
        ChangeNotifierProvider<KelasViewModel>(
          create: (_) => KelasViewModel(),
        ),
        ChangeNotifierProvider<JdkService>(
          create: (_) => JdkService(),
        ),
        ChangeNotifierProvider<TatibViewModel>(
          create: (_) => TatibViewModel(),
        ),
        ChangeNotifierProvider<DpiService>(
          create: (_) => DpiService(),
        ),
        ChangeNotifierProvider<HsoService>(
          create: (_) => HsoService(),
        ),
        ChangeNotifierProvider<EdkService>(
          create: (_) => EdkService(),
        ),
        ChangeNotifierProvider<KdpService>(
          create: (_) => KdpService(),
        ),
        ChangeNotifierProvider<PdpService>(
          create: (_) => PdpService(),
        ),
        ChangeNotifierProvider<AnmService>(
          create: (_) => AnmService(),
        ),
        ChangeNotifierProvider<HmmService>(
          create: (_) => HmmService(),
        ),
        ChangeNotifierProvider<KhkService>(
          create: (_) => KhkService(),
        ),
        ChangeNotifierProvider<WsgService>(
          create: (_) => WsgService(),
        ),
        ChangeNotifierProvider<AumFormViewModel>(
          create: (_) => AumFormViewModel(),
        ),
        ChangeNotifierProvider<LogoutViewModel>(
          create: (_) => LogoutViewModel(),
        ),
        ChangeNotifierProvider<AumViewModel>(
          create: (_) => AumViewModel(),
        ),
        ChangeNotifierProvider<JadwalViewModel>(
          create: (_) => JadwalViewModel(),
        ),
        ChangeNotifierProvider<ChatbotViewModel>(
          create: (_) => ChatbotViewModel(),
        ),
        ChangeNotifierProvider<ChatViewModel>(
          create: (_) => ChatViewModel(),
        ),
        ChangeNotifierProvider<ChatService>(
          create: (_) => ChatService(),
        ),
        ChangeNotifierProvider<UserSecurityPinViewModel>(
          create: (_) => UserSecurityPinViewModel(),
        ),
        ChangeNotifierProvider<PanggilanOrtuViewModel>(
          create: (_) => PanggilanOrtuViewModel(),
        ),
      ],
      builder: (_, __) => GetMaterialApp(
        theme: ThemeData(
          fontFamily: 'Roboto',
          primarySwatch: Colors.green,
          scaffoldBackgroundColor: Colors.white,
          hintColor: Colors.white,
          inputDecorationTheme: const InputDecorationTheme(
            labelStyle: TextStyle(
              color: Colors.white,
            ),
          ),
        ),
        //home: CheckAuth(),
        //home: MainPage(),
        title: 'BiKos',
        getPages: Routes().pages,
        debugShowCheckedModeBanner: false,
        //initialRoute: '/',
      ),
    );
  }
}
