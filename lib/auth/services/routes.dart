import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/auth/view_models/LoginViewModel.dart';
import 'package:bk_mobile_app/auth/view_models/LogoutViewModel.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/aum_result/aum_siswa_result.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/aum_result/form_aum_result.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/add_bimbingan.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/add_pelanggaran.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/create_bimbingan.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/create_pelanggaran.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/edit_bimbingan.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/edit_pelanggaran.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/list_kelas.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/list_kelas_bimbingan.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/list_siswa.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/list_siswa_bimbingan.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/panggilan_ortu/create_panggilan.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/panggilan_ortu/edit_panggilan.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/panggilan_ortu/add_panggilan.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/panggilan_ortu/list_kelas_panggilan.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/catatan_siswa/panggilan_ortu/list_siswa_panggilan.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/bimbingan_page.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/counseling_guru/chatting_screen.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/navbar_guru.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/profile_guru/profile_guru_page.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/homepage_guru_vm.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/view_models/pelanggaran_vm.dart';
import 'package:bk_mobile_app/screens/homepage/home_page.dart';
import 'package:bk_mobile_app/screens/homepage/ortu/laporan/laporan_pelanggaran.dart';
import 'package:bk_mobile_app/screens/homepage/ortu/profile_page/logout_page.dart';
import 'package:bk_mobile_app/screens/homepage/ortu/profile_page/profile_ortu.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/aum_first_page.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/screens/bidang/hmm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/screens/form_aum/form_aum.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/screens/result/result_anm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/screens/result/result_dpi.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/screens/result/result_edk.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/screens/result/result_hmm.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/screens/result/result_hso.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/screens/result/result_jdk.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/screens/result/result_kdp.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/screens/result/result_khk.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/screens/result/result_pdp.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/screens/result/result_wsg.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/coba_chatbot/bot_window.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/counseling_page/bimbingan_page.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/counseling_page/chat_window.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/counseling_page/chatbot_bubble.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/counseling_page/edit_jadwal.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/counseling_page/form_jadwal_siswa.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/counseling_page/jadwal_siswa.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/counseling_page/chat_window.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/counseling_page/livechat.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/main_page/main_page.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/profile_page/logout_page.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/profile_page/my_security_pin_page.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/profile_page/profile_page.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/tatib_page/bab_tatib/first_tatib.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/tatib_page/bab_tatib/second_tatib.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/tatib_page/bab_tatib/third_tatib.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/tatib_page/bab_tatib/fifth_tatib.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/tatib_page/bab_tatib/sixth_tatib.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/tatib_page/bab_tatib/fourth_tatib.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/home_page_vm.dart';
import 'package:bk_mobile_app/screens/homepage/ortu/nav_ortu.dart';
import 'package:bk_mobile_app/screens/homepage/ortu/view_model/nav_ortu_vm.dart';
import 'package:bk_mobile_app/screens/homepage/gurubk/aum_result/list_siswa_aum.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/view%20models/user_vm.dart';
import 'package:bk_mobile_app/screens/welcome/code_unlock.dart';
import 'package:bk_mobile_app/screens/welcome/forgot_page.dart';
import 'package:bk_mobile_app/screens/welcome/login.dart';
import 'package:bk_mobile_app/screens/welcome/make_code_unlock.dart';
import 'package:bk_mobile_app/screens/homepage/ortu/laporan/report_pelanggaran.dart';
import 'package:bk_mobile_app/screens/welcome/splash_screen.dart';
import 'package:flutter/widgets.dart';
import 'package:get/route_manager.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

class Routes {
  List<GetPage> pages = <GetPage>[
    GetPage(
      name: '/',
      page: () => SplashScreenPage(),
    ),
    GetPage(
      name: '/login',
      page: () => Login(),
    ),
    GetPage(
      name: '/forgot',
      page: () => ForgotPage(),
    ),
    // GetPage(
    //   name: '/homepagesiswa',
    //   page: () => HomePage(0),
    // ),
    // GetPage(
    //   name: '/homepageguru',
    //   page: () => HomePageGuru(0),
    // ),
    GetPage(
      name: '/logoutpage',
      page: () => LogoutPage(),
    ),
    GetPage(
      name: '/homepagesiswa',
      page: () => MultiProvider(
        providers: <SingleChildWidget>[
          ChangeNotifierProvider<HomePageViewModel>(
            create: (_) => HomePageViewModel(),
          ),
          ChangeNotifierProvider<UserViewModel>(
            create: (_) => UserViewModel(),
          ),
        ],
        builder: (_, __) => HomePage(),
      ),
    ),
    GetPage(
      name: '/homepageguru',
      page: () => MultiProvider(
        providers: <SingleChildWidget>[
          ChangeNotifierProvider<HomePageGuruViewModel>(
            create: (_) => HomePageGuruViewModel(),
          ),
          ChangeNotifierProvider<UserViewModel>(
            create: (_) => UserViewModel(),
          ),
        ],
        builder: (_, __) => HomePageGuru(),
      ),
    ),
    GetPage(
      name: '/homepageortu',
      page: () => MultiProvider(
        providers: <SingleChildWidget>[
          ChangeNotifierProvider<UserViewModel>(
            create: (_) => UserViewModel(),
          ),
          ChangeNotifierProvider<PelanggaranViewModel>(
            create: (_) => PelanggaranViewModel(),
          ),
          ChangeNotifierProvider<NavOrtuViewModel>(
            create: (_) => NavOrtuViewModel(),
          ),
        ],
        builder: (_, __) => HomePageOrtu(),
      ),
    ),
    GetPage(
      name: '/profilepagesiswa',
      page: () => ChangeNotifierProvider(
          create: (BuildContext context) {
            return LogoutViewModel();
          },
          builder: (_, __) => ProfilePage()),
    ),
    GetPage(
      name: '/profilepageguru',
      page: () => ChangeNotifierProvider(
          create: (BuildContext context) {
            return LogoutViewModel();
          },
          builder: (_, __) => ProfileGuruPage()),
    ),
    GetPage(
      name: '/profilepageortu',
      page: () => ChangeNotifierProvider(
          create: (BuildContext context) {
            return LogoutViewModel();
          },
          builder: (_, __) => ProfileOrtuPage()),
    ),
    GetPage(name: '/listsiswa', page: () => ListSiswa()),
    GetPage(name: '/createpelanggaran', page: () => CreatePelanggaran()),
    GetPage(name: '/addpelanggaran', page: () => AddPelanggaran()),
    GetPage(name: '/editpelanggaran', page: () => EditPelanggaran()),

    GetPage(name: '/listsiswabimbingan', page: () => ListSiswaBimbingan()),
    GetPage(name: '/createbimbingan', page: () => CreateBimbingan()),
    GetPage(name: '/addbimbingan', page: () => AddBimbingan()),
    GetPage(name: '/editbimbingan', page: () => EditBimbingan()),

    GetPage(name: '/laporanpelanggaran', page: () => LaporanPelanggaran()),

    GetPage(name: '/listsiswaaum', page: () => ListSiswaAum()),
    GetPage(name: '/formpage', page: () => FormPage()),

    GetPage(name: '/tatibsatu', page: () => TatibSatu()),
    GetPage(name: '/tatibdua', page: () => TatibDua()),
    GetPage(name: '/tatibtiga', page: () => TatibTiga()),
    GetPage(name: '/tatibempat', page: () => TatibEmpat()),
    GetPage(name: '/tatiblima', page: () => TatibLima()),
    GetPage(name: '/tatibenam', page: () => TatibEnam()),

    GetPage(name: '/hmmscreen', page: () => HmmScreen()),

    GetPage(name: '/resultdpi', page: () => ResultDpiScreen()),
    GetPage(name: '/resultedk', page: () => ResultEdkScreen()),
    GetPage(name: '/resulthmm', page: () => ResultHmmScreen()),
    GetPage(name: '/resulthso', page: () => ResultHsoScreen()),
    GetPage(name: '/resultjdk', page: () => ResultJdkScreen()),
    GetPage(name: '/resultkdp', page: () => ResultKdpScreen()),
    GetPage(name: '/resultkhk', page: () => ResultKhkScreen()),
    GetPage(name: '/resultpdp', page: () => ResultPdpScreen()),
    GetPage(name: '/resultwsg', page: () => ResultWsgScreen()),
    GetPage(name: '/resultanm', page: () => ResultAnmScreen()),

    GetPage(name: '/resultaum', page: () => AumSiswaResult()),

    GetPage(name: '/addjadwal', page: () => JadwalFormPage()),
    GetPage(name: '/jadwalsiswa', page: () => JadwalPage()),

    GetPage(name: '/formaum', page: () => FormAumSiswaResult()),

    GetPage(name: '/chat', page: () => ChatGuru()),
    GetPage(name: '/chatsiswa', page: () => ChatSiswa()),
    GetPage(name: '/chatting', page: () => ChattingScreen()),
    GetPage(name: '/bimbingan', page: () => ChatGuru()),
    GetPage(name: '/chatbot', page: () => ChatWindow()),

    GetPage(name: '/editjadwal', page: () => EditJadwalPage()),

    GetPage(name: '/logoutpageortu', page: () => LogoutPageOrtu()),

    GetPage(name: '/bot', page: () => BotWindow()),

    GetPage(name: '/my_security_pin', page: () => MySecurityPinPage()),
    GetPage(name: '/make_code_unlock', page: () => MakeCodeUnlock()),
    GetPage(name: '/code_unlock', page: () => CodeUnlock()),

    GetPage(name: '/listkelas', page: () => ListKelas()),
    GetPage(name: '/listkelasbimbingan', page: () => ListKelasBimbingan()),
    GetPage(name: '/listkelaspanggilan', page: () => ListKelasPanggilan()),
    GetPage(name: '/listsiswapanggilan', page: () => ListSiswaPanggilan()),
    GetPage(name: '/createpanggilan', page: () => CreatePanggilan()),
    GetPage(name: '/addpanggilan', page: () => AddPanggilan()),
    GetPage(name: '/editpanggilan', page: () => EditPanggilanPage()),

    GetPage(name: '/pelanggaranreport', page: () => PelanggaranReport()),
  ];
}
