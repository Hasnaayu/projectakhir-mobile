import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

mixin DialogMixin {
  void showSuccessSnackbar(String message) {
    Get.snackbar<void>(
      'Success',
      message,
      backgroundColor: Colors.green,
      colorText: Colors.white,
    );
  }

  void showErrorSnackbar(String message) {
    Get.snackbar<void>(
      'Oops',
      message,
      backgroundColor: Colors.red,
      colorText: Colors.white,
    );
  }

  Future<void> showTargetReminderPopUp(List<dynamic> messages,
      List<dynamic> messages2, List<dynamic> messages3) async {
    await Get.defaultDialog<void>(
      title: 'Jadwal Reminder',
      content: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: List<Widget>.generate(
          messages.length,
          (int index) => Column(
            children: [
              if (messages2[index] == 0 && messages3[index] == 0)
                Text(
                  '${index + 1}. \'\ ${messages[index]} \'\ target ends today.',
                )
              else if ((messages2[index] == 0 && messages3[index] < 0) ||
                  (messages2[index] < 0))
                // messages2[index]<0;
                Text(
                  '${index + 1}. \'\ ${messages[index]} \'\ target has expired.',
                )
              else if ((messages2[index] == 0 && messages3[index] > 0) ||
                  (messages2[index] > 0))
                Text(
                  '${index + 1}. \'\ ${messages[index]} \'\ target is ${messages2[index]} day(s) & ${messages3[index]} hour(s) left to achieve.',
                )
            ],
          ),
        ),
      ),
      actions: <Widget>[
        TextButton(
          child: Text(
            'Ok',
          ),
          onPressed: Get.back,
        ),
      ],
    );
  }

  Future<void> showWarningDialog(List<dynamic> messages) async {
    await Get.defaultDialog<void>(
      title: 'Warning',
      content: Column(
        children: List<Widget>.generate(
          messages.length,
          (int index) => Text(
            '- ${messages[index]}',
          ),
        ),
      ),
      actions: <Widget>[
        TextButton(
          child: Text(
            'Ok',
          ),
          onPressed: Get.back,
        ),
      ],
    );
  }

  Future<void> showSingleWarningDialog(String message) async {
    await Get.defaultDialog<void>(
      title: 'Warning',
      content: Text(
        message,
      ),
      actions: <Widget>[
        TextButton(
          child: Text(
            'Ok',
          ),
          onPressed: Get.back,
        ),
      ],
    );
  }

  Future<void> showSingleActionDialog(
    String title,
    String message,
    String buttonText,
    void Function() onTap,
  ) async {
    await Get.defaultDialog<void>(
      title: title,
      content: Text(
        message,
      ),
      onWillPop: () async {
        onTap();
        return true;
      },
      actions: <Widget>[
        TextButton(
          child: Text(
            buttonText,
          ),
          onPressed: onTap,
        ),
      ],
    );
  }

  void showMyBottomSheet(BuildContext context, Widget child) {
    showModalBottomSheet<void>(
      context: context,
      builder: (_) => child,
      backgroundColor: Colors.transparent,
    );
  }

  void showGetBottomSheet(Widget child) {
    Get.bottomSheet(child);
  }
}
