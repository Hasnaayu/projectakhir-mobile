import 'package:bk_mobile_app/auth/mixins/dialog_mixin.dart';
import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/auth/view_models/user_security_pin_view_model.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/user_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:get/route_manager.dart';

class LoginViewModel extends ChangeNotifier with DialogMixin {
  LoginViewModel();
  NetworkService _net;
  UserSecurityPinViewModel _userpin;

  String email = '';
  String password;
  String username;
  String emailError;
  final emailCharacters = RegExp(
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");

  List<UserModel> _users = <UserModel>[];
  List<UserModel> get users => _users;
  bool isHidePassword = true;

  void setNetworkService(NetworkService net) {
    _net = net;
  }

  void setUserPinViewModel(UserSecurityPinViewModel userpin) {
    _userpin = userpin;
    notifyListeners();
  }

  void togglePasswordVisibility() {
    isHidePassword = !isHidePassword;
    notifyListeners();
  }

  void onUsernameChange(String val) {
    username = val;
    notifyListeners();
  }

  void onEmailChange(String val) {
    email = val;
  }

  void onPasswordChange(String val) {
    password = val;
  }

  void onForgotChange(String val) {
    email = val;
    if (!emailCharacters.hasMatch(val)) {
      emailError = 'Email format is not valid.';
    } else if (val == null) {
      emailError = 'Fill in the email.';
    } else {
      emailError = null;
    }
  }

  Future<void> validateToken() async {
    final Response<dynamic> resp = await _net.request(
      '/login',
      requestMethod: 'post',
      data: <String, dynamic>{
        'email': email ?? null,
        'password': password ?? null,
      },
    );

    final Map<String, dynamic> respData = resp.data as Map<String, dynamic>;

    //print(respData);
    String token = respData['token']['token'];

    if (token != null) {
      _net.setToken(token);
    }

    if (await _net.validateToken()) {
      _net.getToken();
      if (respData['user']['id_role'] == 1) {
        Get.toNamed<void>('/homepageguru');
        showSuccessSnackbar('Login succesfully!');
      } else if (respData['user']['id_role'] == 2) {
        Get.toNamed<void>('/homepagesiswa');
        showSuccessSnackbar('Login succesfully!');
      } else {
        Get.toNamed<void>('/homepageortu');
        showSuccessSnackbar('Login succesfully!');
      }
    } else {
      Get.offNamed<void>('/');
      showSingleWarningDialog('The given credentials are wrong');
    }
  }

  Future<void> checkToken() async {
    final Response<dynamic> resp = await _net.request(
      '/user',
    );

    final Map<String, dynamic> respData = resp.data as Map<String, dynamic>;

    print(respData);
  }

  Future<void> isUserAvailable() async {
    final Response<dynamic> resp = await _net.request('/user/index');

    List<dynamic> listData = resp.data['data'];

    if (listData != null) {
      for (dynamic data in listData)
        _users.add(UserModel.fromJson(data as Map<String, dynamic>));
    }
    print(listData);
    notifyListeners();
  }

  Future<void> forgot() async {
    final Response<dynamic> resp = await _net.request(
      '/user/change_password',
      requestMethod: 'post',
      data: <String, dynamic>{
        'email': email ?? null,
      },
    );

    final Map<String, dynamic> respData = resp.data as Map<String, dynamic>;
    if (email != null) {
      for (var i = 0; i < _users.length; i++) {
        if (resp.data['msg'] != null) {
          email = null;
          Get.toNamed('/');
          showSuccessSnackbar(
              'Reset password link has been sent to your email.');
        } else {
          showErrorSnackbar('The credentials are wrong.');
        }
      }
    } else if (email == null) {
      showErrorSnackbar('Fill in the email.');
    }
    print(respData);
    notifyListeners();
  }

  Future<void> splashCheck() async {
    await _net.validateToken();
    await _userpin.checkPin();
    notifyListeners();
  }
}
