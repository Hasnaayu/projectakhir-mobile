import 'package:bk_mobile_app/auth/mixins/dialog_mixin.dart';
import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:get/route_manager.dart';

class LogoutViewModel extends ChangeNotifier with DialogMixin {
  LogoutViewModel();
  NetworkService _net;

  void setNetworkService(NetworkService net) {
    _net = net;
  }

  Future<void> doLogout() async {
    final Response<dynamic> resp = await _net.request(
      '/logout',
      requestMethod: 'get',
    );
    final Map<String, dynamic> respData = resp.data as Map<String, dynamic>;

    await _net.removeToken();
    Get.offAllNamed<void>('/');

    print(respData);
    notifyListeners();
  }
}
