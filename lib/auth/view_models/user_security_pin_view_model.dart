import 'package:bk_mobile_app/auth/mixins/dialog_mixin.dart';
import 'package:bk_mobile_app/auth/services/network_service.dart';
import 'package:bk_mobile_app/screens/homepage/siswa/aum_page/isi/models/user_security_pin_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';

class UserSecurityPinViewModel extends ChangeNotifier with DialogMixin {
  UserSecurityPinViewModel();
  NetworkService _net;

  List<UserSecurityPinModel> _userpins = [];
  List<UserSecurityPinModel> get userpins => _userpins;

  String pin;
  bool isOn;
  bool isHidePassword = true;

  void setNetworkService(NetworkService net) {
    _net = net;
    notifyListeners();
  }

  Future<void> onChangeIsOn(BuildContext context, bool val) async {
    print('onChangeisON');
    print('val=$val');
    print('isOn=$isOn');
    if (val == true) {
      Get.toNamed('/make_code_unlock');
    }
  }

  Future<void> onChangeIsOff(BuildContext context, bool val, int id) async {
    print('onChangeisOFF');
    print('val=$val');
    print('isOn=$isOn');
    if (val == false) {
      turnOffPin(id);
    }
  }

  void onChangePin(String val) {
    pin = val;
    print(pin);
    notifyListeners();
  }

  Future<void> fetchUserSecurityPin() async {
    final Response<dynamic> resp =
        await _net.request('/user_security_pin/index');
    List<dynamic> listData = resp.data['data'];
    if (listData != null) {
      for (dynamic data in listData)
        _userpins
            .add(UserSecurityPinModel.fromJson(data as Map<String, dynamic>));
    }
    print(listData);
    notifyListeners();
  }

  Future<void> checkPin() async {
    final Response<dynamic> resp =
        await _net.request('/user_security_pin/index');

    List<dynamic> listData = resp.data['data'];

    if (listData != null) {
      for (dynamic data in listData)
        _userpins
            .add(UserSecurityPinModel.fromJson(data as Map<String, dynamic>));
      if (_userpins.isNotEmpty) {
        Get.toNamed('/login');
      } else {
        Get.toNamed('/login');
      }
    } else if (listData == null) {
      Get.back();
    }
    print('userPin= $listData');
    notifyListeners();
  }

  Future<void> isPinCorrect() async {
    final Response<dynamic> resp =
        await _net.request('/user_security_pin/index');

    List<dynamic> listData = resp.data['data'];

    if (listData != null) {
      for (dynamic data in listData)
        _userpins
            .add(UserSecurityPinModel.fromJson(data as Map<String, dynamic>));

      for (var i = 0; i < _userpins.length; i++) {
        if (pin == _userpins[i].pin) {
          Get.toNamed('/login');
        } else {
          await turnOffPin(_userpins[i].id);
          await _net.removeToken();
          showSingleActionDialog(
              'Wrong Pin',
              'This account will be signed out for security purpose.',
              'Ok',
              () => Get.offAllNamed<void>('/'));
          // showSingleSnackbar('Incorrect pin.');
        }
      }
    }

    print(listData);
    notifyListeners();
  }

  Future<void> turnOnPin() async {
    var formData = FormData.fromMap({
      'pin': pin,
      'is_on': 1,
    });
    var response = await _net.request('/user_security_pin/store',
        requestMethod: 'post', data: formData);
    print(response);
    if (response.data['success'] == true) {
      pin = null;
      isOn = null;
      await removeEarlierData();
      Get.back<void>();
      showSuccessSnackbar('Successfully activated pin.');
    } else
      showErrorSnackbar('Fill in pin.');

    notifyListeners();
  }

  Future<void> turnOffPin(int id) async {
    final Response<dynamic> resp = await _net
        .request('/user_security_pin/delete/$id', requestMethod: 'delete');

    _userpins.remove(id);

    showSuccessSnackbar('You have successfully turned off pin.');

    print(resp);
    await removeEarlierData();
    notifyListeners();
  }

  Future<void> fetchDataList() async {
    if (_userpins.isEmpty) {
      await fetchUserSecurityPin();
    } else {
      print('datas have been fetched.');
    }
    notifyListeners();
  }

  Future<void> checkPinData() async {
    if (_userpins.isEmpty) {
      await checkPin();
    } else {
      print('datas have been fetched.');
    }
    notifyListeners();
  }

  Future<void> removeEarlierCheckData() async {
    _userpins.clear();
    print(userpins);
    await checkPinData();
    notifyListeners();
  }

  Future<void> removeEarlierData() async {
    _userpins.clear();
    print(userpins);
    await fetchDataList();
    notifyListeners();
  }
}
